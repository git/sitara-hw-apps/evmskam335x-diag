/*
 * i2cAccelerometer
 *
 * This application invokes APIs of HSI2C to control Accelerometer.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "hsi2c.h"
#include "gpio_v2.h"
#include "interrupt.h"
#include "soc_AM335x.h"
#include "uartStdio.h"
#include "evmskAM335x.h"
#include "evmskam335x_diag.h"
#include "I2cApi.h"
#include "delay.h"

/******************************************************************************
**              INTERNAL MACRO DEFINITIONS
******************************************************************************/
#define  GPIO_INSTANCE_PIN_NUMBER      (16u)
/* I2C address of LIS331DLH accelerometer*/
#define  ACC_I2C_SLAVE_ADDR           (0x18)
/* Address of TPS65910A (PMIC - control)  over I2C0. */
#define  PMIC_CNTL_I2C_SLAVE_ADDR   (0x2D)
/* Address of TPS65910A (PMIC - SR)  over I2C0. */
#define  PMIC_SR_I2C_SLAVE_ADDR     (0x12)
#define  ACC_ID           (0x32)

#define  WHO_AM_I           (0x0F)
#define  CTRL_REG1                    (0x20)
#define  CTRL_REG2                    (0x21)
#define  CTRL_REG3                    (0x22)
#define  CTRL_REG4                    (0x23)
#define  CTRL_REG5                    (0x24)
#define  CTRL_REG6                    (0x27)
#define  OUT_X_L_DATA                 (0x28)
#define  OUT_X_H_DATA                 (0x29)
#define  OUT_Y_L_DATA                 (0x2A)
#define  OUT_Y_H_DATA                 (0x2B)
#define  OUT_Z_L_DATA                 (0x2C)
#define  OUT_Z_H_DATA                 (0x2D)

/******************************************************************************
**              INTERNAL FUNCTION PROTOTYPES
******************************************************************************/
//static void SetupAccTransmit(unsigned int dcount);
static void SetupAccReception(unsigned int dcount);

static void AccelerometerRegRead(char regOffset, char* data);
//static void AccelerometerRegWrite(char regOffset, char data);

extern void cleanupInterrupts(unsigned int instNum);

/******************************************************************************
**              GLOBAL VARIABLE DEFINITIONS
******************************************************************************/
extern volatile unsigned char dataFromSlave[I2C_INSTANCE][60];
extern volatile unsigned char dataToSlave[I2C_INSTANCE][60];
extern volatile unsigned int  tCount[I2C_INSTANCE];
extern volatile unsigned int  rCount[I2C_INSTANCE];
extern volatile unsigned int  flag[I2C_INSTANCE];
extern volatile unsigned int  numOfBytes[I2C_INSTANCE];

volatile unsigned int gpioIsrFlag = 0;
char x_low;
char y_low;
char z_low;
char x_high;
char y_high;
char z_high;

unsigned int cosine [] = {10000, 99985, 99939, 99863, 99756, 99619, 99452, 99255,
                          99027, 98769, 98481, 98163, 97815, 97437, 97437,
                          96593, 96126, 95630, 95106, 94552, 93969, 93358,
                          92718, 92050, 91355, 90631, 89879, 89101, 88295,
                          87462, 85717, 84805, 83867, 82904, 81915, 80902,
                          79864, 78801, 77715, 76604, 75471, 74314, 73135,
                          71934, 69466, 68200, 66913, 65606, 64279, 62932,
                          61566, 60182, 58779, 57358, 55919, 54464, 52992,
                          51504, 48481, 46947, 45399, 43837, 42262, 40674,
                          39073, 37461, 35837, 34202, 32557, 30902, 29237,
                          27564, 25882, 24192, 22495, 20791, 19081, 17365,
                          15643, 13917, 12187, 10453, 8716,  6976,  5234,
                          3490,  1745, 0};

/******************************************************************************
**              FUNCTION DEFINITIONS
******************************************************************************/

int Accelerometer_id_test(void)
{
  char c=0;

  I2CInit(I2C_0);
  I2CMasterSlaveAddrSet(SOC_I2C_0_REGS, ACC_I2C_SLAVE_ADDR);

    /* WHO AM I */
    AccelerometerRegRead(WHO_AM_I, &c);
    UARTPuts("\nAccelerometer Device Identification: ", -1);
    UARTPutHexNum(c);
    UARTPuts("\n",-1);

    if(c==ACC_ID) //check for Accelerometer ID
    {
      return 0;
    }
    else
      return -1;


}

#if 0
static void AccelerometerRegWrite(char regOffset, char data)
{
    dataToSlave[I2C_0][0] = regOffset;
    dataToSlave[I2C_0][1] = data;

    tCount[I2C_0] = 0;
    SetupAccTransmit(2);
}
#endif

static void AccelerometerRegRead(char regOffset, char* data)
{
    tCount[I2C_0] = 0;
    rCount[I2C_0] = 0;

    dataToSlave[I2C_0][0] = regOffset;

    SetupAccReception(1);

    *data = dataFromSlave[I2C_0][0];
}

#if 0
/*
** Transmits data over I2C bus
*/
static void SetupAccTransmit(unsigned int dcount)
{
    /* Data Count specifies the number of bytes to be transmitted */
    I2CSetDataCount(SOC_I2C_0_REGS, dcount);

    numOfBytes[I2C_0] = I2CDataCountGet(SOC_I2C_0_REGS);

    /* Configure I2C controller in Master Transmitter mode */
    I2CMasterControl(SOC_I2C_0_REGS, I2C_CFG_MST_TX | I2C_CFG_STOP);

    /* Transmit and Stop condition interrupt is enabled */
    I2CMasterIntEnableEx(SOC_I2C_0_REGS, I2C_INT_TRANSMIT_READY |
                                         I2C_INT_STOP_CONDITION );

    /* Generate Start Condition over I2C bus */
    I2CMasterStart(SOC_I2C_0_REGS);

    while(flag[I2C_0]);

    /* Wait untill I2C registers are ready to access */
    while(!(I2CMasterIntRawStatus(SOC_I2C_0_REGS) & (I2C_INT_ADRR_READY_ACESS)));

    flag[I2C_0] = 1;
}

#endif

/*
** Receives data over I2C bus
*/
static void SetupAccReception(unsigned int dcount)
{
    /* Data Count specifies the number of bytes to be transmitted */
    I2CSetDataCount(SOC_I2C_0_REGS, 0x01);

    numOfBytes[I2C_0] = I2CDataCountGet(SOC_I2C_0_REGS);

    /* Clear status all interrupts */
    cleanupInterrupts(I2C_0);

    /* Configure I2C controller in Master Transmitter mode */
    I2CMasterControl(SOC_I2C_0_REGS, I2C_CFG_MST_TX);

    /* Transmit interrupt is enabled */
    I2CMasterIntEnableEx(SOC_I2C_0_REGS, I2C_INT_TRANSMIT_READY);

    /* Generate Start Condition over I2C bus */
    I2CMasterStart(SOC_I2C_0_REGS);

    while(I2CMasterBusBusy(SOC_I2C_0_REGS) == 0);

    while(tCount[I2C_0] != numOfBytes[I2C_0]);

    flag[I2C_0] = 1;

    /* Wait untill I2C registers are ready to access */
    while(!(I2CMasterIntRawStatus(SOC_I2C_0_REGS) & (I2C_INT_ADRR_READY_ACESS)));

    /* Data Count specifies the number of bytes to be received */
    I2CSetDataCount(SOC_I2C_0_REGS, dcount);

    numOfBytes[I2C_0] = I2CDataCountGet(SOC_I2C_0_REGS);

    cleanupInterrupts(I2C_0);

    /* Configure I2C controller in Master Receiver mode */
    I2CMasterControl(SOC_I2C_0_REGS, I2C_CFG_MST_RX);

    /* Receive and Stop Condition Interrupts are enabled */
    I2CMasterIntEnableEx(SOC_I2C_0_REGS,  I2C_INT_RECV_READY |
                                          I2C_INT_STOP_CONDITION);

    /* Generate Start Condition over I2C bus */
    I2CMasterStart(SOC_I2C_0_REGS);

    while(I2CMasterBusBusy(SOC_I2C_0_REGS) == 0);

    while(flag[I2C_0]);

    flag[I2C_0] = 1;
}
