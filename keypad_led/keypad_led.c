/*
 * keypad_led.c
 *
 * LED test.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "evmskAM335x.h"
#include "interrupt.h"
#include "soc_AM335x.h"
#include "evmskam335x_diag.h"
#include "evmskAM335x.h"
#include "uartStdio.h"
#include "gpio_v2.h"
#include "uart_irda_cir.h"
#include "hw_control_AM335x.h"
#include "hw_cm_per.h"
#include "delay.h"

/******************************************************************************
**                      INTERNAL MACRO DEFINITIONS
*******************************************************************************/
#define GPIO_INSTANCE_ADDRESS        (SOC_GPIO_1_REGS)
#define GPIO_INSTANCE_PIN_NUMBER     (4)

#define GPIO_WAKE_INSTANCE       (SOC_GPIO_0_REGS)
#define GPIO_WAKE_PIN_NUM      (30)

/******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
volatile unsigned char gKey=0; //keypad value

/******************************************************************************
**              FUNCTION DEFINITIONS
******************************************************************************/
#ifndef STARTERWARE_02_00_00_06
/**
 * \brief  This function does the Pin Multiplexing and selects GPIO pin GPIO0[6]
 *         for use. GPIO0[6] means 6th pin of GPIO0 instance.
 *
 * \param  None
 *
 * \return TRUE/FALSE
 *
 */

unsigned int GPIO0Pin6PinMuxSetup(void)
{

    HWREG(SOC_CONTROL_REGS + CONTROL_CONF_SPI0_CS1) =
                (CONTROL_CONF_SPI0_CS1_CONF_SPI0_CS1_RXACTIVE |
                 CONTROL_CONF_SPI0_CS1_CONF_SPI0_CS1_PUTYPESEL |
                 CONTROL_CONF_MUXMODE(7));
    return 0;
}

/**
 * \brief  This function does the Pin Multiplexing for the GPIO pin GPIO0[7]
 *         i.e. 7th pin of GPIO0 instance and selects it for use.
 *
 * \param  None
 *
 * \return TRUE/FALSE
 *
 */

unsigned int GPIO0Pin7PinMuxSetup(void)
{

            HWREG(SOC_CONTROL_REGS + CONTROL_CONF_ECAP0_IN_PWM0_OUT) =
                (CONTROL_CONF_ECAP0_IN_PWM0_OUT_CONF_ECAP0_IN_PWM0_OUT_SLEWCTRL |
                 CONTROL_CONF_ECAP0_IN_PWM0_OUT_CONF_ECAP0_IN_PWM0_OUT_RXACTIVE |  /*Reciever Enabled*/
                 (CONTROL_CONF_ECAP0_IN_PWM0_OUT_CONF_ECAP0_IN_PWM0_OUT_PUTYPESEL & (~CONTROL_CONF_ECAP0_IN_PWM0_OUT_CONF_ECAP0_IN_PWM0_OUT_PUTYPESEL)) | /*Pullup Selected*/
                 (CONTROL_CONF_ECAP0_IN_PWM0_OUT_CONF_ECAP0_IN_PWM0_OUT_PUDEN & (~CONTROL_CONF_ECAP0_IN_PWM0_OUT_CONF_ECAP0_IN_PWM0_OUT_PUDEN)) | /* Pullup enabled*/
                 CONTROL_CONF_MUXMODE(7));


    return 0;
}


/**
 * \brief  This function does the Pin Multiplexing and selects GPIO pin
 *         GPIO1[30] for use. GPIO1[30] means 30th pin of GPIO1 instance.
 *         This pin can be used to control the Audio Buzzer.
 *
 * \param  None
 *
 * \return TRUE/FALSE
 *
 * \note   Either of GPIO1[23] or GPIO1[30] pins could be used to control the
 *         Audio Buzzer.
 */

unsigned int GPIO1Pin30PinMuxSetup(void)
{
          HWREG(SOC_CONTROL_REGS + CONTROL_CONF_GPMC_CSN(1)) =
                (CONTROL_CONF_GPMC_CSN0_CONF_GPMC_CSN0_PUTYPESEL |
                 CONTROL_CONF_MUXMODE(7));
    return 0;
}

/*
** This function enables the L3 and L4_PER interface clocks.
** This also enables functional clocks of GPIO2 instance.
*/

void GPIO2ModuleClkConfig(void)
{
    /* Configuring L3 Interface Clocks. */

    /* Writing to MODULEMODE field of CM_PER_L3_CLKCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_L3_CLKCTRL) |=
          CM_PER_L3_CLKCTRL_MODULEMODE_ENABLE;

    /* Waiting for MODULEMODE field to reflect the written value. */
    while(CM_PER_L3_CLKCTRL_MODULEMODE_ENABLE !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3_CLKCTRL) &
           CM_PER_L3_CLKCTRL_MODULEMODE));

    /* Writing to MODULEMODE field of CM_PER_L3_INSTR_CLKCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_L3_INSTR_CLKCTRL) |=
          CM_PER_L3_INSTR_CLKCTRL_MODULEMODE_ENABLE;

    /* Waiting for MODULEMODE field to reflect the written value. */
    while(CM_PER_L3_INSTR_CLKCTRL_MODULEMODE_ENABLE !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3_INSTR_CLKCTRL) &
           CM_PER_L3_INSTR_CLKCTRL_MODULEMODE));

    /* Writing to CLKTRCTRL field of CM_PER_L3_CLKSTCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_L3_CLKSTCTRL) |=
          CM_PER_L3_CLKSTCTRL_CLKTRCTRL_SW_WKUP;

    /* Waiting for CLKTRCTRL field to reflect the written value. */
    while(CM_PER_L3_CLKSTCTRL_CLKTRCTRL_SW_WKUP !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3_CLKSTCTRL) &
           CM_PER_L3_CLKSTCTRL_CLKTRCTRL));

    /* Writing to CLKTRCTRL field of CM_PER_L3S_CLKSTCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_L3S_CLKSTCTRL) |=
          CM_PER_L3S_CLKSTCTRL_CLKTRCTRL_SW_WKUP;

    /*Waiting for CLKTRCTRL field to reflect the written value. */
    while(CM_PER_L3S_CLKSTCTRL_CLKTRCTRL_SW_WKUP !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3S_CLKSTCTRL) &
           CM_PER_L3S_CLKSTCTRL_CLKTRCTRL));

    /* Writing to MODULEMODE field in CM_PER_OCPWP_CLKCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_CLKCTRL) |=
          CM_PER_OCPWP_CLKCTRL_MODULEMODE_ENABLE;

    /* Waiting for MODULEMODE field to reflect the written value. */
    while(CM_PER_OCPWP_CLKCTRL_MODULEMODE_ENABLE !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_CLKCTRL) &
           CM_PER_OCPWP_CLKCTRL_MODULEMODE));

    /* Writing to CLKTRCTRL field of CM_PER_OCPWP_L3_CLKSTCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_L3_CLKSTCTRL) |=
          CM_PER_OCPWP_L3_CLKSTCTRL_CLKTRCTRL_SW_WKUP;

    /*Waiting for CLKTRCTRL field to reflect the written value. */
    while(CM_PER_OCPWP_L3_CLKSTCTRL_CLKTRCTRL_SW_WKUP !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_L3_CLKSTCTRL) &
           CM_PER_OCPWP_L3_CLKSTCTRL_CLKTRCTRL));


    /* Checking fields for necessary values.  */

    /* Waiting for IDLEST field in CM_PER_L3_CLKCTRL register to be set to 0x0. */
    while((CM_PER_L3_CLKCTRL_IDLEST_FUNC << CM_PER_L3_CLKCTRL_IDLEST_SHIFT)!=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3_CLKCTRL) &
           CM_PER_L3_CLKCTRL_IDLEST));

    /*
    ** Waiting for IDLEST field in CM_PER_L3_INSTR_CLKCTRL register to attain the
    ** desired value.
    */
    while((CM_PER_L3_INSTR_CLKCTRL_IDLEST_FUNC <<
           CM_PER_L3_INSTR_CLKCTRL_IDLEST_SHIFT)!=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3_INSTR_CLKCTRL) &
           CM_PER_L3_INSTR_CLKCTRL_IDLEST));

    /*
    ** Waiting for CLKACTIVITY_L3_GCLK field in CM_PER_L3_CLKSTCTRL register to
    ** attain the desired value.
    */
    while(CM_PER_L3_CLKSTCTRL_CLKACTIVITY_L3_GCLK !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3_CLKSTCTRL) &
           CM_PER_L3_CLKSTCTRL_CLKACTIVITY_L3_GCLK));

    /*
    ** Waiting for STBYST bit in CM_PER_OCPWP_CLKCTRL register to attain
    ** the desired value.
    */
    /* while((CM_PER_OCPWP_CLKCTRL_STBYST_FUNC <<
           CM_PER_OCPWP_CLKCTRL_STBYST_SHIFT) !=
           (HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_CLKCTRL) &
            CM_PER_OCPWP_CLKCTRL_STBYST)); */

    /*
    ** Waiting for IDLEST field in CM_PER_OCPWP_CLKCTRL register to attain the
    ** desired value.
    */
    while((CM_PER_OCPWP_CLKCTRL_IDLEST_FUNC <<
           CM_PER_OCPWP_CLKCTRL_IDLEST_SHIFT) !=
           (HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_CLKCTRL) &
            CM_PER_OCPWP_CLKCTRL_IDLEST));

    /*
    ** Waiting for CLKACTIVITY_OCPWP_L3_GCLK field in CM_PER_OCPWP_L3_CLKSTCTRL
    ** register to attain the desired value.
    */
    while(CM_PER_OCPWP_L3_CLKSTCTRL_CLKACTIVITY_OCPWP_L3_GCLK !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_L3_CLKSTCTRL) &
           CM_PER_OCPWP_L3_CLKSTCTRL_CLKACTIVITY_OCPWP_L3_GCLK));


    /*
    ** Waiting for CLKACTIVITY_L3S_GCLK field in CM_PER_L3S_CLKSTCTRL register
    ** to attain the desired value.
    */
    while(CM_PER_L3S_CLKSTCTRL_CLKACTIVITY_L3S_GCLK !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L3S_CLKSTCTRL) &
          CM_PER_L3S_CLKSTCTRL_CLKACTIVITY_L3S_GCLK));

    /* Configuring L4 Interface Clocks. */

    /* Writing to MODULEMODE field of CM_PER_L4LS_CLKCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_L4LS_CLKCTRL) |=
          CM_PER_L4LS_CLKCTRL_MODULEMODE_ENABLE;

    /* Waiting for MODULEMODE field to reflect the written value. */
    while(CM_PER_L4LS_CLKCTRL_MODULEMODE_ENABLE !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L4LS_CLKCTRL) &
           CM_PER_L4LS_CLKCTRL_MODULEMODE));

    /* Writing to CLKTRCTRL field of CM_PER_L4LS_CLKSTCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_L4LS_CLKSTCTRL) |=
          CM_PER_L4LS_CLKSTCTRL_CLKTRCTRL_SW_WKUP;

    /* Waiting for CLKTRCTRL field to reflect the written value. */
    while(CM_PER_L4LS_CLKSTCTRL_CLKTRCTRL_SW_WKUP !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L4LS_CLKSTCTRL) &
           CM_PER_L4LS_CLKSTCTRL_CLKTRCTRL));

    /* Verifying if other configurations are correct. */

    /*
    ** Waiting for IDLEST field in CM_PER_L4LS_CLKCTRL register to attain the
    ** desired value.
    */
    while((CM_PER_L4LS_CLKCTRL_IDLEST_FUNC <<
           CM_PER_L4LS_CLKCTRL_IDLEST_SHIFT) !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L4LS_CLKCTRL) &
           CM_PER_L4LS_CLKCTRL_IDLEST));

    /*
    ** Waiting for CLKACTIVITY_L4LS_GCLK bit in CM_PER_L4LS_CLKSTCTRL register
    ** to attain the desired value.
    */
    while(CM_PER_L4LS_CLKSTCTRL_CLKACTIVITY_L4LS_GCLK !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L4LS_CLKSTCTRL) &
           CM_PER_L4LS_CLKSTCTRL_CLKACTIVITY_L4LS_GCLK));

    /*
    ** Waiting for CLKACTIVITY_OCPWP_L4_GCLK bit in CM_PER_OCPWP_L3_CLKSTCTRL
    ** register to attain the desired value.
    */
    /* while(CM_PER_OCPWP_L3_CLKSTCTRL_CLKACTIVITY_OCPWP_L4_GCLK !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_OCPWP_L3_CLKSTCTRL) &
          CM_PER_OCPWP_L3_CLKSTCTRL_CLKACTIVITY_OCPWP_L4_GCLK)); */

    /* Performing configurations for GPIO2 instance. */

    /* Writing to MODULEMODE field of CM_PER_GPIO2_CLKCTRL register. */
    HWREG(SOC_CM_PER_REGS + CM_PER_GPIO2_CLKCTRL) |=
          CM_PER_GPIO2_CLKCTRL_MODULEMODE_ENABLE;

    /* Waiting for MODULEMODE field to reflect the written value. */
    while(CM_PER_GPIO2_CLKCTRL_MODULEMODE_ENABLE !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_GPIO2_CLKCTRL) &
           CM_PER_GPIO2_CLKCTRL_MODULEMODE));
    /*
    ** Writing to OPTFCLKEN_GPIO_2_GDBCLK bit in CM_PER_GPIO2_CLKCTRL
    ** register.
    */
    HWREG(SOC_CM_PER_REGS + CM_PER_GPIO2_CLKCTRL) |=
          CM_PER_GPIO2_CLKCTRL_OPTFCLKEN_GPIO_2_GDBCLK;

    /*
    ** Waiting for OPTFCLKEN_GPIO_2_GDBCLK bit to reflect the desired
    ** value.
    */
    while(CM_PER_GPIO2_CLKCTRL_OPTFCLKEN_GPIO_2_GDBCLK !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_GPIO2_CLKCTRL) &
           CM_PER_GPIO2_CLKCTRL_OPTFCLKEN_GPIO_2_GDBCLK));

    /*
    ** Waiting for IDLEST field in CM_PER_GPIO2_CLKCTRL register to attain the
    ** desired value.
    */
    while((CM_PER_GPIO2_CLKCTRL_IDLEST_FUNC <<
           CM_PER_GPIO2_CLKCTRL_IDLEST_SHIFT) !=
           (HWREG(SOC_CM_PER_REGS + CM_PER_GPIO2_CLKCTRL) &
            CM_PER_GPIO2_CLKCTRL_IDLEST));

    /*
    ** Waiting for CLKACTIVITY_GPIO_2_GDBCLK bit in CM_PER_L4LS_CLKSTCTRL
    ** register to attain desired value.
    */
    while(CM_PER_L4LS_CLKSTCTRL_CLKACTIVITY_GPIO_2_GDBCLK !=
          (HWREG(SOC_CM_PER_REGS + CM_PER_L4LS_CLKSTCTRL) &
           CM_PER_L4LS_CLKSTCTRL_CLKACTIVITY_GPIO_2_GDBCLK));
}

/*
** This function enables GPIO1 pins
*/
unsigned int GPIO2PinMuxSetup(unsigned int pinNo)
{
    HWREG(SOC_CONTROL_REGS + CONTROL_CONF_GPMC_AD(pinNo)) =
        (CONTROL_CONF_GPMC_AD_CONF_GPMC_AD_SLEWCTRL |     /* Slew rate slow */
        CONTROL_CONF_GPMC_AD_CONF_GPMC_AD_RXACTIVE |    /* Receiver enabled */
        (CONTROL_CONF_GPMC_AD_CONF_GPMC_AD_PUDEN & (~CONTROL_CONF_GPMC_AD_CONF_GPMC_AD_PUDEN)) | /* PU_PD enabled */
        (CONTROL_CONF_GPMC_AD_CONF_GPMC_AD_PUTYPESEL & (~CONTROL_CONF_GPMC_AD_CONF_GPMC_AD_PUTYPESEL)) | /* PD */
        (CONTROL_CONF_MUXMODE(7))    /* Select mode 7 */
        );
     return TRUE;
}


unsigned int GPIO0Pin30PinMuxSetup(void)
{
  HWREG(SOC_CONTROL_REGS + CONTROL_CONF_GPMC_WAIT0) =
              (CONTROL_CONF_GPMC_A11_CONF_GPMC_A11_SLEWCTRL |
              CONTROL_CONF_GPMC_WAIT0_CONF_GPMC_WAIT0_RXACTIVE |
              (CONTROL_CONF_GPMC_WAIT0_CONF_GPMC_WAIT0_PUDEN & (~CONTROL_CONF_GPMC_WAIT0_CONF_GPMC_WAIT0_PUDEN)) |
               (CONTROL_CONF_GPMC_WAIT0_CONF_GPMC_WAIT0_PUTYPESEL &(~CONTROL_CONF_GPMC_WAIT0_CONF_GPMC_WAIT0_PUTYPESEL)) |
              (CONTROL_CONF_MUXMODE(7))
              );
  return 0;
}

#if 0
void Clkout1_Setup(void)
{
  HWREG(SOC_CONTROL_REGS + CONTROL_CONF_XDMA_EVENT_INTR(0)) =
                    CLKOUT1_SEL_MODE;
}
#endif

#endif

/*
** GPIO Interrupt Service Routine.
*/
static void GPIOIsr(void)
{
  gKey = ((GPIOPinRead(SOC_GPIO_2_REGS,GPIO_INSTANCE_PIN_NUMBER_3)&0x0008)>>3); //SW1
    gKey+=((GPIOPinRead(SOC_GPIO_2_REGS,GPIO_INSTANCE_PIN_NUMBER_2)&0x0004)>>1); //SW2
  gKey+=((GPIOPinRead(SOC_GPIO_0_REGS,GPIO_INSTANCE_PIN_NUMBER_30)&0x40000000)>>28); //SW3
  gKey+=((GPIOPinRead(SOC_GPIO_2_REGS,GPIO_INSTANCE_PIN_NUMBER_5)&0x0020)>>2); //SW4

    /* Check the Interrupt Status of the GPIO Card Detect pin. */
    if(GPIOPinIntStatus(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                        GPIO_INT_LINE_1,
                        GPIO_SW1_PIN_NUM) & (1 << GPIO_SW1_PIN_NUM))
    {
        /* Clear the Interrupt Status of the GPIO Card Detect pin. */
        GPIOPinIntClear(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                        GPIO_INT_LINE_1,
                        GPIO_SW1_PIN_NUM);

    }

    /* Check the Interrupt Status of the GPIO Card Detect pin. */
    if(GPIOPinIntStatus(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                        GPIO_INT_LINE_1,
                        GPIO_SW2_PIN_NUM) & (1 << GPIO_SW2_PIN_NUM))
    {
        /* Clear the Interrupt Status of the GPIO Card Detect pin. */
        GPIOPinIntClear(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                        GPIO_INT_LINE_1,
                        GPIO_SW2_PIN_NUM);
    }


    /* Check the Interrupt Status of the GPIO Card Detect pin. */
    if(GPIOPinIntStatus(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                        GPIO_INT_LINE_1,
                        GPIO_SW4_PIN_NUM) & (1 << GPIO_SW4_PIN_NUM))
    {
      /* Clear the Interrupt Status of the GPIO Card Detect pin. */
      GPIOPinIntClear(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                  GPIO_INT_LINE_1,
                  GPIO_SW4_PIN_NUM);
    }

    /* Check the Interrupt Status of the GPIO Card Detect pin. */
    if(GPIOPinIntStatus(GPIO_BASE_ADD_KEYPAD_SW3,
                    GPIO_INT_LINE_1,
                    GPIO_SW3_PIN_NUM) & (1 << GPIO_SW3_PIN_NUM))
    {
      /* Clear the Interrupt Status of the GPIO Card Detect pin. */
      GPIOPinIntClear(GPIO_BASE_ADD_KEYPAD_SW3,
                        GPIO_INT_LINE_1,
                        GPIO_SW3_PIN_NUM);
    }

}

/*
** This function configures the Interrupt Controller (INTC) to receive
** GPIO Interrupt.
*/
void GPIOINTCConfigure(void)
{

#if 0 //Debounce removed
    /* Enable Debouncing feature for the Intput GPIO Pin. */
      GPIODebounceFuncControl(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                  GPIO_SW1_PIN_NUM,
                              GPIO_DEBOUNCE_FUNC_ENABLE);

      GPIODebounceFuncControl(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                  GPIO_SW2_PIN_NUM,
                            GPIO_DEBOUNCE_FUNC_ENABLE);
      GPIODebounceFuncControl(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                  GPIO_SW4_PIN_NUM,
                            GPIO_DEBOUNCE_FUNC_ENABLE);

      GPIODebounceFuncControl(GPIO_BASE_ADD_KEYPAD_SW3,
                    GPIO_SW3_PIN_NUM,
                              GPIO_DEBOUNCE_FUNC_ENABLE);

      /*
      ** Configure the Debouncing Time for all the input pins of
      ** the seleceted GPIO instance.
      */
      GPIODebounceTimeConfig(GPIO_BASE_ADD_KEYPAD_SW1_2_4,32); //1 milliSec
      GPIODebounceTimeConfig(GPIO_BASE_ADD_KEYPAD_SW3, 32); //1 milliSec
#endif

      /*
      ** Disable interrupt generation on detection of a logic HIGH or
      ** LOW levels.
      */
      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
             GPIO_SW1_PIN_NUM,
                     GPIO_INT_TYPE_NO_LEVEL);
      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
             GPIO_SW2_PIN_NUM,
                   GPIO_INT_TYPE_NO_LEVEL);
      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
             GPIO_SW4_PIN_NUM,
                   GPIO_INT_TYPE_NO_LEVEL);

      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW3,
             GPIO_SW3_PIN_NUM,
                   GPIO_INT_TYPE_NO_LEVEL);

      /*
      ** Enable interrupt generation on detection of a rising or a
      ** falling edge.
      */
      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
             GPIO_SW1_PIN_NUM,
             GPIO_INT_TYPE_RISE_EDGE);
      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
             GPIO_SW2_PIN_NUM,
             GPIO_INT_TYPE_RISE_EDGE);
      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
             GPIO_SW4_PIN_NUM,
             GPIO_INT_TYPE_RISE_EDGE);

      GPIOIntTypeSet(GPIO_BASE_ADD_KEYPAD_SW3,
             GPIO_SW3_PIN_NUM,
             GPIO_INT_TYPE_RISE_EDGE);


      /* Enable interrupt for the specified GPIO Input Pin. */
      GPIOPinIntEnable(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                       GPIO_INT_LINE_1,
                       GPIO_SW1_PIN_NUM);
      GPIOPinIntEnable(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                       GPIO_INT_LINE_1,
                       GPIO_SW2_PIN_NUM);
      GPIOPinIntEnable(GPIO_BASE_ADD_KEYPAD_SW1_2_4,
                       GPIO_INT_LINE_1,
                       GPIO_SW4_PIN_NUM);

      GPIOPinIntEnable(GPIO_BASE_ADD_KEYPAD_SW3,
                       GPIO_INT_LINE_1,
                       GPIO_SW3_PIN_NUM);

      /* Configure the INTC to receive GPIO Interrupts. */

      /* Register the Interrupt Service Routine(ISR). */
      IntRegister(GPIO_SYS_INT_NUM_SW1_2_4, GPIOIsr);
      IntRegister(GPIO_SYS_INT_NUM_SW3, GPIOIsr);

      /* Set the priority for the GPIO0 system interrupt in INTC. */
      IntPrioritySet(GPIO_SYS_INT_NUM_SW1_2_4, 0, AINTC_HOSTINT_ROUTE_IRQ);
      IntPrioritySet(GPIO_SYS_INT_NUM_SW3, 0, AINTC_HOSTINT_ROUTE_IRQ);

      /* Enable the GPIO0 system interrupt in INTC. */
      IntSystemEnable(GPIO_SYS_INT_NUM_SW1_2_4);
      IntSystemEnable(GPIO_SYS_INT_NUM_SW3);

}

void Clear_Leds(void)
{
  /*Clear All Leds */
  GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_4,GPIO_PIN_LOW);
  GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_5,GPIO_PIN_LOW);
  GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_7,GPIO_PIN_LOW);
  GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_6,GPIO_PIN_LOW);
}

/*ON- 1, OFF-0*/
void UserLedOnOff(unsigned int ledno,unsigned int onoff)
{

  switch(ledno)
  {
  case 1:
    GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_4,onoff);
    break;
  case 2:
    GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_5,onoff);
    break;
  case 3:
    GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_7,onoff);
    break;

  case 4:
    GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_6,onoff);
    break;
  default:
    break;
  }
}

void UserLedtest(void)
{
  int i=0;

  Clear_Leds();

  /* Driving GPIO1[4-7] pin to logic Sequentialy HIGH and finally off */
  for(i=0;i<5;i++)
  {
    switch(i)
    {
    case 0:
      UARTPuts("\nUser LED 1 ON\n", -1);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_4,GPIO_PIN_HIGH);

      break;
    case 1:
      UARTPuts("\nUser LED 2 ON\n", -1);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_4,GPIO_PIN_LOW);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_5,GPIO_PIN_HIGH);
      break;
    case 2:
      UARTPuts("\nUser LED 3 ON\n", -1);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_5,GPIO_PIN_LOW);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_7,GPIO_PIN_HIGH);
      break;
    case 3:
      UARTPuts("\nUser LED 4 ON\n", -1);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_7,GPIO_PIN_LOW);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_6,GPIO_PIN_HIGH);
      break;
    case 4:
      UARTPuts("\nAll User LED ON\n", -1);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_4,GPIO_PIN_HIGH);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_5,GPIO_PIN_HIGH);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_7,GPIO_PIN_HIGH);
      GPIOPinWrite(SOC_GPIO_1_REGS,GPIO_INSTANCE_PIN_NUMBER_6,GPIO_PIN_HIGH);
      break;
    default:
      break;
    }

    delay(1000);


  }
}

/*
 * Pass the Key number 0-3 for SW1-SW4
 * return 0 on success and -1 on fail
 */

int Checkforkeypress(unsigned char testno)
{
  int retval=-1;
  unsigned char expKey=0;

  /*Clear All Leds */
  Clear_Leds();

  switch(gKey) //key)
  {
    case USER_KEY_1:
      UARTPuts("\nUser Key 1 is pressed\n", -1);
      UserLedOnOff(LED_1,LED_ON); //User LED 1 ON

        break;
    case USER_KEY_2:
      UARTPuts("\nUser Key 2 is pressed\n", -1);
      UserLedOnOff(LED_2,LED_ON); //User LED 2 ON
      break;
    case USER_KEY_3:
      UARTPuts("\nUser Key 3 is pressed\n", -1);
      UserLedOnOff(LED_3,LED_ON); //User LED 3 ON
      break;
    case USER_KEY_4:
      UARTPuts("\nUser Key 4 is pressed\n", -1);
      UserLedOnOff(LED_4,LED_ON); //User LED 4 ON
      break;
    default:
      UARTPuts("\nMultiple keys are pressed\n", -1);
        //key=0; //multiple key press is currently disabled in this diagnostics
        break;
  }


  switch(testno)
  {
    case 0:
      expKey=USER_KEY_1;
        break;
    case 1:
      expKey=USER_KEY_2;
      break;
    case 2:
      expKey=USER_KEY_3;
      break;
    case 3:
      expKey=USER_KEY_4;
      break;
    default:
        break;
  }


  if(gKey==expKey)
  {
      UARTPuts("\nCorrect Key Pressed - PASS", -1);
      retval=0;
  }
  else
  {
    UARTPuts("\nWrong Key Pressed - FAIL", -1);
    retval=-1;
  }

  gKey=0; //clear the key value to read a new key press value

  return retval;
}

void configWakeGpio()
{
  /* Enabling the GPIO module. */
  GPIOModuleEnable(GPIO_WAKE_INSTANCE);

  /* Perform a module reset of the GPIO module. */
    //GPIOModuleReset(GPIO_WAKE_INSTANCE);

  /* Set the specified pin as an Input pin. */
    GPIODirModeSet(GPIO_WAKE_INSTANCE,
                   GPIO_WAKE_PIN_NUM,
                   GPIO_DIR_INPUT);

  GPIOIntTypeSet(GPIO_WAKE_INSTANCE,
          GPIO_WAKE_PIN_NUM,
          GPIO_INT_TYPE_BOTH_EDGE);

  HWREG(GPIO_WAKE_INSTANCE + 0x34) = 0x40000000;
  HWREG(GPIO_WAKE_INSTANCE + 0x38) = 0x40000000;

  HWREG(GPIO_WAKE_INSTANCE + 0x44) = 0x40000000;

}

void enableGpioWake(void)
{
  /*  Clearing immediately after wake up is not consistent.
  **  Seems event is logged after clearing  */
  /*  Clear wake interrupt  */
  HWREG(GPIO_WAKE_INSTANCE + 0x2C) = 0x40000000;
  HWREG(GPIO_WAKE_INSTANCE + 0x30) = 0x40000000;

  /*  Configure GPIO in smart-dile and wake capable mode  */
  HWREG(GPIO_WAKE_INSTANCE + GPIO_SYSCONFIG) = 0x1C;
}

void disableGpioWake(void)
{
  /*  Configure GPIO in Force-idle mode */
  HWREG(GPIO_WAKE_INSTANCE + GPIO_SYSCONFIG) = 0;
}
