/*
 * evmskAM335x_diagMain.c
 *
 * This file contains the main() and other functions.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "interrupt.h"
#include "soc_AM335x.h"
#include "ToneLoop.h"
#include "Cfg.h"
#include "Aic31.h"
#include "evmskam335x_diag.h"
#include "evmskAM335x.h"
#include "uartStdio.h"
#include "gpio_v2.h"
#include "uart_irda_cir.h"
#include "hw_control_AM335x.h"
#include "hw_cm_per.h"
#include "IDConfiguration.h"
#include "emacTest.h"
#include "phy.h"
#include "ar8031.h"
#include "delay.h"
#include "emac.h"
#include "cpsw.h"
#include "cap_touch.h"
#include "ds0PwrMgmnt.h"
#include "cache.h"
#include "mmu.h"
#include "cp15.h"
#include "clock.h"
#include "I2cApi.h"


/****************************************************************************
**                   INTERNAL MACRO DEFINITIONS
***************************************************************************/
//#define MMU_CACHE_ENABLE

#define START_ADDR_DDR             (0x80000000)
#define START_ADDR_DEV             (0x44000000)
#define START_ADDR_OCMC            (0x40300000)
#define NUM_SECTIONS_DDR           (512)
#define NUM_SECTIONS_DEV           (960)
#define NUM_SECTIONS_OCMC          (1)

/* Automated Diagnostics Test*/
#define DDR3_SELECT           1
#define I2C_EEPROM_SELECT       2
#define ACCELEROMETER_SELECT      3
#define MICROSD_SELECT          4
#define USB1_HOST_SELECT        5
#define PMIC_SELECT           6
#define ENET_GIGABIT_1_SELECT     7
#define ENET_GIGABIT_2_SELECT     8

/* Manual tests*/
#define LCD_SELECT            9
#define TOUCH_SCREEN_SELECT       10
#define AUDIO_CODEC_SELECT        11
#define USER_LED_SELECT         12
#define USER_KEYPAD_SELECT        13
#define INTERRUPT_KEY_SELECT      14
#define USB0_DEVICE_SELECT        15
#define PM_DS0_SELECT         16
#define RETURN_TO_MAIN_SELECT     17

/****************************************************************************
**                   LOCAL FUNCTION PROTOTYPES
****************************************************************************/
static void PeripheralsSetUp(void);
static void dummyIsr(void);
static void uartIsr(void);

void Delay(volatile unsigned int count);
static void initNMIEvents(void);
//static void clearNMIEvents(void);
static void NMIIsr(void);
extern INT16 DDR_DMATest(void);

/****************************************************************************
**                  GLOBAL VARIABLES DEFINITIONS
****************************************************************************/
#ifdef __TMS470__
#pragma DATA_ALIGN(pageTable, 16384);
static volatile unsigned int pageTable[4*1024];
#elif defined(__IAR_SYSTEMS_ICC__)
#pragma data_alignment=16384
static volatile unsigned int pageTable[4*1024];
#else
static volatile unsigned int pageTable[4*1024] __attribute__((aligned(16*1024)));
#endif


unsigned char usb_flag; // for USB vs MMC Disk IO handling
signed char gNmiF=0; //NMI interrupt flag
signed char gusb1_host_onceF=0,gusb1_host_statusF=0; //USB1 host one time test, test status
signed char gusb0_dev_onceF=0,gusb0_dev_statusF=0;//USB0 dev one time test, test status
signed char gmicroSD_onceF=0, gmicroSD_statusF=0; //microSD one time test, test status
unsigned char gPassF, gFailF;
unsigned char pm_ds0_onceF=0; //ds0 one time initialisation

/* Global for the link detection of 10M/100M to be used
 * for the 1000M since 1G link detection is not working
 * Note: this is just a temporary fix*/
extern unsigned char glinkDetectF;

extern unsigned int iram_start;
extern unsigned int iram_size;
extern unsigned int relocstart;
extern unsigned int relocend;

/****************************************************************************
**                      FUNCTION DEFINITIONS
****************************************************************************/
/*
** Function to setup MMU. This function Maps three regions ( 1. DDR
** 2. OCMC and 3. Device memory) and enables MMU.
*/
void MMUConfigAndEnable(void)
{
    /*
    ** Define DDR memory region of AM335x. DDR can be configured as Normal
    ** memory with R/W access in user/privileged modes. The cache attributes
    ** specified here are,
    ** Inner - Write through, No Write Allocate
    ** Outer - Write Back, Write Allocate
    */
    REGION regionDdr = {
                        MMU_PGTYPE_SECTION, START_ADDR_DDR, NUM_SECTIONS_DDR,
                        MMU_MEMTYPE_NORMAL_NON_SHAREABLE(MMU_CACHE_WT_NOWA,
                                                         MMU_CACHE_WB_WA),
                        MMU_REGION_NON_SECURE, MMU_AP_PRV_RW_USR_RW,
                        (unsigned int*)pageTable
                       };
    /*
    ** Define OCMC RAM region of AM335x. Same Attributes of DDR region given.
    */
    REGION regionOcmc = {
                         MMU_PGTYPE_SECTION, START_ADDR_OCMC, NUM_SECTIONS_OCMC,
                         MMU_MEMTYPE_NORMAL_NON_SHAREABLE(MMU_CACHE_WT_NOWA,
                                                          MMU_CACHE_WB_WA),
                         MMU_REGION_NON_SECURE, MMU_AP_PRV_RW_USR_RW,
                         (unsigned int*)pageTable
                        };

    /*
    ** Define Device Memory Region. The region between OCMC and DDR is
    ** configured as device memory, with R/W access in user/privileged modes.
    ** Also, the region is marked 'Execute Never'.
    */
    REGION regionDev = {
                        MMU_PGTYPE_SECTION, START_ADDR_DEV, NUM_SECTIONS_DEV,
                        MMU_MEMTYPE_DEVICE_SHAREABLE,
                        MMU_REGION_NON_SECURE,
                        MMU_AP_PRV_RW_USR_RW  | MMU_SECTION_EXEC_NEVER,
                        (unsigned int*)pageTable
                       };

    /* Initialize the page table and MMU */
    MMUInit((unsigned int*)pageTable);

    /* Map the defined regions */
    MMUMemRegionMap(&regionDdr);
    MMUMemRegionMap(&regionOcmc);
    MMUMemRegionMap(&regionDev);

    /* Now Safe to enable MMU */
    MMUEnable((unsigned int*)pageTable);
}

#ifdef HW_FLOW
/**
 * UARTConfigureHWFlowControl.
 *
 * This function configures the H/W Flow Control (RTS & CTS signals)
 * This function takes uart address as a parameter.
 *
 *
 * @param   uartAddr        [IN]    uart base address.
 * EnableF [IN] - Enable/Disable Flag -  0 - Disable, 1- Enable
 *
 *
 */

void UARTConfigureHWFlowControl(unsigned int uartAddr, unsigned EnableF)
{
  unsigned int tcrTlrBitVal = 0;

  //For H/W Flow Control
    /* CTS Pin -- Input Pin */

    HWREG(SOC_CONTROL_REGS + CONTROL_CONF_UART_CTSN(0) ) =
        (CONTROL_CONF_UART0_CTSN_CONF_UART0_CTSN_SLEWCTRL |
            CONTROL_CONF_UART0_CTSN_CONF_UART0_CTSN_RXACTIVE |
            (CONTROL_CONF_UART0_CTSN_CONF_UART0_CTSN_PUDEN & (~CONTROL_CONF_UART0_CTSN_CONF_UART0_CTSN_PUDEN)) |
            (CONTROL_CONF_UART0_CTSN_CONF_UART0_CTSN_PUTYPESEL & (~CONTROL_CONF_UART0_CTSN_CONF_UART0_CTSN_PUTYPESEL)));

  /* RTS Pin -- Output Pin */
  HWREG(SOC_CONTROL_REGS + CONTROL_CONF_UART_RTSN(0) ) =
      (CONTROL_CONF_UART0_RTSN_CONF_UART0_RTSN_SLEWCTRL |
          CONTROL_CONF_UART0_RTSN_CONF_UART0_RTSN_RXACTIVE |
          (CONTROL_CONF_UART0_RTSN_CONF_UART0_RTSN_PUDEN & (~CONTROL_CONF_UART0_RTSN_CONF_UART0_RTSN_PUDEN)) |
          (CONTROL_CONF_UART0_RTSN_CONF_UART0_RTSN_PUTYPESEL & (~CONTROL_CONF_UART0_RTSN_CONF_UART0_RTSN_PUTYPESEL)));


  tcrTlrBitVal = UARTSubConfigTCRTLRModeEn(uartAddr);
  /* Write the RTS_START and RTS_HALT Trigger values
   * Trigger level = 4 � [4-bit register value]
   * */

  UARTFlowCtrlTrigLvlConfig(uartAddr,3,0);

  /* Restoring the value of TCRTLR bit in MCR. */
    UARTTCRTLRBitValRestore(uartAddr, tcrTlrBitVal);

    if(EnableF)
      UARTAutoRTSAutoCTSControl(uartAddr,UART_AUTO_CTS_ENABLE,UART_AUTO_RTS_ENABLE);
    else
      UARTAutoRTSAutoCTSControl(uartAddr,UART_AUTO_CTS_DISABLE,UART_AUTO_RTS_DISABLE);

}
#endif


/****************************************************************************
**                      FUNCTION DEFINITIONS
****************************************************************************/
/*
** Enable all the peripherals in use
*/
static void PeripheralsSetUp(void)
{
  /****************************************************/
  /***************Peripheral Clock Setting*************/
  /****************************************************/

    enableModuleClock(CLK_UART0);
  enableModuleClock(CLK_LCDC);
  enableModuleClock(CLK_TIMER2);
    enableModuleClock(CLK_I2C0);
  enableModuleClock(CLK_MCASP1);
  enableModuleClock(CLK_ADC_TSC);
  enableModuleClock(CLK_TPTC2);
  enableModuleClock(CLK_TPTC1);
  enableModuleClock(CLK_TPTC0);
  enableModuleClock(CLK_TPCC);

    /*GPIO LED - 1.04-1.07,
     * Keypad - 0.30,2.03,2.02,2.05,
     * LCD Display enable 1.28,1.29(NA if PWM),
     * MMC Card detect 0.06,
     * headset detect - 2.00*/
    /* Configuring the functional clock for GPIO0 instance. */
    GPIO0ModuleClkConfig();
    GPIO1ModuleClkConfig();
    GPIO2ModuleClkConfig();

    EDMAModuleClkConfig();

    CPSWClkEnable();

    /****************************************************/
    /*******Peripheral PINMUX Setting********************/
    /****************************************************/

    /* Configuration for User LEDs GPIO1_04 - GPIO1_07. */
    GPIO1PinMuxSetup(4);
    GPIO1PinMuxSetup(5);
    GPIO1PinMuxSetup(6);
    GPIO1PinMuxSetup(7);

    /* Configuration for User keypad GPIO0_30, GPI02_3,GPI02_2,GPI02_5 */

    GPIO0Pin30PinMuxSetup();
    GPIO2PinMuxSetup(3);
    GPIO2PinMuxSetup(2);
    GPIO2PinMuxSetup(5);

    /* Configuration for Headset detect GPIO2_0 */
    GPIO2PinMuxSetup(0);

    /*LCD Backlight enable and LCD Display Enable*/
    GPIO1PinMuxSetup(28);

    /* MMC Card Detect Pin mux selection*/
    GPIO0Pin6PinMuxSetup();

#ifdef CAPTOUCH
    /*CT Wake GPIO2_1 */
    GPIO2PinMuxSetup(1);
#endif

    /*DDR_VTT_EN - GPIO0_7 */
    GPIO0Pin7PinMuxSetup();

    LCDPinMuxSetup();
    CPSWPinMuxSetup();
    I2CPinMuxSetup(0);
    I2CPinMuxSetup(1);
    McASP1PinMuxSetup();
    TSCADCPinMuxSetUp();
    ECAPPinMuxSetup(2);

    /****************************************************/
    /*******GPIO Module initialization******************/
    /****************************************************/

    /* Enabling the GPIO module. */
    GPIOModuleEnable(SOC_GPIO_0_REGS);
    GPIOModuleEnable(SOC_GPIO_1_REGS);
    GPIOModuleEnable(SOC_GPIO_2_REGS);

    /* Resetting the GPIO module. */
    GPIOModuleReset(SOC_GPIO_0_REGS);
    GPIOModuleReset(SOC_GPIO_1_REGS);
    GPIOModuleReset(SOC_GPIO_2_REGS);


     /* Configuring GPIO pin as an output pin for
      * LED - GPIO1_04 - GPIO1_07*/

    GPIODirModeSet(SOC_GPIO_1_REGS,
             GPIO_INSTANCE_PIN_NUMBER_4,
             GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_1_REGS,
             GPIO_INSTANCE_PIN_NUMBER_5,
             GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_1_REGS,
             GPIO_INSTANCE_PIN_NUMBER_7,
             GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_1_REGS,
             GPIO_INSTANCE_PIN_NUMBER_6,
             GPIO_DIR_OUTPUT);
    /* Keypad as Input GPIO2_02,GPIO2_03,GPIO2_05, GPIO0_30 */

    GPIODirModeSet(SOC_GPIO_0_REGS,
             GPIO_INSTANCE_PIN_NUMBER_30,
             GPIO_DIR_INPUT);
    GPIODirModeSet(SOC_GPIO_2_REGS,
             GPIO_INSTANCE_PIN_NUMBER_3,
             GPIO_DIR_INPUT);
    GPIODirModeSet(SOC_GPIO_2_REGS,
             GPIO_INSTANCE_PIN_NUMBER_2,
             GPIO_DIR_INPUT);

    GPIODirModeSet(SOC_GPIO_2_REGS,
             GPIO_INSTANCE_PIN_NUMBER_5,
             GPIO_DIR_INPUT);

    /* Headset Detect input */
    GPIODirModeSet(SOC_GPIO_2_REGS,
             GPIO_INSTANCE_PIN_NUMBER_0,
             GPIO_DIR_INPUT);


    /* LCD Back light enable, LCD display enable = GPIO1_28,GPIO1_29 */

    GPIODirModeSet(SOC_GPIO_1_REGS,
               GPIO_INSTANCE_PIN_NUMBER_28,
               GPIO_DIR_OUTPUT);

    /* Configuring GPIO0[HSMMCSD_0_CARD_DETECT_PINNUM] pin as an input pin. */
    GPIODirModeSet(SOC_GPIO_0_REGS,
                 HSMMCSD_0_CARD_DETECT_PINNUM,
                 GPIO_DIR_INPUT);

#ifdef CAPTOUCH
    /*CT Wake GPIO2_1 */
    GPIODirModeSet(SOC_GPIO_2_REGS,
             GPIO_INSTANCE_PIN_NUMBER_1,
             GPIO_DIR_OUTPUT);
#endif

    /*DDR_VTT_EN - GPIO0_7  - configured as input*/
    GPIODirModeSet(SOC_GPIO_0_REGS,
             GPIO_INSTANCE_PIN_NUMBER_7,
             GPIO_DIR_OUTPUT);

    /*DDR_VTT_EN - GPIO0_7  - configured as HIGH*/
    GPIOPinWrite(SOC_GPIO_0_REGS,GPIO_INSTANCE_PIN_NUMBER_7,GPIO_PIN_HIGH);

}

/*
** Dummy ISR to handle spurious interrupts
*/
static void dummyIsr(void)
{
    ; /* Perform nothing */
}

/*
** Uart ISR to read the inputs
*/
static void uartIsr(void)
{
  volatile unsigned char rxByte;
    ; /* Perform nothing */
  rxByte = UARTCharGetNonBlocking(SOC_UART_0_REGS);
  UARTCharPutNonBlocking(SOC_UART_0_REGS, rxByte);
}


int EthernetPorttest(unsigned int port)
{
  int err=0;


#ifdef MMU_CACHE_ENABLE
  // Disable Data Cache
    CacheDisable(CACHE_ALL);
    CP15MMUDisable();
#endif
  //glinkDetectF=0;

  if((port!=1)&&(port!=2))
  {
    UARTPuts("\nWrong port selected\n",-1);
    return -1;
  }

  EthernetInit();

  UARTPuts("\nEthernet - RGMII",-1);
  UARTPutNum(port);
  UARTPuts(" test \n", -1);

  UARTPuts("\nPort",-1);
  UARTPutNum(port);
  UARTPuts(" 10M test\n",-1);

  err=EMACTest_10M(port);

  if (FAIL_DIAG == err)
  {
    UARTPuts("\nPort",-1);
    UARTPutNum(port);
    UARTPuts(" 10M test failed\n",-1);

      return err;
  }


  UARTPuts("\nPort",-1);
  UARTPutNum(port);
  UARTPuts(" 100M test\n",-1);

  err=EMACTest(port);
  if (FAIL_DIAG == err)
  {
    UARTPuts("\nPort",-1);
    UARTPutNum(port);
    UARTPuts(" 100M test failed\n",-1);
      return err;

  }

  UARTPuts("\nPort",-1);
  UARTPutNum(port);
  UARTPuts(" 1000M test\n",-1);

  err=EMACTest_GigaBit(port);
  if (FAIL_DIAG == err)
  {
    UARTPuts("\nPort",-1);
    UARTPutNum(port);
    UARTPuts(" 1000M test failed\n",-1);

      return err;
  }

  /*reset the PHY*/
  PhyReset(MDIO_BASE,(port-1));
  /* Reset the EMAC Switch Subsystem */
  CPSWSSReset(EMAC_BASE);
  /* Reset the EMAC */
  CPSWSlReset(EMAC_MAC1_BASE);
  CPSWSlReset(EMAC_MAC2_BASE);
  /* Reset the DMAs */
  CPSWCPDMAReset(EMAC_DMA_BASE);

#ifdef MMU_CACHE_ENABLE
  MMUConfigAndEnable();

  /* Enable cache */
  CacheEnable(CACHE_ALL);
#endif

  return 0;

}


void Highlight_line_testno(void)
{
  UARTPuts("\n\n*************************************************************\n\n", -1);
}

void Highlight_line_result(void)
{
  UARTPuts("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n", -1);
}

int EvmSK_DiagTestSelect(int testOption)
{
  int err=0;      //return variable
  unsigned char uart_char=0; //console read for keypad test quit
  unsigned int timeout_loop=0;
  unsigned char loopvar=0;
  int countdown=10;
  int KeyFail=-1;

    switch(testOption)
    {
    case DDR3_SELECT:
      /* DDR test */
#ifdef MMU_CACHE_ENABLE
      // Disable Data Cache
      CacheDisable(CACHE_ALL);
      CP15MMUDisable();
#endif
      Highlight_line_testno();
      UARTPuts("\n1. DDR3 DMA Test Test", -1);
      Highlight_line_testno();

      err=LPDDRTest();
      Highlight_line_result();
      if(err<0)
      {
             UARTPuts("\n1. DDR3 dataline Test FAIL", -1);
      }
      else
           UARTPuts("\n1. DDR3 dataline Test PASS", -1);

      Highlight_line_result();

      err=DDR_DMATest();

      Highlight_line_result();
      if(err<0)
      {
        gFailF++;
          UARTPuts("\n1. DDR3 pattern Test FAIL", -1);
      }
      else
      {
        gPassF++;
        UARTPuts("\n1. DDR3 DMA Test PASS", -1);
      }

      Highlight_line_result();

#ifdef MMU_CACHE_ENABLE
      MMUConfigAndEnable();

      /* Enable cache */
      CacheEnable(CACHE_ALL);
#endif


      break;

    case I2C_EEPROM_SELECT:
      Highlight_line_testno();
      UARTPuts("\n2. EEPROM Test", -1);
      Highlight_line_testno();

      E2prominit();

      err=E2promtest();
      Highlight_line_result();
      if(err==-1)
      {
        UARTPuts("\n2. EEPROM Read/Write FAIL", -1);
        gFailF++;
      }
      else
      {
        UARTPuts("\n2. EEPROM Read/Write PASS", -1);
        gPassF++;
      }
      Highlight_line_result();

      break;

    case ACCELEROMETER_SELECT:

          Highlight_line_testno();
          UARTPuts("\n3. Accelerometer Test", -1);
          Highlight_line_testno();

          err=Accelerometer_id_test();

          Highlight_line_result();
          if(err==-1)
          {
              UARTPuts("\n3. Accelerometer ID test FAIL", -1);
              gFailF++;
          }
          else
          {
            UARTPuts("\n3. Accelerometer ID test PASS", -1);
            gPassF++;
          }

          Highlight_line_result();


          break;
    case MICROSD_SELECT:

      if(!gmicroSD_onceF)
      {
          gmicroSD_onceF=1;

            Highlight_line_testno();
            UARTPuts("\n4. microSD Test", -1);
            Highlight_line_testno();

            hs_mmcsd_test(0);

            Highlight_line_result();

            UARTPuts("\n4. microSD Test result is: ", -1);
              if(gmicroSD_statusF)
              {
                UARTPuts("PASS", -1);
                 gPassF++;
              }
              else
              {
                UARTPuts("FAIL", -1);
                 gFailF++;
              }
              Highlight_line_result();
      }
      else
      {
        Highlight_line_result();
        UARTPuts("\n4. microSD Test was already executed and the result is: ", -1);

         if(gmicroSD_statusF)
         {
           UARTPuts("PASS", -1);
             gPassF++;
         }
         else
         {
            UARTPuts("FAIL", -1);
            gFailF++;
         }

        Highlight_line_result();
      }


            break;

    case USB1_HOST_SELECT:

      if(!gusb1_host_onceF)
      {
#ifndef MMU_CACHE_ENABLE
        MMUConfigAndEnable();
        /* Enable cache */
        CacheEnable(CACHE_ALL);
#endif
        Highlight_line_testno();
        UARTPuts("\n5. USB1 Host Test", -1);
        Highlight_line_testno();

        usb_flag=1;
        usb_host_msc_init(); // USB1 Host init
        usb_host_msc_test();
        usb_flag=0;
        gusb1_host_onceF=1;

        Highlight_line_result();
        UARTPuts("\n5. USB1 Host Test result is: ", -1);
          if(gusb1_host_statusF)
          {
            UARTPuts("PASS", -1);
             gPassF++;
          }
          else
          {
            UARTPuts("FAIL", -1);
             gFailF++;
          }

          Highlight_line_result();

#ifndef MMU_CACHE_ENABLE
          // Disable Data Cache
         CacheDisable(CACHE_ALL);
         CP15MMUDisable();
#endif


      }
      else
      {
        Highlight_line_result();
        UARTPuts("\n5. USB1 Host Test was already executed and the result is: ", -1);
        if(gusb1_host_statusF)
          UARTPuts("PASS", -1);
        else
          UARTPuts("FAIL", -1);
        Highlight_line_result();
      }

        break;



    case PMIC_SELECT:
      Highlight_line_testno();
      UARTPuts("\n6. PMIC Test", -1);
      Highlight_line_testno();
      err=Pmic_test();

      Highlight_line_result();
      if(err==-1)
      {
        gFailF++;
        UARTPuts("\n6. PMIC test FAIL", -1);
      }
      else
      {
        gPassF++;
        UARTPuts("\n6. PMIC test PASS", -1);
      }
      Highlight_line_result();

      break;

    case ENET_GIGABIT_1_SELECT:
          Highlight_line_testno();
          UARTPuts("\n7. Ethernet Port1 Test", -1);
          Highlight_line_testno();

          /*Ethernet gigabit port 1 test*/
          err=EthernetPorttest(1);
          if(!err)
            gPassF++;
          else
            gFailF++;

          break;

    case ENET_GIGABIT_2_SELECT:
          Highlight_line_testno();
          UARTPuts("\n8. Ethernet Port2 Test", -1);
          Highlight_line_testno();

            /*Ethernet gigabit port 2 test*/
          err=EthernetPorttest(2);
          if(!err)
            gPassF++;
          else
            gFailF++;

            break;

    case LCD_SELECT:

#ifdef MMU_CACHE_ENABLE
      // Disable Data Cache
      CacheDisable(CACHE_ALL);
      CP15MMUDisable();
#endif
      Highlight_line_testno();
      UARTPuts("\n9. LCD Test - Requires User verification", -1);
      Highlight_line_testno();

      Lcdtest();

#ifdef MMU_CACHE_ENABLE
      MMUConfigAndEnable();

      /* Enable cache */
      CacheEnable(CACHE_ALL);
#endif

        break;

    case TOUCH_SCREEN_SELECT:

#ifdef MMU_CACHE_ENABLE
      // Disable Data Cache
      CacheDisable(CACHE_ALL);
      CP15MMUDisable();
#endif
      Highlight_line_testno();
      UARTPuts("\n10. Touch screen Test - Requires User verification", -1);
      Highlight_line_testno();

#ifndef CAPTOUCH
      SetUpLCD();
      ClearLCD();
      LCDCDrawBorder();

      SquareBoxDraw(0,0,10,0xFF,0xFF,0xFF);     //left top
      SquareBoxDraw(0,262,10,0xFF,0xFF,0xFF);   //left bottom
      SquareBoxDraw(470,0,10,0xFF,0xFF,0xFF);   //right top
      SquareBoxDraw(470,262,10,0xFF,0xFF,0xFF); //right bottom
      SquareBoxDraw(235,131,10,0xFF,0xFF,0xFF);   //center


      err=tsc_test();

#else
      err=CapTouchDiag();
#endif

      Highlight_line_result();

      if(!err)
      {
        UARTPuts("\n10. Touch screen Test - PASS", -1);
        gPassF++;
      }
      else
      {
        UARTPuts("\n10. Touch screen Test - FAIL", -1);
        gFailF++;
      }

      Highlight_line_result();

#ifndef CAPTOUCH
      ClearLCD();
      EcapBkLightVary(LOWBACKLIGHT);
      DisableBackLight();
#endif

#ifdef MMU_CACHE_ENABLE
      MMUConfigAndEnable();

        /* Enable cache */
        CacheEnable(CACHE_ALL);
#endif

        break;

    case AUDIO_CODEC_SELECT:
      Highlight_line_testno();


      UARTPuts("\n11. Audio Test - Requires User verification", -1);
      Highlight_line_testno();


      /*Audio related interfaces initialization*/
        //McASP1ModuleClkConfig();
        //McASP1PinMuxSetup();

        //AudioCodecInit();
        /*Audio codec, I2S and DMA iniatilisation*/
        ToneLoopInit();
        /* Start playing the tone */
        ToneLoopStart();

        timeout_loop=0;
        do
        {

            if(UARTCharsAvail(UART_CONSOLE_BASE))
          {
            uart_char = UARTGetc();
          }

            delay(1);
          timeout_loop++;
          if(timeout_loop==5000) //5 seconds 500000)
            break;

        }while(uart_char!=ESC);

        ToneLoopdeInit();

        break;

    case USER_LED_SELECT:
      Highlight_line_testno();
      UARTPuts("\n12. User LED Test - Requires User verification", -1);
      Highlight_line_testno();

      UserLedtest();
        break;

    case USER_KEYPAD_SELECT:

      Highlight_line_testno();
      UARTPuts("\n13. Keypad test", -1);
      Highlight_line_testno();
      UARTPuts("\n", -1);

      KeyFail=-1;
      gKey=0;

      GPIOINTCConfigure(); //GPIO Interrupt enable

      for(loopvar=0;loopvar<4;loopvar++)
      {
        timeout_loop=0;
        countdown=10;

        UARTPuts("\nPress User Key ", -1);
        UARTPutNum(loopvar+1);
        UARTPuts("\n", -1);

        while(!gKey&&(countdown>0))
        {
          delay(1);
          timeout_loop++;

          if(timeout_loop==1000)
          {
            UARTPuts("\rPress within ", -1);
            UARTPutNum(countdown);
            UARTPuts("  seconds", -1);
            timeout_loop=0;
            countdown--;
          }

        }

        Highlight_line_result();

        if(gKey)
        {
          KeyFail=Checkforkeypress(loopvar); //Key Identification and respective LED glow
        }
        else
        {
          UARTPuts("\nKey Not Pressed - FAIL", -1);
          KeyFail=-1;

        }

        Highlight_line_result();

      }


      if(!KeyFail)
        gPassF++;
      else
        gFailF++;


      /* Disable the GPIO0 system interrupt in INTC. */
      IntSystemDisable(GPIO_SYS_INT_NUM_SW1_2_4);
      IntSystemDisable(GPIO_SYS_INT_NUM_SW3);

      IntUnRegister(GPIO_SYS_INT_NUM_SW1_2_4);
      IntUnRegister(GPIO_SYS_INT_NUM_SW3);

        break;


    case INTERRUPT_KEY_SELECT:
      Highlight_line_testno();
      UARTPuts("\n14. Interrupt Key Test", -1);
      Highlight_line_testno();

      gNmiF=0;

      //initNMIEvents();
      IntSystemEnable(SYS_INT_NMI);

      UARTPuts("\nPress the Interrupt Key", -1);
      UARTPuts("\n", -1);
      timeout_loop=0;
      countdown=10;

      while(!gNmiF&&(countdown>0))
      {
        delay(1);
        timeout_loop++;
        if(timeout_loop==1000)
        {
          UARTPuts("\rPress within ", -1);
          UARTPutNum(countdown);
            UARTPuts("  seconds ", -1);
          timeout_loop=0;
          countdown--;
        }


      }

      Highlight_line_result();

      if(gNmiF==1)
      {
        UARTPuts("\n14. Interrupt Key Test PASS", -1);
        gPassF++;
      }
      else
      {
        UARTPuts("\n14. Interrupt Key Test FAIL", -1);
        IntSystemDisable(SYS_INT_NMI);
        gFailF++;
      }

       /* UnRegister the ISR */
        //IntUnRegister(SYS_INT_NMI);


      Highlight_line_result();

        break;

    case USB0_DEVICE_SELECT:


      if(!gusb0_dev_onceF)
      {
#ifndef MMU_CACHE_ENABLE
        MMUConfigAndEnable();
        /* Enable cache */
        CacheEnable(CACHE_ALL);
#endif

        Highlight_line_testno();
        UARTPuts("\n15. USB0 Device Test-Press ESC to exit", -1);
        Highlight_line_testno();

        usb_dev_msc_init(); // USB0 Dev init
        err=usb_dev_msc_test();
        gusb0_dev_onceF=1;

        Highlight_line_result();
        UARTPuts("\n15. USB0 Device Test result is: ", -1);

          if(!err)
          {
            UARTPuts("PASS", -1);
              gusb0_dev_statusF=1;
              gPassF++;
          }
          else
          {
            UARTPuts("FAIL", -1);
            gFailF++;
          }



          Highlight_line_result();

#ifndef MMU_CACHE_ENABLE
          // Disable Data Cache
          CacheDisable(CACHE_ALL);
          CP15MMUDisable();
#endif


      }
      else
      {
        Highlight_line_result();
        UARTPuts("\n15. USB0 Device Test was already executed and the result is: ", -1);

        if(gusb0_dev_statusF)
          UARTPuts("PASS", -1);
        else
          UARTPuts("FAIL", -1);
        Highlight_line_result();

      }



      break;
    case PM_DS0_SELECT:


#ifdef MMU_CACHE_ENABLE
          // Disable Data Cache
          CacheDisable(CACHE_DCACHE);
          CP15MMUDisable();
#endif

      Highlight_line_result();
      UARTPuts("\n16. PM-DS0-Test", -1);
      UARTPuts("\nLCD pattern displayed before Entering PM-DS0", -1);
      Highlight_line_result();

      Lcdtest_gradient();

        /*DMA iniatilisation*/
        ToneLoopInit();
        /* Start playing the tone */
        ToneLoopStart();

        delay(2500);

      ActionDeepSleep0();

      Highlight_line_result();
      UARTPuts("\n16. PM-DS0-SUCCESS, check for LCD Display pattern", -1);
      Highlight_line_result();


      delay(2500);
      ClearLCD();
      EcapBkLightVary(LOWBACKLIGHT);
      DisableBackLight();

      ToneLoopdeInit();


#ifdef MMU_CACHE_ENABLE
      MMUConfigAndEnable();
      /* Enable cache */
      CacheEnable(CACHE_DCACHE);
#endif
      break;

    default:
      UARTPuts("\nWrong Test Option Selected", -1);
      break;
    }

    return 0;
}

int EvmSKDiagMenuDisplay(unsigned char idMemDisp)
{
  int retVal=-1;

  UARTPuts("\n\n\n\n\n\n",-1);
  if(idMemDisp)
  {
    retVal = IdDisplayConfiguration();

    if(retVal!=0)
    {
      UARTPuts("\n\nID Memory is wrongly or not programmed",-1);
      UARTPuts("\nProgram the ID Memory with correct values and then run diagnostics", -1);
      return -1;
    }
  }
  Highlight_line_testno();
  UARTPuts("\n              SK3358 Diagnostics Test v", -1);
  UARTPuts(VERSION,sizeof(VERSION));
  Highlight_line_testno();
  UARTPuts("\n 0-Auto Diagnostics Test Number 1-8 ",-1);
  UARTPuts("\n 1-DDR3",-1);
  UARTPuts("\n 2-I2C EEPROM",-1);
  UARTPuts("\n 3-Accelerometer",-1);
  UARTPuts("\n 4-microSD",-1);
  UARTPuts("\n 5-USB1 Host port",-1);
  UARTPuts("\n 6-PMIC",-1);
  UARTPuts("\n 7-Ethernet Gigabit Port 1",-1);
  UARTPuts("\n 8-Ethernet Gigabit Port 2",-1);
  UARTPuts("\n 9-LCD",-1);
  UARTPuts("\n10-Touch screen",-1);
  UARTPuts("\n11-Audio codec",-1);
  UARTPuts("\n12-User LED",-1);
  UARTPuts("\n13-User Keypad",-1);
  UARTPuts("\n14-Interrupt Key",-1);
  UARTPuts("\n15-USB Device port",-1);
  UARTPuts("\n16-Power Management - DS0",-1);
  UARTPuts("\n17-Return to Main Menu",-1);


  UARTPuts("\nEnter your test option: ", -1);

  return 0;
}

void EvmSKDiag_MainMenuDisplay(void)
{
  UARTPuts("\n\n\n\n\n\n",-1);
  Highlight_line_testno();
  UARTPuts("\n     Main Menu - SK3358 Diagnostics Test v", -1);
  UARTPuts(VERSION,sizeof(VERSION));
  Highlight_line_testno();
  UARTPuts("\n 1-ID Memory Programming",-1);
  UARTPuts("\n 2-Diagnostics",-1);

  UARTPuts("\nEnter your test option: ", -1);
}

/*
** Main function. The application starts here.
*/
int main(void)
{
  unsigned char mainMenuDisplayF=0, diagMenuDisplayF=0,TestMenu=0; //Flag to manage the menu display
  int testOpt=0;
  int iloop=0;
  int retVal=-1;

#ifdef __TMS470__
      /* Relocate the required section to internal RAM */
      memcpy((void *)(&relocstart), (const void *)(&iram_start),
          (unsigned int)(&iram_size));
#elif defined(__IAR_SYSTEMS_ICC__)
  #pragma section = "CodeRelocOverlay"
  #pragma section = "DataRelocOverlay"
  #pragma section = "DataOverlayBlk"
  #pragma section = "CodeOverlayBlk"
      char* srcAddr = (__section_begin("CodeRelocOverlay"));
      char* endAddr = (__section_end("DataRelocOverlay"));

      memcpy((void *)(__section_begin("CodeRelocOverlay")),
          (const void *)(__section_begin("CodeOverlayBlk")),
          endAddr - srcAddr);

#else
      memcpy((void *)&(relocstart), (const void *)&(iram_start),
          (unsigned int)(((&(relocend)) -
              (&(relocstart))) * (sizeof(unsigned int))));
#endif


#ifdef MMU_CACHE_ENABLE
  MMUConfigAndEnable();

  /* Enable cache */
  CacheEnable(CACHE_ALL);
#endif
  gusb0_dev_onceF=0;
  gusb1_host_onceF=0;
  gmicroSD_onceF=0;

  usb_flag=0;

    PeripheralsSetUp();

    /*LCD Disable Backlight*/
    DisableBackLight();

    /* Initialize the ARM Interrupt Controller */
    IntAINTCInit();

    /* Register the ISRs */
    IntRegister(127, dummyIsr);

    IntMasterIRQEnable();

    IntSystemEnable(127);
    IntPrioritySet(127, 0, AINTC_HOSTINT_ROUTE_IRQ);

    /*NMI Interrupt enable*/
    initNMIEvents();

    /*start- essentials for the PM-DS0*/
    CM3IntRegister();

    IntPrioritySet(SYS_INT_M3_TXEV, 0, AINTC_HOSTINT_ROUTE_IRQ );
    IntSystemEnable(SYS_INT_M3_TXEV);

    IntSystemEnable(SYS_INT_UART0INT);
    IntPrioritySet(SYS_INT_UART0INT, 0, AINTC_HOSTINT_ROUTE_IRQ);
    IntRegister(SYS_INT_UART0INT, uartIsr);

    CM3EventsClear();
    CM3LoadAndRun();
    waitForM3Txevent();

    /*end- essentials for the PM-DS0*/

    DelayTimerSetup();

    EcapInit();

    /* UART initialisation 1-HW Flow enabled, 0- No HW Flow */
     UARTStdioInit();
     UARTConfigureHWFlowControl(UART_CONSOLE_BASE,1);


    Raster0Init();

    I2CIntRegister(I2C_0);
    IntPrioritySet(SYS_INT_I2C0INT, 0, AINTC_HOSTINT_ROUTE_IRQ );
    IntSystemEnable(SYS_INT_I2C0INT);
    I2CInit(I2C_0);

    AudioCodecInit();
    //ToneLoopInit();

    configVddOpVoltage();

    //EthernetInit();

    MailBoxInit();

    mainMenuDisplayF=1;
    diagMenuDisplayF=0;

    while(1)
    {
          /*Application Live Status*/
          /* Driving GPIO1[4-7] pin to logic alternative HIGH and LOW */
          UserLedOnOff(LED_1,LED_ON);
          UserLedOnOff(LED_2,LED_OFF);
          UserLedOnOff(LED_3,LED_ON);
          UserLedOnOff(LED_4,LED_OFF);

          delay(100);

          UserLedOnOff(LED_1,LED_OFF);
          UserLedOnOff(LED_2,LED_ON);
          UserLedOnOff(LED_3,LED_OFF);
          UserLedOnOff(LED_4,LED_ON);

          delay(100);

          if(mainMenuDisplayF) //Main Menu
          {
            EvmSKDiag_MainMenuDisplay();
            mainMenuDisplayF=0;
            TestMenu=1;

          }
          else if(diagMenuDisplayF)
            {
              if(diagMenuDisplayF==1)
              {
                retVal=EvmSKDiagMenuDisplay(1);

                if(retVal!=0)  //if ID Memory is not programmed go to main menu
                {
                  mainMenuDisplayF=1;
                  diagMenuDisplayF=0;
                  continue;
                }

              }
              else
                EvmSKDiagMenuDisplay(0);

              diagMenuDisplayF=0;
              TestMenu=2;
            }

          if(UARTCharsAvail(UART_CONSOLE_BASE))
          {
              testOpt = UARTGetNum();
              UARTPuts("\n\n", -1);
              if(TestMenu==1) //Main Menu test option selection
              {
                switch(testOpt)
                {
                case 1: //ID Memory programming
                  userInput();
                  mainMenuDisplayF=1;
                  break;
                case 2: //Diag Menu select
                  diagMenuDisplayF=1;
                  break;
                default:
                  UARTPuts("\nWrong Test Option Selected", -1);
                  mainMenuDisplayF=1;
                  break;
                }

              }
              else //Diagnostics Menu test option selection
              {

                if(testOpt==0)
                {
                  gPassF=0;
                  gFailF=0;

                  for(iloop=1;iloop<=8;iloop++)
                  {
                    EvmSK_DiagTestSelect(iloop);

                    if(UARTCharsAvail(UART_CONSOLE_BASE))
                    {
                      testOpt = UARTGetc();
                      if(testOpt==ESC)
                      {
                        UARTPuts("\nDiagnostics Test stopped", -1);
                        break;
                      }
                    }


                  }

                  Highlight_line_result();
                  UARTPuts("\nTotal No. of Automated Tests: 8", -1);
                  UARTPuts("\nTotal Test Passed : ", -1);
                  UARTPutNum(gPassF);
                  UARTPuts("\nTotal Test Failed : ", -1);
                  UARTPutNum(gFailF);
                  UARTPuts("\nPlease refer the log for creating the report", -1);
                  UARTPuts("\nTests like LCD, Touchscreen, Audio, LED, Keypad, Interrupt Key USB0 Device requires user verification", -1);

                  Highlight_line_result();

                }
                else if(testOpt==RETURN_TO_MAIN_SELECT)
                    mainMenuDisplayF=1;
                else
                  EvmSK_DiagTestSelect(testOpt);

                if(testOpt!=RETURN_TO_MAIN_SELECT) // Returning to main menu
                {
                  diagMenuDisplayF=2;
                }
              }

          }


    }

}


/*
** This function provides a delay for the specified count value.
*/

void Delay(volatile unsigned int count)
{
    while(count--);
}

/*
** Initialize M3 interrupts
*/
static void initNMIEvents(void)
{
    /* Register the ISR in the Interrupt Vector Table.*/
    IntRegister(SYS_INT_NMI, NMIIsr);

    IntPrioritySet(SYS_INT_NMI, 0, AINTC_HOSTINT_ROUTE_IRQ );

    /* Enable the System Interrupts for AINTC.*/
    //IntSystemEnable(SYS_INT_NMI);

    IntSystemDisable(SYS_INT_NMI);

  /*  Clear NMI events if any */
  //clearNMIEvents();
}

#if 0
/*
** Clear CM3 event and re-enable the event
*/
static void clearNMIEvents(void)
{

}
#endif

/*
** NMI ISR handler
*/
static void NMIIsr(void)
{
  UARTPuts("\nInterrupt key is pressed\n", -1);

  /* Enable the System Interrupts for AINTC.*/
  IntSystemDisable(SYS_INT_NMI);

  gNmiF=1;
}


/****************************** End of file *********************************/

