/*
 * IDConfiguration.c
 *
 * EEProm ID configuration.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "hsi2c.h"
#include "evmskAM335x.h"
#include "evmskam335x_diag.h"
#include "interrupt.h"
#include "uartStdio.h"
#include "soc_AM335x.h"
#include "IDConfiguration.h"
#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"
#include "string.h"
#include "delay.h"
#include "I2cApi.h"

#define AM335x 1
#define CONFIRM 0
#define EEPROM_DATA_READ 0

INT8 userData[USER_DATA_LEN];
char gBoardSNo[12];

/******************************************************************************
**              GLOBAL VARIABLE DEFINITIONS
******************************************************************************/
extern volatile unsigned char dataFromSlave[I2C_INSTANCE][60]; //2];
extern volatile unsigned char dataToSlave[I2C_INSTANCE][60]; //3];
extern volatile unsigned int  tCount[I2C_INSTANCE];
extern volatile unsigned int  rCount[I2C_INSTANCE];
extern volatile unsigned int  flag[I2C_INSTANCE];
extern volatile unsigned int  numOfBytes[I2C_INSTANCE];

extern void SetupI2CTransmit(unsigned int dcount, unsigned int instNum);
extern void SetupE2PROMReception(unsigned int dcount, unsigned int instNum);

extern signed char gmicroSD_onceF;

/****************************************************************************
 * dec2ASCII
 *
 *  This function is used to Display MAC ADRESS for different Ethernet, This
 *  function is used only for the base boards as these boards are with MAC
 *  supports
 *   *
 * @parameter
 *  UINT8 a              : INPUT : FIRST NUMBER    85
 *  UINT8 *retdata       : OUTUT : ASCII       ADD0>> 5 ADD1>> 5  >> 0X55
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 */
INT16 dec2ASCII
(
    UINT8 a,
    UINT8 *retData
)
{
    UINT8 numberA;
    UINT8 numberB;

    numberA = a & 0xF0;
    numberB = a & 0x0F;

    numberA = numberA >> 4;

    if(10 == numberA)
        numberA = 'A';
    else if(11 == numberA)
        numberA = 'B';
    else if(12 == numberA)
        numberA = 'C';
    else if(13 == numberA)
        numberA = 'D';
    else if(14 == numberA)
        numberA = 'E';
    else if(15 == numberA)
        numberA = 'F';
    else
       numberA = numberA + 48;

    if(10 == numberB)
        numberB = 'A';
    else if(11 == numberB)
        numberB = 'B';
    else if(12 == numberB)
        numberB = 'C';
    else if(13 == numberB)
        numberB = 'D';
    else if(14 == numberB)
        numberB = 'E';
    else if(15 == numberB)
        numberB = 'F';
    else
        numberB = numberB + 48;
    *retData = numberA;
     retData = retData + 1;
     *retData = numberB;
     return SUCCESS_DIAG;

}


/****************************************************************************
 * userInput
 *
 *  This routine takes the INPUT from user to select for different task.
 *
 * @parameter
 *      void
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   FAIL_DIAG
 */

INT16 userInput(void)
{
    INT16 retVal;
    INT32 selectNumber;
    char idmContents[5][12];/*0-idmHeader,1-idmBoardName,2-idmVersion,3-idmSerial,4-idmConfig*/
    int loop=0,cnt=0,str=0; //loop variable


    if(!gmicroSD_onceF)
    {
      gmicroSD_onceF=1;
      retVal = hs_mmcsd_test(1); //ID Memory config read

      if(retVal!=-1)
      {
        for(loop=0;userData[loop]!='\0';loop++)
        {

        if((userData[loop]==CR)&&(userData[loop+1]==LF))
        {
          idmContents[str][cnt] = '\0';
          str++; //string increment
          cnt=0;
          loop++; //skip the Line Feed character
        }
        else
        {
          idmContents[str][cnt] =  userData[loop];
          cnt++; //string char increment
        }

        }

        idmContents[str][cnt] = '\0';

        UARTPuts("\n\nID Memory Contents Read from the config file from microSD\n",-1);
        for(loop=0;loop<5;loop++)
        {
          UARTPuts("\n",-1);
          UARTPuts(idmContents[loop],-1);
        }


        if(!(strcmp(idmContents[ID_MEM_HEADER],BOARD_HEADER))&&!(strcmp(idmContents[ID_MEM_BOARDNAME],BOARD_NAME)))
        {
          /* Program the ID Memory Header */
          strcpy(userData,idmContents[ID_MEM_HEADER]);
          retVal = getIdHeader();
          if(FAIL_DIAG == retVal)
          {
            UARTPuts("\n\nHeader : FAIL_DIAG",-1);
          }

          /* Program the ID Memory Board Name */
          strcpy(userData,idmContents[ID_MEM_BOARDNAME]);
          retVal = getIdBName();
          if(FAIL_DIAG == retVal)
          {
            UARTPuts("\n\nName: FAIL_DIAG",-1);
          }

          /* Program the ID Memory Version */
          strcpy(userData,idmContents[ID_MEM_VERSION]);
          retVal = getVersion();
          if(FAIL_DIAG == retVal)
          {
            UARTPuts("\n\nVersion : FAIL_DIAG",-1);
          }

          /* Program the ID Memory Configuration */
          strcpy(userData,idmContents[ID_MEM_CONFIG]);

          retVal = getConfiguration();

          if(FAIL_DIAG == retVal)
          {
            UARTPuts("\n\nConfiguration : FAIL_DIAG",-1);
          }

          userData[0]='\0';
          retVal = getSerial();
          if(FAIL_DIAG == retVal)
          {
            UARTPuts("\n\nSerial number Scan Failed",-1);
          }
          else
          {
            IdDisplayConfiguration();
            return 0;
          }
        }
        else
        {
          UARTPuts("\n\nWrong ID Memory Header/board name\n",-1);
          UARTPuts("\nManual entry of ID Memory contents required\n",-1);
        }

      }
      else
      {
        UARTPuts("\n\nmicroSD card not detected/ID Memory config file not present\n",-1);
        UARTPuts("\nManual entry of ID Memory contents required\n",-1);
      }

    }
    else
    {
      UARTPuts("\n\nmicroSD mount was already executed\n",-1);
      UARTPuts("\nFor automated ID Memory programming perform a reset\n",-1);
      UARTPuts("\nManual entry of ID Memory contents required\n",-1);
    }



    /*Welcome Screen with Board Description*/
    while(1)
    {
      UARTPuts("\n\n\n\n ******************************************",-1);
      UARTPuts("\n       BOARD ID CONFIGURATION SETTINGS     \n",-1);
      UARTPuts("\n ***************************************** \n",-1);
      UARTPuts("\n\n         TMDXEVM3358-SK Board            \n",-1);

      UARTPuts("\n\n\n 1. Show Configurations \n\n\n 2. Edit ",-1);
      UARTPuts("Configurations \n\n\n 3. Any Other Key to Cancel ",-1);
      UARTPuts("\n\n\n Enter Your Choice :  ",-1);
      selectNumber=UARTGetNum();


      if(selectNumber < 3 && selectNumber >= 1)
      {

        if(selectNumber==1)
        {
          retVal = IdDisplayConfiguration();
        }
        else
        {
          UARTPuts("\n 1. Edit All\n 2. Header Only\n 3. Board Name ",-1);
          UARTPuts("\n 4. Board Version ",-1);
          UARTPuts("\n 5. Board Serial Number",-1);
          UARTPuts("\n 6. Board Configuration",-1);
          UARTPuts("\n 7. Any Other Key to cancel",-1);

          selectNumber = 0;

          /*Getting  User Input and Confirmation*/
          UARTPuts("\n\n\n Enter Your Choice :  ",-1);
          selectNumber=UARTGetNum();

          if (selectNumber >= 7 || selectNumber < 1)
          {
            retVal = SUCCESS_DIAG;
            UARTPuts("\n User Selected to cancel  ",-1);
          }
          else if(selectNumber <= 6)
          {
            switch (selectNumber)
            {
            case 1 :
              retVal = getIdHeader();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nHeader : FAIL_DIAG",-1);
              }

              retVal = getIdBName();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nName: FAIL_DIAG",-1);
              }

              retVal = getVersion();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nVersion : FAIL_DIAG",-1);
              }

              retVal = getSerial();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nSerial : FAIL_DIAG",-1);
              }

              retVal = getConfiguration();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nConfiguration : FAIL_DIAG",-1);
              }

              break;
            case 2 :
              retVal = getIdHeader();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nHeader : FAIL_DIAG",-1);
              }
              break;
            case 3 :
              retVal = getIdBName();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nName : FAIL_DIAG",-1);
              }
              break;

            case 4 :
              retVal = getVersion();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nVersion : FAIL_DIAG",-1);
              }
              break;
            case 5 :
              retVal = getSerial();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nSerial : FAIL_DIAG",-1);
              }
              break;
            case 6 :
              retVal = getConfiguration();
              if(FAIL_DIAG == retVal)
              {
                UARTPuts("\n\nConfiguration : FAIL_DIAG",-1);
              }
              break;
            default:
              break;
            }
          }
        }
      }
      else
        break;

    }


    return retVal;  //0
}

/*
 ** Reads data from a specific address of e2prom
*/
static INT16 EEPROMRead(UINT16 eepromAddr,UINT8 *data,UINT8 length)
{
      unsigned int i;

        dataToSlave[I2C_0][0] = ((eepromAddr&0xFF00)>>8);
        dataToSlave[I2C_0][1] = (eepromAddr&0x00FF);

        for (i = 0; i < length; i++ )
        {
             dataFromSlave[I2C_0][i]=0;
        }

        tCount[I2C_0] = 0;
        rCount[I2C_0] = 0;

        SetupE2PROMReception(length,I2C_0);

        for (i = 0; i < length; i++ )
        {
             data[i] = dataFromSlave[I2C_0][i];
        }

        return SUCCESS_DIAG;

}


/***************************************************************
 * EEPROMWrite
 *
 * This function is for Writing the ID Memory configuration to EEPROM
 *
 * @parameter
 *
 *
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   FAIL_DIAG
 */
static INT16 EEPROMWrite
(
    UINT16 eepromAddr,
    UINT8 *data,
    UINT8 length
)
{
    unsigned int i;
    unsigned int totalnoB=length+2; //includes 2 byte address

    dataToSlave[I2C_0][0] = ((eepromAddr&0xFF00)>>8);
    dataToSlave[I2C_0][1] = (eepromAddr&0x00FF);

    for (i = 2; i < (length+2); i++ )
    {
         dataToSlave[I2C_0][i]=data[i-2];
    }


    tCount[I2C_0] = 0;
    SetupI2CTransmit(totalnoB,I2C_0);

    return SUCCESS_DIAG;
}

/****************************************************************************
 * IdDisplayConfiguration
 *
 *  This function is used for Display the Configuration setting in ID-MEMORY
 *  Function reads the content and display it on the Monitor to confirm.
 *  It ask
 *  from user to confirm data written in ID-MEMORY is correct or not.
 *
 *  INFORMATION it Shows Like
 *
 *           HEADER NAME
 *           NAME
 *           VERSION NUMBER
 *           SERIAL NUMBER
 *           CONFIGURATION
 *           SERIAL NUMBER
 *           ETH MAC ADD , For the Selected Board
 *
 * @parameter
 *
 *
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 *   FAIL_DIAG
 */
INT16 IdDisplayConfiguration(void)
{
    INT16 retVal;
    UINT8 IdMemoryData[120];
    INT8 boardHeader[8];
    UINT8 temp3 = 0;


    /* DISPLAYING BOARD ID MEMORY DATA  */
    userData[0] =0;
    userData[1] =0;
    userData[2] =0;

    /*After Data Validation initialization of ID-MEMORY     */
    E2prominit();

    /*After Data Validation initialization of ID-MEMORY     */

    UARTPuts("\n**********************************************\n",-1);
    //UARTPuts("\n**********************************************\n",-1);

    UARTPuts("\n TMDXEVM3358-SK Board \n",-1);

    /*Reading Header From ID MEMORY */
    UARTPuts("\n \n******** Reading From ID MEMORY  ***** ",-1);
    retVal = EEPROMRead(ADD_ID_MEMORY_HEADER, IdMemoryData, LAST_ADDRESS);
    if(SUCCESS_DIAG != retVal)
  {
      UARTPuts("\n\n \n CHECK PROPER BOARD SELECTED\n ERROR ",-1);
      UARTPuts("IN GETING HEADER FROM ID-MEMORY \n",-1);
      return retVal;
  }


  retVal = dec2ASCII(IdMemoryData[0],(UINT8*) &userData[0]);
  userData[2] = 0;
  UARTPuts("\n Header in ID-MEMORY LSB: 0x",-1);
  UARTPuts(userData,-1);
  temp3=0;
  boardHeader[temp3++] = userData[0];
  boardHeader[temp3++] = userData[1];

  retVal = dec2ASCII(IdMemoryData[1],(UINT8*) &userData[0]);
  userData[2] = 0;
  UARTPuts("\n Header in ID-MEMORY    : 0x",-1);
  UARTPuts(userData,-1);
  boardHeader[temp3++] = userData[0];
  boardHeader[temp3++] = userData[1];

  retVal = dec2ASCII(IdMemoryData[2],(UINT8*) &userData[0]);
  userData[2] = 0;
  UARTPuts("\n Header in ID-MEMORY    : 0x",-1);
  UARTPuts(userData,-1);
  boardHeader[temp3++] = userData[0];
  boardHeader[temp3++] = userData[1];


  retVal = dec2ASCII(IdMemoryData[3], (UINT8*)&userData[0]);
  userData[2] = 0;
  UARTPuts("\n Header in ID-MEMORY MSB: 0x",-1);
  UARTPuts(userData,-1);
  boardHeader[temp3++] = userData[0];
  boardHeader[temp3++] = userData[1];

  boardHeader[temp3] = '\0';

  if(strcmp(&boardHeader[0],BOARD_HEADER))
  {
        UARTPuts("\n \n Wrong BOARD HEADER IN ID-MEMORY",-1);
        return -1;
  }

  temp3 = 0;

  while ((temp3 + ADD_ID_MEMORY_B_NAME)  < ADD_ID_MEMORY_VER )
  {
    userData[temp3] = IdMemoryData[temp3 + ADD_ID_MEMORY_B_NAME];
    temp3 = temp3 + 1;
  }


  userData[BOARD_NAME_LENGTH] = '\0';

  UARTPuts("\n \n BOARD NAME IN ID-MEMORY : ",-1);
  UARTPuts(userData,-1);

  if(strcmp(&userData[0],BOARD_NAME))
  {
      UARTPuts("\n \n Wrong BOARD NAME IN ID-MEMORY",-1);
      return -1;
  }

  /*Reading BOARD VERSION NUMBER From ID MEMORY */
  temp3 = 0;
  while ((temp3 + ADD_ID_MEMORY_VER)  < ADD_ID_MEMORY_SR_NO )
  {
    userData[temp3] = IdMemoryData[temp3 + ADD_ID_MEMORY_VER];
    temp3 = temp3 + 1;
  }

  userData[BOARD_VER_LENGTH] = '\0';

  UARTPuts("\n \n VERSION NUMBER :   ",-1);
  UARTPuts(userData,-1);

  if(strcmp(&userData[0],BOARD_VERSION))
  {
        UARTPuts("\n \n Wrong BOARD VERSION IN ID-MEMORY",-1);
        return -1;
  }


  /*Reading BOARD SERIAL NUMBER From ID MEMORY */
  temp3 = 0;
  while ((temp3 + ADD_ID_MEMORY_SR_NO)  < ADD_ID_MEMORY_CONFG )
  {
    userData[temp3] = IdMemoryData[temp3 + ADD_ID_MEMORY_SR_NO];
    temp3 = temp3 + 1;
  }

  userData[BOARD_SR_NO_LENGTH] = '\0';

  UARTPuts("\n\n SERIAL NUMBER  :   ",-1);
  UARTPuts(userData,-1);

  /*Reading BOARD CONFIGURATION From ID MEMORY */
  temp3 = 0;
  while ((temp3 + ADD_ID_MEMORY_CONFG)  < (ADD_ID_MEMORY_CONFG+BOARD_CONFG_USING_LEN))
  {
    userData[temp3] = IdMemoryData[temp3 + ADD_ID_MEMORY_CONFG];
    if(userData[temp3] == 0x46)
    {
      break;
    }
    temp3 = temp3 + 1;
  }

  userData[BOARD_CONFG_OPT_LEN] = '\0';
  userData[temp3] = '\0';
  UARTPuts("\n\n Configurations :   ",-1);
  UARTPuts(userData,-1);

  UARTPuts("\n ********************************************** ",-1);




    return 0;
}








/****************************************************************************
 * ASCII2dec
 *
 *  This function is used to Display MAC ADRESS for different Ethernet, This
 *  function is used only for the base boards as these boards are with MAC
 *  supports
 *   *
 * @parameter
 *  UINT8 a              : INPUT : FIRST NUMBER   5
 *  UINT8 b              : INPUT : SECOND NUMBER  5
 *  UINT8 *retData       : OUTUT : DECIMAL VALUE   85  >> 0X55
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 */
INT16 ASCII2dec
(
    UINT8 a,
    UINT8 b,
    UINT8 *retdata
)
{
    UINT8 numberA;
    UINT8 numberB;

    if('a'== a || 'A' == a)
        numberA = 10;
    else if('b'== a || 'B' == a)
        numberA = 11;
    else if('c'== a || 'C' == a)
        numberA = 12;
    else if('d'== a || 'D' == a)
        numberA = 13;
    else if('e'== a || 'E' == a)
        numberA = 14;
    else if('F'== a || 'f' == a)
        numberA = 15;
    else if(a > 57 || a < 48)
    {
      UARTPuts("\nInvalid Data Input",-1);
      return -1; //ERR_INVALID_PARAM;
    }
    else
        numberA = a - 48;

    if('a'== b || 'A' == b)
        numberB = 10;
    else if('b'== b || 'B' == b)
        numberB = 11 ;
    else if('c'== b || 'C' == b)
        numberB = 12 ;
    else if('d'== b || 'D' == b)
        numberB = 13 ;
    else if('e'== b || 'E' == b)
        numberB = 14 ;
    else if('F'== b || 'f' == b)
        numberB = 15 ;
    else if(b > 57 || b < 48)
    {
      UARTPuts("\nInvalid Data Input",-1);
      return FAIL_DIAG;
    }
    else
        numberB = b - 48;
    numberA = numberA << 4;

    *retdata = numberA | numberB;
    return SUCCESS_DIAG;

}




/****************************************************************************
 * getIdHeader
 *
 *  This function is used for Editing Header Information ON ID-MEMORY,
 *  INFORMATION Like
 *           HEADER NAME
 *           NAME
 *           VERSION NUMBER
 *           SERIAL NUMBER
 *           CONFIGURATION
 *           SERIAL NUMBER
 *           ETH MAC ADD
 *
 *  This function also initialize the EEPROM I2C based ,
 *
 * @parameter
 *    UINT8 bNumber :  INPUT : NUMBER FOR SELECTED BOARD
 *                  BOARD_13X13            1
 *                  BOARD_15X15            2
 *                  BOARD_GPD              3
 *                  BOARD_IAMC             4
 *                  BOARD_IP_PHONE         5
 *                  BOARD_LCD              6
 *  Board number should be number for connected Board as It takes slave address
 *   for
 *  EEPROM based on this value.
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 *   FAIL_DIAG
 */

INT16 getIdHeader(void)
{
    INT16 retVal;
    INT8 tempVar1;

    UINT8 decVal[10];

    E2prominit();

    if((strlen(userData) != (BOARD_HEADER_LENGTH*2))) //To enter Manual entry
    {

    UARTPuts("\n",-1);
/* =========================================================
 *
 *  HEADER PART START
 *
 * =========================================================*/

    decVal[0] = 0;
    decVal[1] = 0;
    decVal[2] = 0;
    decVal[3] = 0;
    decVal[4] = 0;
    UARTPuts("\n READING HEADER INFORMATION",-1);
    retVal = EEPROMRead(ADD_ID_MEMORY_HEADER, (UINT8*)decVal,
        BOARD_HEADER_LENGTH );

    if(SUCCESS_DIAG != retVal)
    {
      UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN GETING",-1);
      UARTPuts(" HEADER FROM ID-MEMORY \n",-1);
      return retVal;
    }

       retVal = dec2ASCII(decVal[0],(UINT8*) &userData[0]);
       userData[2] = 0;
       UARTPuts("\n Header in ID-MEMORY LSB: 0x",-1);
       UARTPuts(userData,-1);

       retVal = dec2ASCII(decVal[1],(UINT8*) &userData[0]);
       userData[2] = 0;

       UARTPuts("\n Header in ID-MEMORY    : 0x",-1);
       UARTPuts(userData,-1);

       retVal = dec2ASCII(decVal[2],(UINT8*) &userData[0]);
       userData[2] = 0;

       UARTPuts("\n Header in ID-MEMORY    : 0x",-1);
       UARTPuts(userData,-1);

       retVal = dec2ASCII(decVal[3], (UINT8*)&userData[0]);
       userData[2] = 0;

       UARTPuts("\n Header in ID-MEMORY MSB: 0x",-1);
       UARTPuts(userData,-1);
       UARTPuts("\n\n ",-1);

       UARTPuts("\n To edit type HEADER and enter\n",-1);
       UARTPuts("\n Press 'ESC' key to exit at any time!!",-1);

        userData[0] =0;
        userData[1] =0;
        userData[2] =0;

    UARTPuts("\n***************************************** \n",-1);
    UARTPuts("\n***************************************** \n",-1);
    UARTPuts("\n ENTER HEADER INFO 4 HEX BYTES (e.g  LSB AA5533EE MSB):  ",-1);

    retVal = UARTGets(userData,8);
  if(retVal!=8)
  {
    UARTPuts("\n ERROR IN GETING HEADER  \n",-1);
    retVal=FAIL_DIAG;
    return retVal;
    }

    }
  tempVar1 = 0;
  while(tempVar1 < BOARD_HEADER_LENGTH*2)
  {
    userData[tempVar1] = toupper(userData[tempVar1]);
    tempVar1 = tempVar1 + 1;
  }
  retVal = ASCII2dec(userData[0], userData[1], decVal);
  if (SUCCESS_DIAG != retVal)
    return retVal;

  retVal = ASCII2dec(userData[2], userData[3], &decVal[1]);
  if (SUCCESS_DIAG != retVal)
    return retVal;
  retVal = ASCII2dec(userData[4], userData[5], &decVal[2]);
  if (SUCCESS_DIAG != retVal)
    return retVal;
  retVal = ASCII2dec(userData[6], userData[7], &decVal[3]);
  if (SUCCESS_DIAG != retVal)
    return retVal;
  userData[8] = 0;
  UARTPuts(" \n Header : ",-1);
  UARTPuts(userData,-1);

    retVal = EEPROMWrite(ADD_ID_MEMORY_HEADER, (UINT8*)decVal, BOARD_HEADER_LENGTH);
    if(SUCCESS_DIAG != retVal)
    {
    UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN WRITING ",-1);
    UARTPuts("HEADER IN ID-MEMORY  \n",-1);

        return retVal;
    }

    delay(5);

    UARTPuts("\n WRITING HEADER IN ID-MEMORY  COMPLETED ",-1);
    UARTPuts("SUCCESS_DIAGFULLY\n",-1);
    UARTPuts("\n Now ACCESSING ID-MEMORY TO CHECK \n",-1);

    decVal[0] = 0;
    decVal[1] = 0;
    decVal[2] = 0;
    decVal[3] = 0;
    decVal[4] = 0;

    retVal = EEPROMRead(ADD_ID_MEMORY_HEADER, (UINT8*)decVal,BOARD_HEADER_LENGTH );
    if(SUCCESS_DIAG != retVal)
    {
    UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN GETING",-1);
    UARTPuts(" HEADER FROM ID-MEMORY \n",-1);

    return retVal;
    }

       retVal = dec2ASCII(decVal[0],(UINT8*) &userData[0]);
       userData[2] = 0;

    UARTPuts("\n Header in ID-MEMORY LSB: 0x",-1);
    UARTPuts(userData,-1);

       retVal = dec2ASCII(decVal[1],(UINT8*) &userData[0]);
       userData[2] = 0;

    UARTPuts("\n Header in ID-MEMORY    : 0x",-1);
    UARTPuts(userData,-1);

       retVal = dec2ASCII(decVal[2],(UINT8*) &userData[0]);
       userData[2] = 0;

    UARTPuts("\n Header in ID-MEMORY    : 0x",-1);
    UARTPuts(userData,-1);

    retVal = dec2ASCII(decVal[3], (UINT8*)&userData[0]);
    userData[2] = 0;

    UARTPuts("\n Header in ID-MEMORY MSB: 0x",-1);
    UARTPuts(userData,-1);
    UARTPuts("\n\n ",-1);

    return SUCCESS_DIAG;
/* =========================================================
 *
 *  HEADER PART COMPLETE
 *
 * =========================================================*/
}



/****************************************************************************
 * getIdBName
 *
 *  This function is used for Editing Bame Information ON ID-MEMORY,
 *  INFORMATION Like
 *           HEADER NAME
 *           NAME
 *           VERSION NUMBER
 *           SERIAL NUMBER
 *           CONFIGURATION
 *           SERIAL NUMBER
 *           ETH MAC ADD
 *
 *  This function also initialize the EEPROM I2C based ,
 *
 * @parameter
 *    UINT8 bNumber :  INPUT : NUMBER FOR SELECTED BOARD
 *                  BOARD_13X13            1
 *                  BOARD_15X15            2
 *                  BOARD_GPD              3
 *                  BOARD_IAMC             4
 *                  BOARD_IP_PHONE         5
 *                  BOARD_LCD              6
 *  Board number should be number for connected Board as It takes slave address
 *   for
 *  EEPROM based on this value.
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 *   FAIL_DIAG
 */

INT16 getIdBName(void)
{
    INT16 retVal;

    E2prominit();      /*  Base Board    */


    if((strlen(userData) != BOARD_NAME_LENGTH)) //Manual entry
    {
  UARTPuts("\n ",-1);


/* =========================================================
 *
 *  BOARD NAME PART START
 *
 * =========================================================*/


    userData[0] =0;
    userData[1] =0;
    userData[2] =0;

  UARTPuts("\n***************************************** \n",-1);
  UARTPuts("\n Examples:-  \n",-1);
  UARTPuts("\n For SK3358 IN ASCII     A335x_SK\n",-1);
  UARTPuts("\n*****************************************\n",-1);
  retVal = EEPROMRead(ADD_ID_MEMORY_B_NAME, (UINT8*)userData,BOARD_NAME_LENGTH);

  if(SUCCESS_DIAG != retVal)
  {

  UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN GETING ",-1);
  UARTPuts("BOARD NAME FROM ID-MEMORY \n",-1);
  return retVal;
  }
  userData[BOARD_NAME_LENGTH] = '\0';

    UARTPuts("\n BOARD NAME IN ID-MEMORY :-",-1);
    UARTPuts(userData,-1);
    UARTPuts("\n\n ",-1);

    UARTPuts("\n To edit type and enter\n",-1);
    UARTPuts("\n Press 'ESC' key to exit at any time!!",-1);



  UARTPuts("\n\n ENTER BOARD NAME : 8 BYTES AND ENTER :  ",-1);
  UARTPuts("\n***************************************** \n\n",-1);
  UARTPuts("\n BOARD NAME  :-",-1);


  userData[0] = 0;
  userData[1] = 0;
  userData[2] = 0;
  userData[3] = 0;
  userData[4] = 0;
  userData[5] = 0;
  userData[6] = 0;
  userData[7] = 0;

  retVal = UARTGets(userData,8);
  if(8 != retVal)
  {
  UARTPuts("\n ERROR IN GETING BOARD NAME  \n",-1);
  retVal= FAIL_DIAG;
  return retVal;
    }
    }
    userData[BOARD_NAME_LENGTH] = '\0';
    userData[0] = toupper(userData[0]);
    userData[1] = toupper(userData[1]);
    userData[2] = toupper(userData[2]);
    userData[3] = toupper(userData[3]);
    userData[4] = toupper(userData[4]);
    userData[5] = toupper(userData[5]);
    userData[6] = toupper(userData[6]);
    userData[7] = toupper(userData[7]);
    //*sPtr = toupper((unsigned char)*sPtr);
    UARTPuts(" \n BOARD NAME :",-1);
    UARTPuts(userData,-1);


  retVal = EEPROMWrite(ADD_ID_MEMORY_B_NAME, (UINT8*)userData, BOARD_NAME_LENGTH);
  if(SUCCESS_DIAG != retVal)
  {
  UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN WRITING ",-1);
  UARTPuts("BOARD NAME IN ID-MEMORY  \n",-1);

  return retVal;
  }

  UARTPuts("\n",-1);

  delay(5);

  retVal = EEPROMRead(ADD_ID_MEMORY_B_NAME, (UINT8*)userData,BOARD_NAME_LENGTH);

  if(SUCCESS_DIAG != retVal)
  {
  UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN GETING ",-1);
  UARTPuts("BOARD NAME FROM ID-MEMORY \n",-1);

  return retVal;
  }
  userData[BOARD_NAME_LENGTH] = '\0';

    UARTPuts("\n BOARD NAME IN ID-MEMORY:-",-1);
    UARTPuts(userData,-1);
    UARTPuts("\n\n ",-1);
/* =========================================================
 *
 *  BOARD NAME PART COMPLETE
 *
 * =========================================================*/
    return SUCCESS_DIAG;

}


/****************************************************************************
 * getVersion
 *
 *  This function is used for Editing Version Information ON ID-MEMORY,
 *  INFORMATION Like
 *           VERSION NUMBER
 *
 *  This function also initialize the EEPROM I2C based ,
 *
 * @parameter
 *
 *  Board number should be number for connected Board as It takes slave address
 *   for
 *  EEPROM based on this value.
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 *   FAIL_DIAG
 */

INT16 getVersion(void)
{
    INT16 retVal;

    E2prominit();      /*  Base Board    */

    if((strlen(userData) != BOARD_VER_LENGTH)) //Manual entry
    {
    UARTPuts("\n",-1);


/* =========================================================
 *
 *  VERSION PART START
 *
 * =========================================================*/

    userData[0] = 0;
  userData[1] = 0;
  userData[2] = 0;

  UARTPuts("\n***************************************** \n",-1);
  UARTPuts("\n***************************************** \n",-1);
  UARTPuts("\n Version information 4 byte  ASCII '1.0A' = REV. 01.0A\n",-1);

  retVal = EEPROMRead(ADD_ID_MEMORY_VER, (UINT8*)userData, BOARD_VER_LENGTH);

  if(SUCCESS_DIAG != retVal)
  {

  UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN ",-1);
    UARTPuts("GETING BOARD VERSION NUMBER FROM ID-MEMORY \n",-1);

  return retVal;
  }

  userData[BOARD_VER_LENGTH] = '\0';

  UARTPuts("\n BOARD VERSION IN ID-MEMORY (LSB BYTE FIRST):-",-1);
    UARTPuts(userData,-1);
    UARTPuts("\n\n ",-1);
    UARTPuts("\n To edit type and enter\n",-1);
    UARTPuts("\n Press 'ESC' key to exit at any time!!",-1);
  UARTPuts("\n VERSION : ",-1);


  retVal = UARTGets(userData,4);
  if(retVal!=4)
  {
    UARTPuts("\n ERROR IN GETING VERSION NUMBER",-1);
    retVal=FAIL_DIAG;
    return retVal;
  }
    }

  userData[0] = toupper(userData[0]);
  userData[1] = toupper(userData[1]);
  userData[2] = toupper(userData[2]);
  userData[3] = toupper(userData[3]);
  userData[BOARD_VER_LENGTH] = '\0';

  UARTPuts(" \n BOARD VERSION NUMBER :  ",-1);
  UARTPuts(userData,-1);


  retVal = EEPROMWrite(ADD_ID_MEMORY_VER, (UINT8*)userData, BOARD_VER_LENGTH);

  if(SUCCESS_DIAG != retVal)
  {
  UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN",-1);
  UARTPuts(" WRITING BOARD NAME IN ID-MEMORY  \n",-1);

  }
  delay(5);
  retVal = EEPROMRead(ADD_ID_MEMORY_VER, (UINT8*)userData,
    BOARD_VER_LENGTH);
  if(SUCCESS_DIAG != retVal)
  {
    UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN ",-1);
    UARTPuts("GETING BOARD VERSION NUMBER FROM ID-MEMORY \n",-1);
    return retVal;
  }

  userData[BOARD_VER_LENGTH] = '\0';
    UARTPuts("\n BOARD VERSION IN ID-MEMORY (LSB BYTE FIRST):-",-1);
    UARTPuts(userData,-1);
    UARTPuts("\n\n ",-1);

/* =========================================================
 *
 *  BOARD VERSION NUMBER PART COMPLETE
 *
 * =========================================================*/

    return SUCCESS_DIAG;

}

/****************************************************************************
 * getSerial
 *
 *  This function is used for Editing Version Information ON ID-MEMORY,
 *  INFORMATION Like
 *  SerialNUMBER
 *
 *  This function also initialize the EEPROM I2C based ,
 *
 * @parameter
 *    UINT8 bNumber :  INPUT : NUMBER FOR SELECTED BOARD
 *                  BOARD_13X13            1
 *                  BOARD_15X15            2
 *                  BOARD_GPD              3
 *                  BOARD_IAMC             4
 *                  BOARD_IP_PHONE         5
 *                  BOARD_LCD              6
 *  Board number should be number for connected Board as It takes slave address
 *   for
 *  EEPROM based on this value.
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 *   FAIL_DIAG
 */

INT16 getSerial(void)
{
    INT16 retVal;
    INT8 tempVar1 ;

//    UINT8 decVal[10];
    E2prominit();      /*  Base Board    */

    UARTPuts("\n",-1);

/* =========================================================
 *
 * BOARD SERIAL NUMBER PART START
 *
 * =========================================================*/

    userData[0] = 0;
    userData[1] = 0;
    userData[2] = 0;
  UARTPuts("\n************************************************\n",-1);
  UARTPuts("\n ENTER BOARD SERIAL NUMBER INFORMATION ",-1);
  UARTPuts("\n12 BYTES AND PRESS ENTER: \n",-1);

  UARTPuts("\n Example:'WWYY4P15NNNN' \n",-1);
  UARTPuts("\n          WW = 2 Digit Week Of Year Of Production",-1);
  UARTPuts("\n          YY = 2 Digit Year Of Production",-1);
  UARTPuts("\n          NNNN = Incrementing Board Number\n",-1);
  retVal = EEPROMRead(ADD_ID_MEMORY_SR_NO, (UINT8*)userData,BOARD_SR_NO_LENGTH);
  if(SUCCESS_DIAG != retVal)
  {
  UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN ",-1);
  UARTPuts("GETING BOARD SERIAL NUMBER FROM ID-MEMORY \n",-1);

  return retVal;
  }
  userData[BOARD_SR_NO_LENGTH] = '\0';
  userData[BOARD_SR_NO_LENGTH] = '\0';
    UARTPuts("\n BOARD SERIAL NUMBER IN ID-MEMORY :-",-1);
    UARTPuts(userData,-1);
    UARTPuts("\n To edit type and enter\n",-1);
    UARTPuts("\n Press 'ESC' key to exit at any time!!",-1);
    UARTPuts("\n\n ",-1);
  UARTPuts("\n Serial Number :-",-1);

#if 0
  retVal = UARTGets(userData,12);
  if(retVal!=12)
  {
    UARTPuts("\n ERROR IN GETING BOARD SERIAL NUMBER \n",-1);
    retVal=FAIL_DIAG;
    return retVal;
  }
#endif

  do
  {
    retVal = UARTGets(userData,BOARD_SR_NO_LENGTH);

    if(retVal==0) //quit serial number entry by ESC or CR
    {
      UARTPuts("\nSERIAL NUMBER Scan/entry exit",-1);
      retVal=FAIL_DIAG;
      return retVal;
    }

    if(retVal!=BOARD_SR_NO_LENGTH) //Message for re-entry/scanning
    {
        UARTPuts("\nScan/entry missing full SERIAL NUMBER\n",-1);
        UARTPuts("\nRe-enter or Scan the Board SERIAL NUMBER\n",-1);
        UARTPuts("\n\n ",-1);
        UARTPuts("\n Serial Number :-",-1);
    }


  }while(retVal!=BOARD_SR_NO_LENGTH);

   tempVar1 = 0;
     while(tempVar1 < BOARD_SR_NO_LENGTH)
     {
       userData[tempVar1] = toupper(userData[tempVar1]);
       tempVar1 = tempVar1 + 1;
     }
  userData[BOARD_SR_NO_LENGTH] = '\0';
  //UARTPuts(" \n  BOARD SERIAL NUMBER :");
  //UARTPuts(userData);

  retVal = EEPROMWrite(ADD_ID_MEMORY_SR_NO, (UINT8*)userData, BOARD_SR_NO_LENGTH);
  if(SUCCESS_DIAG != retVal)
  {
    UARTPuts("\n ERROR IN WRITING BOARD CONFIGURATION IN",-1);
    UARTPuts(" ID-MEMORY  \n",-1);
  }

  delay(5);

  retVal = EEPROMRead(ADD_ID_MEMORY_SR_NO, (UINT8*)userData,BOARD_SR_NO_LENGTH);
  if(SUCCESS_DIAG != retVal)
  {
    UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN ",-1);
    UARTPuts("GETING BOARD SERIAL NUMBER FROM ID-MEMORY \n",-1);
    return retVal;
  }

/* =========================================================
 *
 *  BOARD SERIAL NUMBER PART COMPLETE
 *
 * =========================================================*/
  userData[BOARD_SR_NO_LENGTH] = '\0';
    UARTPuts("\n BOARD SERIAL NUMBER IN ID-MEMORY:-",-1);
    UARTPuts(userData,-1);
    UARTPuts("\n\n ",-1);
    return SUCCESS_DIAG;

}



/****************************************************************************
 * getConfiguration
 *
 *  This function is used for Editing Version Information ON ID-MEMORY,
 *  INFORMATION Like Configuration
 *
 *  This function also initialize the EEPROM I2C based ,
 *
 * @parameter
 *
 * @return
 *
 *   SUCCESS_DIAG
 *   ERR_INVALID_PARAM
 *   FAIL_DIAG
 */

INT16 getConfiguration(void)
{
    INT16 retVal;
    INT8 tempVar1;

    E2prominit();      /*  Base Board    */

    if((strlen(userData) != BOARD_CONFG_USING_LEN)) //Manual entry
    {
      UARTPuts("\n",-1);

      /* =========================================================
       *
       * BOARD CONFIGURATION SETTING PART START
       *
       * =========================================================*/
      userData[0] =0;
      userData[1] =0;
      userData[2] =0;

      UARTPuts("\n***********************************************\n",-1);
      UARTPuts("\n ENTER BOARD CONFIGURATION INFORMATION 32 ",-1);
      UARTPuts("BYTES\n",-1);
      UARTPuts("\n The following codes are used:",-1);
      UARTPuts("\n ASCII 'SKU#00' = SK3358 with Resistive touchscreen",-1);
      UARTPuts("\n ASCII 'SKU#01' = SK3358 with Capacitive touchscreen",-1);

      retVal = EEPROMRead(ADD_ID_MEMORY_CONFG, (UINT8*)userData,BOARD_CONFG_OPT_LEN);

      if(SUCCESS_DIAG != retVal)
      {
        UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN ",-1);
        UARTPuts("GETING BOARD CONFIGURATIONS FROM ID-MEMORY \n",-1);
        return retVal;
      }

      userData[BOARD_CONFG_OPT_LEN] = '\0';

      UARTPuts("\nCONFI. IN MEMORY(LSB BYTE FIRST) \n \n  : ",-1);
      UARTPuts(userData,-1);

      userData[BOARD_CONFG_OPT_LEN] = '\0';
      UARTPuts("\n To edit type and enter\n",-1);
      UARTPuts("\n Press 'ESC' key to exit at any time!!",-1);
      UARTPuts("\n\n ",-1);
      UARTPuts("\n Configuration :-",-1);
      retVal = UARTGets(userData,6);

      if(6 != retVal)
      {
        UARTPuts("\n ERROR IN GETING BOARD CONFIGURATIONS \n",-1);
        retVal=FAIL_DIAG;
        return retVal;
      }

    }

    if(strlen(userData) != BOARD_CONFG_OPT_LEN)
    {
      char len = strlen(userData);
      for (;len < BOARD_CONFG_OPT_LEN;len++)
        userData[len] = 0x46;
    }
    tempVar1 = 0;

    while(tempVar1 < BOARD_CONFG_OPT_LEN)
    {
      userData[tempVar1] = toupper(userData[tempVar1]);
      tempVar1 = tempVar1 + 1;
    }
    userData[BOARD_CONFG_OPT_LEN] = '\0';
    UARTPuts(" \n BOARD CONFIGURATION :",-1);
    UARTPuts(userData,-1);

  UARTPuts(" \n ",-1);
  retVal = EEPROMWrite(ADD_ID_MEMORY_CONFG, (UINT8*)userData, BOARD_CONFG_OPT_LEN);
  if(SUCCESS_DIAG != retVal)
  {
    UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR ",-1);
    UARTPuts("IN WRITING BOARD CONFIGURATION IN ID-MEMORY  \n",-1);
  }

  delay(5);

  retVal = EEPROMRead(ADD_ID_MEMORY_CONFG, (UINT8*)userData,
    BOARD_CONFG_OPT_LEN);
  if(SUCCESS_DIAG != retVal)
  {
    UARTPuts("\n\n CHECK PROPER BOARD SELECTED\n ERROR IN ",-1);
    UARTPuts("GETING BOARD CONFIGURATIONS FROM ID-MEMORY \n",-1);
    return retVal;
  }
  userData[BOARD_CONFG_OPT_LEN] = '\0';
    UARTPuts("\nCONFI. IN MEMORY:-",-1);
    UARTPuts(userData,-1);
    /* =========================================================
     *
     *  BOARD CONFIGURATION PART COMPLETE
     *
     * =========================================================*/

    UARTPuts("\n\n ",-1);

    return SUCCESS_DIAG;

}
