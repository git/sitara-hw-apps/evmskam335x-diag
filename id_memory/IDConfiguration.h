/*
 * IDConfiguration.h
 *
 * EEProm ID configuration.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef IDCONFIGURATION_H_
#define IDCONFIGURATION_H_

/*
 * INCLUDES
 */
#include "evmskAM335x_diag.h"

/*
 * DEFINE PART AND VARIABLE
 */

#define BOARD_HEADER_LENGTH    4  //4 bytes which is entered in HEX as ASCII 8 bytes
#define BOARD_NAME_LENGTH      8
#define BOARD_VER_LENGTH       4
#define BOARD_SR_NO_LENGTH     12
#define BOARD_CONFG_USING_LEN  6
#define BOARD_CONFG_OPT_LEN    32

#define USER_DATA_LEN          100


#define ADD_ID_MEMORY_HEADER    0
#define ADD_ID_MEMORY_B_NAME   (ADD_ID_MEMORY_HEADER + BOARD_HEADER_LENGTH)
#define ADD_ID_MEMORY_VER      (ADD_ID_MEMORY_B_NAME + BOARD_NAME_LENGTH)
#define ADD_ID_MEMORY_SR_NO    (ADD_ID_MEMORY_VER + BOARD_VER_LENGTH)
#define ADD_ID_MEMORY_CONFG    (ADD_ID_MEMORY_SR_NO + BOARD_SR_NO_LENGTH)

#define LAST_ADDRESS           ADD_ID_MEMORY_CONFG + 32

#define CR            0x0D //Carriage return
#define LF            0x0A //Carriage return

#define ID_MEM_CONTENTS_LEN   34

/* ID Memory contents string index in the microSD card*/
#define ID_MEM_HEADER   0
#define ID_MEM_BOARDNAME  1
#define ID_MEM_VERSION    2
#define ID_MEM_SERIAL   3
#define ID_MEM_CONFIG   4

/*
 * FUNCTION DECLARATION
 */

INT16 getIdConfData(void);

INT16 IdDisplayConfiguration(void);

INT16 userInput(void);

INT16 ASCII2dec
(
    UINT8 a,
    UINT8 b,
    UINT8 *retdata
);

INT16 dec2ASCII
(
    UINT8 a,
    UINT8 *retData
);

INT16 getIdHeader(void);
INT16 getIdBName(void);
INT16 getVersion(void);
INT16 getSerial(void);
INT16 getConfiguration(void);



#endif /* IDCONFIGURATION_H_ */
