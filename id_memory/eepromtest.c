/*
 * eepromtest.c
 *
 * Sample application for HSI2C.This application reads the contents
 * of EEPROM and displays it on serial console.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "hsi2c.h"
#include "evmskAM335x.h"
#include "interrupt.h"
#include "uartStdio.h"
#include "soc_AM335x.h"
#include "I2cApi.h"

/******************************************************************************
**                      INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/* I2C address of CAT24C256 e2prom */
#define  I2C_SLAVE_ADDR         (0x50)

/* Higher byte address (i.e A8-A15) */
#define  E2PROM_ADDR_MSB         0x04

/* Lower byte address (i.e A0-A7) */
#define  E2PROM_ADDR_LSB         0x00


/******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/
static void E2promRead(unsigned char *data);
static void E2promWrite(unsigned int eepromAddr, unsigned char noB); // New API only works when WP is not enabled in the EEPROM

/******************************************************************************
**              GLOBAL VARIABLE DEFINITIONS
******************************************************************************/
extern volatile unsigned char dataFromSlave[I2C_INSTANCE][60];
extern volatile unsigned char dataToSlave[I2C_INSTANCE][60];
extern volatile unsigned int  tCount[I2C_INSTANCE];
extern volatile unsigned int  rCount[I2C_INSTANCE];
extern volatile unsigned int  flag[I2C_INSTANCE];
extern volatile unsigned int  numOfBytes[I2C_INSTANCE];

extern void SetupI2CTransmit(unsigned int dcount, unsigned int instNum);
extern void SetupE2PROMReception(unsigned int dcount, unsigned int instNum);

/*
 * SK3358 ID Memory(EEPROM) Structure
 */
typedef struct id_mem_header
{
    char Header[4];
    char BoardName[8];
    char Version[4];
    char SerialNumber[12];
    char ConfigurationOption[32];
}id_header;

#define BOARD_HEADER_LENGTH    4
#define BOARD_NAME_LENGTH      8
#define BOARD_VER_LENGTH       4
#define BOARD_SR_NO_LENGTH     12
#define BOARD_CONFG_OPT_LEN    32

#define ADD_ID_MEMORY_HEADER    0
#define ADD_ID_MEMORY_B_NAME   (ADD_ID_MEMORY_HEADER + BOARD_HEADER_LENGTH)
#define ADD_ID_MEMORY_VER      (ADD_ID_MEMORY_B_NAME + BOARD_NAME_LENGTH)
#define ADD_ID_MEMORY_SR_NO    (ADD_ID_MEMORY_VER + BOARD_VER_LENGTH)
#define ADD_ID_MEMORY_CONFG    (ADD_ID_MEMORY_SR_NO + BOARD_SR_NO_LENGTH)

/******************************************************************************
**              FUNCTION DEFINITIONS
******************************************************************************/
void E2prominit(void)
{
  /* Configures AINTC to generate interrupt */
    //I2CAINTCConfigure();

      /*
      ** Configures I2C to Master mode to generate start
      ** condition on I2C bus and to transmit data at a
      ** speed of 100khz
      */
      //SetupI2C(I2C_SLAVE_ADDR);

    I2CInit(I2C_0);

      // Configure PMIC slave address
      I2CMasterSlaveAddrSet(SOC_I2C_0_REGS, I2C_SLAVE_ADDR);

}

int E2promtest(void)
{
    unsigned char dataRead[50];
    //unsigned char temp;
    unsigned int i;

    UARTPuts("\nI2C EEPROM Test\n", -1);

    /*
    ** Read data from a selected address of
    ** e2prom
    */
    E2promRead(dataRead);

    UARTPuts("\n",-1);
    for(i = 0; i < 50; i++)
    {
      UARTPutNum(dataRead[i]);
        UARTPutc(',');
    }

    UARTPuts("\n",-1);

    E2promWrite(0x0400,50);

    E2promRead(dataRead);

    UARTPuts("\n",-1);

    for(i = 0; i < 50; i++)
    {
         UARTPutNum(dataRead[i]);
         UARTPutc(',');
         if(dataRead[i]!=i)
         {
           return -1; //eeprom read/write failed
         }

    }

    UARTPuts("\n",-1);

    return 0;

}


 /*
 ** Reads data from a specific address of e2prom
 */
static void E2promRead(unsigned char *data)
{
    unsigned int i;

    dataToSlave[I2C_0][0] = E2PROM_ADDR_MSB;
    dataToSlave[I2C_0][1] = E2PROM_ADDR_LSB;

    tCount[I2C_0] = 0;
    rCount[I2C_0] = 0;
    SetupE2PROMReception(50,I2C_0);

    for (i = 0; i < 50; i++ )
    {
         data[i] = dataFromSlave[I2C_0][i];
    }


}

/*
 ** Reads data from a specific address of e2prom
 ** eepromAddr - address to write in EEPROM
 ** noB - Number of Bytes
 ** ** New API only works when WP is not enabled in the EEPROM
 */
static void E2promWrite(unsigned int eepromAddr, unsigned char noB)
{
    unsigned int i;
    unsigned int totalnoB=noB+2; //includes 2 byte address

    dataToSlave[I2C_0][0] = ((eepromAddr&0xFF00)>>8);
    dataToSlave[I2C_0][1] = (eepromAddr&0x00FF);

    for (i = 2; i < totalnoB; i++ )
    {
             dataToSlave[I2C_0][i]=(i-2);
    }


    tCount[I2C_0] = 0;
    SetupI2CTransmit(totalnoB,I2C_0);

}

