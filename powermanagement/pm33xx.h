/*
 * pm33xx.h
 *
 * AM33XX Power Management Routines
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/


#ifndef __AM335X_MACROS
#define __AM335X_MACROS


#define AM33XX_CTRL_BASE  (0x44E10000)

#define M3_TXEV_EOI     (AM33XX_CTRL_BASE + 0x1324)
#define A8_M3_IPC_REGS      (AM33XX_CTRL_BASE + 0x1328)
#define DS_RESUME_ADDR      0x40300220
#define DS_IPC_DEFAULT      0xffffffff
#define M3_UMEM       0x44D00000

#define DS0_ID        0x3
#define DS1_ID        0x5

#define M3_STATE_UNKNOWN    -1
#define M3_STATE_RESET      0
#define M3_STATE_INITED     1
#define M3_STATE_MSG_FOR_LP   2
#define M3_STATE_MSG_FOR_RESET    3

/* DDR offsets */
#define DDR_CMD0_IOCTRL     (AM33XX_CTRL_BASE + 0x1404)
#define DDR_CMD1_IOCTRL     (AM33XX_CTRL_BASE + 0x1408)
#define DDR_CMD2_IOCTRL     (AM33XX_CTRL_BASE + 0x140C)
#define DDR_DATA0_IOCTRL    (AM33XX_CTRL_BASE + 0x1440)
#define DDR_DATA1_IOCTRL    (AM33XX_CTRL_BASE + 0x1444)

#define DDR_IO_CTRL     (AM33XX_CTRL_BASE + 0x0E04)
#define VTP0_CTRL_REG     (AM33XX_CTRL_BASE + 0x0E0C)
#define DDR_CKE_CTRL      (AM33XX_CTRL_BASE + 0x131C)
#define DDR_PHY_BASE_ADDR   (AM33XX_CTRL_BASE + 0x2000)

#define CMD0_CTRL_SLAVE_RATIO_0   (DDR_PHY_BASE_ADDR + 0x01C)
#define CMD0_CTRL_SLAVE_FORCE_0   (DDR_PHY_BASE_ADDR + 0x020)
#define CMD0_CTRL_SLAVE_DELAY_0   (DDR_PHY_BASE_ADDR + 0x024)
#define CMD0_DLL_LOCK_DIFF_0    (DDR_PHY_BASE_ADDR + 0x028)
#define CMD0_INVERT_CLKOUT_0    (DDR_PHY_BASE_ADDR + 0x02C)

#define CMD1_CTRL_SLAVE_RATIO_0   (DDR_PHY_BASE_ADDR + 0x050)
#define CMD1_CTRL_SLAVE_FORCE_0   (DDR_PHY_BASE_ADDR + 0x054)
#define CMD1_CTRL_SLAVE_DELAY_0   (DDR_PHY_BASE_ADDR + 0x058)
#define CMD1_DLL_LOCK_DIFF_0    (DDR_PHY_BASE_ADDR + 0x05C)
#define CMD1_INVERT_CLKOUT_0    (DDR_PHY_BASE_ADDR + 0x060)

#define CMD2_CTRL_SLAVE_RATIO_0   (DDR_PHY_BASE_ADDR + 0x084)
#define CMD2_CTRL_SLAVE_FORCE_0   (DDR_PHY_BASE_ADDR + 0x088)
#define CMD2_CTRL_SLAVE_DELAY_0   (DDR_PHY_BASE_ADDR + 0x08C)
#define CMD2_DLL_LOCK_DIFF_0    (DDR_PHY_BASE_ADDR + 0x090)
#define CMD2_INVERT_CLKOUT_0    (DDR_PHY_BASE_ADDR + 0x094)

#define DATA0_RD_DQS_SLAVE_RATIO_0  (DDR_PHY_BASE_ADDR + 0x0C8)
#define DATA0_RD_DQS_SLAVE_RATIO_1  (DDR_PHY_BASE_ADDR + 0x0CC)

#define DATA0_WR_DQS_SLAVE_RATIO_0  (DDR_PHY_BASE_ADDR + 0x0DC)
#define DATA0_WR_DQS_SLAVE_RATIO_1  (DDR_PHY_BASE_ADDR + 0x0E0)

#define DATA0_WRLVL_INIT_RATIO_0  (DDR_PHY_BASE_ADDR + 0x0F0)
#define DATA0_WRLVL_INIT_RATIO_1  (DDR_PHY_BASE_ADDR + 0x0F4)

#define DATA0_GATELVL_INIT_RATIO_0  (DDR_PHY_BASE_ADDR + 0x0FC)
#define DATA0_GATELVL_INIT_RATIO_1  (DDR_PHY_BASE_ADDR + 0x100)

#define DATA0_FIFO_WE_SLAVE_RATIO_0 (DDR_PHY_BASE_ADDR + 0x108)
#define DATA0_FIFO_WE_SLAVE_RATIO_1 (DDR_PHY_BASE_ADDR + 0x10C)

#define DATA0_WR_DATA_SLAVE_RATIO_0 (DDR_PHY_BASE_ADDR + 0x120)
#define DATA0_WR_DATA_SLAVE_RATIO_1 (DDR_PHY_BASE_ADDR + 0x124)

#define DATA0_DLL_LOCK_DIFF_0   (DDR_PHY_BASE_ADDR + 0x138)

#define DATA0_RANK0_DELAYS_0    (DDR_PHY_BASE_ADDR + 0x134)
#define DATA1_RANK0_DELAYS_0    (DDR_PHY_BASE_ADDR + 0x1D8)

/* Temp placeholder for the values we want in the registers */
#define EMIF_READ_LATENCY 0x04
#define EMIF_TIM1   0x0666B3D6
#define EMIF_TIM2   0x143731DA
#define EMIF_TIM3   0x00000347
#define EMIF_SDCFG    0x43805332
#define EMIF_SDCFG_2  0x43805332
#define EMIF_SDREF    0x0000081a
#define EMIF_SDMGT    0x80000000
#define EMIF_SDRAM    0x00004650
#define EMIF_PHYCFG   0x2

#define DDR2_DLL_LOCK_DIFF  0x0
#define DDR2_RD_DQS   0x12
#define DDR2_PHY_FIFO_WE  0x80

#define DDR_PHY_RESET   (0x1 << 10)
#define DDR_PHY_READY   (0x1 << 2)
#define DDR2_RATIO    0x80
#define CMD_FORCE   0x00
#define CMD_DELAY   0x00

#define DDR2_INVERT_CLKOUT  0x00
#define DDR2_WR_DQS   0x00
#define DDR2_PHY_WRLVL    0x00
#define DDR2_PHY_GATELVL  0x00
#define DDR2_PHY_WR_DATA  0x40
#define PHY_RANK0_DELAY   0x01
#define PHY_DLL_LOCK_DIFF 0x0
#define DDR_IOCTRL_VALUE  0x18B

#define VTP_CTRL_READY    (0x1 << 5)
#define VTP_CTRL_ENABLE   (0x1 << 6)
#define VTP_CTRL_LOCK_EN  (0x1 << 4)
#define VTP_CTRL_START_EN (0x1)

#endif
