/*
 * emac.h
 *
 * This file contains the EMAC driver API prototypes and macro definitions.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _EMAC_H_
#define _EMAC_H_


#include "stdio.h"
#include "mdio.h"
#include "hw_types.h"
#include "evmskAM335x.h"
#include "evmskam335x_diag.h"

/* ------------------------------------------------------------------------ *
 *  EMAC Descriptor                                                         *
 * ------------------------------------------------------------------------ */
typedef struct _EMAC_Desc {
    struct _EMAC_Desc *pNext;   // Pointer to next descriptor
    UINT8* pBuffer;             // Pointer to data buffer
    UINT32 BufOffLen;           // Buffer Offset(MSW) and Length(LSW)
    UINT32 PktFlgLen;           // Packet Flags(MSW) and Length(LSW)
} EMAC_Desc;

#define EMAC_BASE               0x4A100000

#define EMAC_MAC1_BASE      ( EMAC_BASE + 0xD80)
#define EMAC_MAC2_BASE      ( EMAC_BASE + 0xDC0)

#define EMAC_ALE_BASE     ( EMAC_BASE + 0xD00)
#define EMAC_DMA_BASE     ( EMAC_BASE + 0x800)

#define EMAC_RAM_BASE           (EMAC_BASE + 0x2000)

#define CPSW_ALE_PORTCTL0_PORT_STATE_FORWARD   (0x00000003u)
#define CPSW_ALE_PORTCTL1_PORT_STATE_FORWARD   (0x00000003u)
#define CPSW_ALE_PORTCTL2_PORT_STATE_FORWARD   (0x00000003u)

#define EMAC_RAM_LEN              0x2000

#define EMAC_MAX_HEADER_DESC           (8u)

/* Packet Flags */
#define EMAC_DSC_FLAG_SOP               0x80000000
#define EMAC_DSC_FLAG_EOP               0x40000000
#define EMAC_DSC_FLAG_OWNER             0x20000000
#define EMAC_DSC_FLAG_EOQ               0x10000000
#define EMAC_DSC_FLAG_TDOWNCMPLT        0x08000000
#define EMAC_DSC_FLAG_PASSCRC           0x04000000

/* The following flags are RX only */
#define EMAC_DSC_FLAG_JABBER            0x02000000
#define EMAC_DSC_FLAG_OVERSIZE          0x01000000
#define EMAC_DSC_FLAG_FRAGMENT          0x00800000
#define EMAC_DSC_FLAG_UNDERSIZED        0x00400000
#define EMAC_DSC_FLAG_CONTROL           0x00200000
#define EMAC_DSC_FLAG_OVERRUN           0x00100000
#define EMAC_DSC_FLAG_CODEERROR         0x00080000
#define EMAC_DSC_FLAG_ALIGNERROR        0x00040000
#define EMAC_DSC_FLAG_CRCERROR          0x00020000
#define EMAC_DSC_FLAG_NOMATCH           0x00010000
#define EMAC_TO_PORT_ENABLE       (1 << 20)
#define EMAC_TO_PORT          (1 << 16)

#define EMAC_MODE_1000M 2
#define EMAC_MODE_100M  1
#define EMAC_MODE_10M 0

/* Function Prototypes */
void EMACInit(unsigned int emacBase, unsigned short emacSpeed, unsigned int macLoopback,unsigned short portnum);
INT16 EMACTest_EthPackets(unsigned int emacBase,unsigned short portnum);

#endif  /* End of _EMAC_H_ */
