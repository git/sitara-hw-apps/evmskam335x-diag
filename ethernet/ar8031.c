/*
 * ar8031.c
 *
 * This file contains the device abstraction layer APIs for AR8031 PHY.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "hw_types.h"
#include "ar8031.h"
#include "mdio.h"
#include "phy.h"
#include "uartStdio.h"
#include "cpsw.h"
#include "soc_AM335x.h"
#include "evmskAM335x.h"
#include "evmskam335x_diag.h"
#include "delay.h"
#include "emac.h"


/** **************************************************************************
 * \n \brief DELAY loop used for Micro-second DELAY
 *
 * DELAY loop used for Micro-second DELAY
 *
 * \return
 * \n      return SUCCESS_DIAG for success  - Description
 * \n      return FAIL_DIAGED for error   - Description
 */
void AM335x_wait
(
  UINT32 DELAY
)
{
    volatile UINT32 loopCntr;
    for ( loopCntr = 0 ; loopCntr < DELAY ; loopCntr++ ){ };

}


/** **************************************************************************
 * \n \brief Micro-second DELAY routine
 *
 * Utility routine for micro-seconds
 *
 * \param  u32usec  [IN]  DELAY in micro-seconds
 *
 * \return
 * \n      return SUCCESS_DIAG for success  - Description
 * \n      return FAIL_DIAGED for error   - Description
 */
INT16 DELAYMicroSeconds
(
    UINT32 duration
)
{
      AM335x_wait( duration * 2 );

  return(SUCCESS_DIAG);
}

/**
 * \brief   Resets the PHY
 *
 * \param   mdioBaseAddr  Base Address of the MDIO Module Registers.
 * \param   phyAddr       PHY Adress.
 *
 * \return  None
 *
 **/
void PhyReset(unsigned int mdioBaseAddr, unsigned int phyAddr)
{
    /* Reset the phy */
    MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_BCR, PHY_SOFTRESET);
}

/**
 * \brief   Configures the PHY for a given speed and duplex mode. This
 *          API will first reset the PHY. Then it sets the desired speed
 *          and duplex mode for the PHY.
 *
 * \param   mdioBaseAddr  Base Address of the MDIO Module Registers.
 * \param   phyAddr       PHY Adress.
 * \param   speed         Speed to be enabled
 * \param   duplexMode    Duplex Mode
 *
 * \return  status after configuring \n
 *          TRUE if configuration successful
 *          FALSE if configuration failed
 *
 **/
unsigned int PhyConfigure(unsigned int mdioBaseAddr, unsigned int phyAddr,
                               unsigned short speed, unsigned short duplexMode)
{
    unsigned short data;

    data = PHY_SOFTRESET;

    /* Reset the phy */
    MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_BCR, data);

    /* wait till the reset bit is auto cleared */
    while(data)
    {
        /* Read the reset */
        if(MDIOPhyRegRead(mdioBaseAddr, phyAddr, PHY_BCR, &data) != TRUE)
        {
            return FALSE;
        }
    }

    /* Set the configurations */
    MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_BCR, (speed | duplexMode));

    return TRUE;
}


/**
 * \brief   Reads the Debug register value from the the PHY
 *      Register read has to be done through Indirect addressing way
 *
 * \param   mdioBaseAddr  Base Address of the MDIO Module Registers.
 * \param   phyAddr       PHY Address.
 * \param   regIdx        Index of the Debug register to be read
 * \param   regValAdr     address where value of the register will be written
 *
 * \return  status of the read
 *
 **/
int AR8031PhyDebugRegRead(unsigned int mdioBaseAddr, unsigned int phyAddr,
                            unsigned int regIdx, unsigned short *regValAdr)
{
  MDIOPhyRegWrite(mdioBaseAddr, phyAddr,PHY_DEBUG_PORT, regIdx);

    return (MDIOPhyRegRead(mdioBaseAddr, phyAddr, PHY_DEBUG_PORT2, regValAdr));
}

/**
 * \brief   Write the Debug register with the input
 *      Register write has to be done through Indirect addressing way
 *
 * \param   mdioBaseAddr  Base Address of the MDIO Module Registers.
 * \param   phyAddr       PHY Address.
 * \param   regIdx        Register Index
 * \param   regVal        value to be written
 *
 * \return  None
 *
 **/
void AR8031PhyDebugRegWrite(unsigned int mdioBaseAddr, unsigned int phyAddr,
                      unsigned int regIdx, unsigned short regVal)
{
    MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_DEBUG_PORT, regIdx );

    MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_DEBUG_PORT2, regVal );

}

/**
 * \brief   Reads the MMD (MDIO Managable Device) register value from the the PHY
 *      Register read has to be done through Indirect addressing way
 *
 * \param   mdioBaseAddr  Base Address of the MDIO Module Registers.
 * \param   phyAddr       PHY Address.
 * \param devAddr     MMD(MDIO Managable device) address
 * \param   regIdx        Index of the MMD register to be read
 * \param   regValAdr     address where value of the register will be written
 *
 * \return  status of the read
 *
 **/
int AR8031PhyMMDRegRead(unsigned int mdioBaseAddr, unsigned int phyAddr,
          unsigned short devAddr, unsigned short regIdx, unsigned short *regValAdr)
{
  /* Set the function as address with device address */
  MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_CONTROL,( PHY_MMD_ACCESS_FUNCT_ADDR
          |devAddr) );

  MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_ADDR_DATA, regIdx);

  /* Set the function as data with device address */
  MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_CONTROL,( PHY_MMD_ACCESS_FUNCT_DATA
            |devAddr) );

    return (MDIOPhyRegRead(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_ADDR_DATA, regValAdr));
}

/**
 * \brief   Write the MMD register with the input
 *      Register write has to be done through Indirect addressing way
 *
 * \param   mdioBaseAddr  Base Address of the MDIO Module Registers.
 * \param   phyAddr       PHY Address.
 * \param   devAddr     MMD (MDIO Managable Device) Address
 * \param   regIdx        MMD Register Index
 * \param   regVal        value to be written
 *
 * \return  None
 *
 **/
void AR8031PhyMMDRegWrite(unsigned int mdioBaseAddr, unsigned int phyAddr,
    unsigned short devAddr, unsigned short regIdx, unsigned short regVal)
{
  unsigned short tempRegdata=0;

  /* Set the function as address with device address */
  MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_CONTROL,( PHY_MMD_ACCESS_FUNCT_ADDR
            |devAddr) );

  MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_ADDR_DATA, regIdx);

  /* Set the function as data with device address */
  MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_CONTROL,( PHY_MMD_ACCESS_FUNCT_DATA
              |devAddr) );

  MDIOPhyRegRead(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_ADDR_DATA, &tempRegdata);

  MDIOPhyRegWrite(mdioBaseAddr, phyAddr, PHY_MMD_ACCESS_ADDR_DATA, regVal);
}



/**
 * \brief   This API Initializes the MDIO and configures the PHY
 *      device into Gigabit mode and Full duplex mode.
 *
 *      This function configures the PHY device into internal or external
 *      loopback mode.
 *
 * \param loopbackType  PHY Internal/External Loopback
 *
 * \return  0-on success link detection, -1 on link no detected //None
 *
 **/
int AR8031PHY_Configure(unsigned short emacSpeed, unsigned int loopbackType,unsigned short portnum)
{
  int i=0;
  unsigned int phynum, linkstat=0;
  unsigned short phyRegdata = 0;
  unsigned int phyID = 0;


  if(portnum==1)
    phynum=0;
  else
    phynum=1;

  /* Enable MII interface and configure the clkdiv value */
  MDIOInit(MDIO_BASE, CLK_FREQ, MDCLK_FREQ);
  delay(100);

  /* Check for PHYs */
    for(i=0;i<32;i++)
  {
      if((MDIOPhyAliveStatusGet(MDIO_BASE) & ( 1 << i)) != 0)
      {
        UARTPuts("\n\t\tPHY found at address", -1);
        UARTPutNum(i);
       }

  }

    /* check the PHY ID value and register access */
    phyID = PhyIDGet(MDIO_BASE, phynum);

    if(AR8031_PHYID == phyID)
    {
      UARTPuts("\n\t\tPHY chip is detected, PHY ID = ", -1);
      UARTPutHexNum(phyID);
      UARTPuts(" @PHY address= ", -1);
      UARTPutHexNum(phynum);
    }
    else
    {
      UARTPuts("\n\t\tPHY chip is not detected, PHY ID = ", -1);
      UARTPutHexNum(phyID);
      return -1;
    }

    /* PHY settings */
    switch(emacSpeed)
    {
    case EMAC_MODE_10M:
      PhyConfigure(MDIO_BASE, phynum, PHY_SPEED_SELECT_10M, FULL_DUPLEX);
      break;
    case EMAC_MODE_100M:
      PhyConfigure(MDIO_BASE, phynum, PHY_SPEED_SELECT_100M, FULL_DUPLEX);
      break;
    case EMAC_MODE_1000M:
      PhyConfigure(MDIO_BASE, phynum, PHY_SPEED_SELECT_1000M, FULL_DUPLEX);
      break;

    default:
      UARTPuts("\nWrong Speed option selected",-1);
      return -1;
    }

  /* External Loop back */
  if(loopbackType)
  {

    /* disable Hibernate mode */
    AR8031PhyDebugRegRead(MDIO_BASE, phynum, PHY_DEBUG_HIB_CONTROL_AN_TEST, &phyRegdata );

    phyRegdata &= (~PHY_DEBUG_HIB_ENABLE);

    AR8031PhyDebugRegWrite(MDIO_BASE, phynum, PHY_DEBUG_HIB_CONTROL_AN_TEST, phyRegdata );

    /* Enable external loopback */
    AR8031PhyDebugRegWrite(MDIO_BASE, phynum, PHY_DEBUG_EXTERNAL_LOOPBACK, 0x1);

    if(emacSpeed==EMAC_MODE_1000M)
    {
      MDIOPhyRegWrite(MDIO_BASE, phynum, PHY_CONTROL, 0x8140);
    }
    else if(emacSpeed==EMAC_MODE_100M)
    {
      MDIOPhyRegWrite(MDIO_BASE, phynum, PHY_CONTROL, 0xA100);
    }
    else
    {
      MDIOPhyRegWrite(MDIO_BASE, phynum, PHY_CONTROL, 0x8100);
    }

  }
  else  /* Internal Loopback */
  {

    if(emacSpeed==EMAC_MODE_1000M)
    {
      MDIOPhyRegWrite(MDIO_BASE, phynum, PHY_CONTROL, 0x4140);
    }
    else if(emacSpeed==EMAC_MODE_100M)
    {
      MDIOPhyRegWrite(MDIO_BASE, phynum, PHY_CONTROL, 0x6100);
    }
    else
    {
      MDIOPhyRegWrite(MDIO_BASE, phynum, PHY_CONTROL, 0x4100);
    }

  }

  /* Wait for link */
  delay(500);

  if(loopbackType)
  {
    linkstat=PhyLinkStatusGet(MDIO_BASE, phynum,50);


    if(linkstat)
    {
      UARTPuts("\n\t\tLink Detected",-1);
    }
    else
    {
      UARTPuts("\n\t\tLink is not detected",-1);
      return -1;
    }
  }

  return 0;

}
