/*
 * emacTest.c
 *
 * This file contains the source code for EMAC Test Module.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "emac.h"
#include "ar8031.h"
#include "delay.h"
#include "uartStdio.h"
#include "soc_AM335x.h"
#include "evmskAM335x.h"
#include "evmskam335x_diag.h"

#define NORMAL_OPERATION    0
#define MAC_LOOPBACK      1

#define PHY_INTERNAL_LOOPBACK 0
#define PHY_EXTERNAL_LOOPBACK 1

/*
**  Intialize ethernet.
*/
void EthernetInit(void)
{
    EVMPortRGMIIModeSelect();
}


/**
 * \brief This function is used to test the functionality
 *      of ethernet device.
 *
 * \param   none
 *
 * \return  Error values
 **/

int EMACTest_PHYExternalLoopback(unsigned short emacSpeed,unsigned short portnum)
{
  int index = 0;
  int errors = 0;

  UARTPuts("\n\t\tEMAC PHY External Loopback Test start",-1);

  EMACInit(EMAC_BASE, emacSpeed, NORMAL_OPERATION,portnum);

  errors = AR8031PHY_Configure(emacSpeed, PHY_EXTERNAL_LOOPBACK,portnum);

  if(!errors)
  {
    for(index=0; index < 2; index++)
    {
      errors += EMACTest_EthPackets(EMAC_BASE,portnum);
    }
  }

  if(!errors)
  {
    UARTPuts("\n\t\tEMAC PHY External Loopback Test passed!!",-1);
  }
  else
  {
    UARTPuts("\n\t\tEMAC PHY External Loopback Test failed!!",-1);
    return -1;
  }

  return 0;

}

/**
 * \brief This function is used to test the MAC Loopback functionality
 *      of ethernet device.
 *
 * \param   none
 *
 * \return  Error values
 *
 **/

int EMACTest_MACLoopback(unsigned short emacSpeed,unsigned short portnum)
{
  int index = 0;
  int errors = 0;

  UARTPuts("\n\t\tEMAC MAC Loopback Test start",-1);

  EMACInit(EMAC_BASE, emacSpeed, MAC_LOOPBACK,portnum);

  for(index=0; index < 2; index++)
  {
    errors += EMACTest_EthPackets(EMAC_BASE,portnum);
  }

  if(!errors)
  {
    UARTPuts("\n\t\tEMAC MAC Loopback Test passed!!",-1);
  }
  else
  {
    UARTPuts("\n\t\tEMAC MAC Loopback Test failed!!",-1);
    return -1;
  }

  return 0;

}

/**
 * \brief This function is used to test the PHY Loopback functionality
 *      of ethernet device.
 *
 * \param   none
 *
 * \return  Error values
 *
 **/

int EMACTest_PHYLoopback(unsigned short emacSpeed,unsigned short portnum)
{
  int index = 0;
  int errors = 0;

  UARTPuts("\n\t\tEMAC PHY Internal Loopback Test start",-1);

  EMACInit(EMAC_BASE, emacSpeed, NORMAL_OPERATION,portnum);

  AR8031PHY_Configure(emacSpeed, PHY_INTERNAL_LOOPBACK,portnum);

  for(index=0; index < 2; index++)
  {
    errors += EMACTest_EthPackets(EMAC_BASE,portnum);
  }

  if(!errors)
  {
    UARTPuts("\n\t\tEMAC PHY Loop back Test passed!!",-1);
  }
  else
  {
    UARTPuts("\n\t\tEMAC PHY Loopback Test failed!!",-1);
    return -1;
  }

  return 0;

}

/* void main( void ) */
short EMACTest(unsigned short portnum)
{
  short retVal = FAIL_DIAG;

  /* Ethernet Pin Mux setup */
  //CPSWPinMuxSetup();

    /* Ethernet Clock Configuration */
    //CPSWClkEnable();

    //EVMPortRGMIIModeSelect();


    retVal = EMACTest_MACLoopback(EMAC_MODE_100M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;

    retVal = EMACTest_PHYLoopback(EMAC_MODE_100M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;

    retVal = EMACTest_PHYExternalLoopback(EMAC_MODE_100M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;

    return SUCCESS_DIAG;
}


short EMACTest_10M(unsigned short portnum)
{
  short retVal = FAIL_DIAG;

  /* Ethernet Pin Mux setup */
  //CPSWPinMuxSetup();

  /* Ethernet Clock Configuration */
  //CPSWClkEnable();

  //EVMPortRGMIIModeSelect();

    retVal = EMACTest_MACLoopback(EMAC_MODE_10M,portnum);

   if(retVal != SUCCESS_DIAG )
        return retVal;

    retVal = EMACTest_PHYLoopback(EMAC_MODE_10M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;

    retVal = EMACTest_PHYExternalLoopback(EMAC_MODE_10M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;

    return SUCCESS_DIAG;
}



short EMACTest_GigaBit(unsigned short portnum)
{
  short retVal = FAIL_DIAG;

  /* Ethernet Pin Mux setup */
  //CPSWPinMuxSetup();

  /* Ethernet Clock Configuration */
  //CPSWClkEnable();

  //EVMPortRGMIIModeSelect();

    retVal = EMACTest_MACLoopback(EMAC_MODE_1000M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;

#if 0 /* It is working sometimes and not working on some times.
     This may be delay problem or something else. */

    retVal = EMACTest_PHYLoopback(EMAC_MODE_1000M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;
#endif


    retVal = EMACTest_PHYExternalLoopback(EMAC_MODE_1000M,portnum);

    if(retVal != SUCCESS_DIAG )
        return retVal;

    return SUCCESS_DIAG;
}


