/*
 * ar8031.h
 *
 * This file contains the AR8031 PHY driver API prototypes and macro definitions.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef AR8031_H_
#define AR8031_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CLK_FREQ  125000000
#define MDCLK_FREQ_1000M  2500000
#define MDCLK_FREQ  1000000

/* PHY ID 1 & 2 register contents */
#define AR8031_PHYID  0x004DD074

#undef AR8031_DEBUG

#define MDIO_BASE SOC_CPSW_MDIO_REGS
/* PHY register offset definitions */
//#define PHY_BCR                           (0u)
//#define PHY_BSR                           (1u)
#define PHY_ID1                           (2u)
#define PHY_ID2                           (3u)
//#define PHY_AUTONEG_ADV                   (4u)
//#define PHY_LINK_PARTNER_ABLTY            (5u)

#define PHY_CONTROL                      0x0
#define PHY_SPECIFIC_STATUS        0x11

#define PHY_DEBUG_PORT           0x1D
#define PHY_DEBUG_PORT2          0x1E
#define PHY_CHIP_CONFIG          0x1F
#define PHY_1000BASET_CONTROL     0x09

#define PHY_DEBUG_HIB_CONTROL_AN_TEST 0xB
#define PHY_DEBUG_EXTERNAL_LOOPBACK   0x11

#define PHY_MMD_ACCESS_CONTROL        0x0D
#define PHY_MMD_ACCESS_ADDR_DATA      0x0E

#define PHY_MMD_SMARTEEE_CONTROL3     0x805D

//#define PHY_MMD_CLK_25M_CLOCK_SELECT    0x8016

/* PHY register field definitions */

#define PHY_MMD_ACCESS_FUNCT_ADDR     0x0000
#define PHY_MMD_ACCESS_FUNCT_DATA     0x4000

#define PHY_MMD_SMARTEEE_CONTROL3_LPI_EN  0x0100

//#define PHY_MMD_CLK_25M_125M_PLL    0x0018
//#define PHY_MMD_CLK_25M_125M_DSP    0x001C

/* speed select */

#define PHY_SPEED_SELECT_10M      0x0
#define PHY_SPEED_SELECT_100M     0x2000
#define PHY_SPEED_SELECT_1000M      0x0040

#define FULL_DUPLEX           0x0100
#define HALF_DUPLEX           0x0

#define PHY_SPEED_SELECT_13       0x2000
#define PHY_SPEED_SELECT_6        0x0040

#define PHY_CONTROL_AUTONEG_ENABLE_SHIFT  12
#define PHY_CONTROL_RESTART_AUTONEG_SHIFT 9

/* duplex */
#define PHY_DEBUG_HIB_ENABLE    0x8000

#if 0
/* PHY status definitions */
#define PHY_ID_SHIFT                      (16u)
#define PHY_SOFTRESET                     (0x8000)
#define PHY_AUTONEG_ENABLE                (0x1000u)
#define PHY_AUTONEG_RESTART               (0x0200u)
#define PHY_AUTONEG_COMPLETE              (0x0020u)
#define PHY_AUTONEG_INCOMPLETE            (0x0000u)
#define PHY_AUTONEG_STATUS                (0x0020u)
#define PHY_AUTONEG_ABLE                  (0x0008u)
#define PHY_LPBK_ENABLE                   (0x4000u)
#define PHY_LINK_STATUS                   (0x0004u)

#endif

int AR8031PHY_Configure(unsigned short emacSpeed, unsigned int loopbackType,unsigned short portnum);
extern void PhyReset(unsigned int mdioBaseAddr, unsigned int phyAddr);
unsigned int PhyConfigure(unsigned int mdioBaseAddr, unsigned int phyAddr,
                               unsigned short speed, unsigned short duplexMode);
#ifdef __cplusplus
}
#endif

#endif /* AR8031_H_ */
