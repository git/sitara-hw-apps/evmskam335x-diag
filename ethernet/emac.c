/*
 * emac.c
 *
 * This file contains the device abstraction layer APIs for EMAC.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "emac.h"
#include "delay.h"
#include "string.h"
#include "cpsw.h"
#include "soc_AM335x.h"
#include "evmskAM335x.h"
#include "evmskam335x_diag.h"
#include "ar8031.h"
#include "uartStdio.h"


/* macro definitions */

#define EMAC_DEBUG

#define TX_BUF    128
#define RX_BUF    128

#define CPSW_RGMII_SEL_MODE                   (0x02u)
#define CPSW_MDIO_SEL_MODE                    (0x00u)


static UINT8 packet_data[TX_BUF];
static UINT8 packet_buffer1[RX_BUF];
static UINT8 packet_buffer2[RX_BUF];

volatile INT32 RxCount;
volatile INT32 TxCount;
volatile INT32 ErrCount;
volatile EMAC_Desc *pDescRx;
volatile EMAC_Desc *pDescTx;

static EMAC_Desc* pDescBase = ( EMAC_Desc* )EMAC_RAM_BASE;

static INT16 EMAC_Verify_Packet( EMAC_Desc* pDesc, UINT32 size, UINT32 flagCRC );

/**
 * \brief   Disables the ALE port from leaning an address
 *
 * \param   baseAddr      Base Address of the ALE module
 * \param   portNum       The port number
 *
 * \return  None
 *
 **/
void CPSWALELearnModeDisable(unsigned int baseAddr, unsigned int  portNum)
{
    HWREG(baseAddr + CPSW_ALE_PORTCTL(portNum)) |=
                                         CPSW_ALE_PORTCTL0_NO_LEARN;
}

/**
 * \brief   This API Initializes the EMAC and EMAC Control modules. When
 *          EMAC Control module is reset, the CPPI RAM is cleared.
 *          All the EMAC interrupts are disabled.
 *
 *          This function configures the EMAC into Gigabit speed, Full duplex mode
 *          and  Loopback enable/disable.
 *
 * \param   emacBase    EMAC Base address
 * \param macLoopback   MAC Loopback enable/disable
 *
 * \return  None
 *
 **/

void EMACInit(unsigned int emacBase, unsigned short emacSpeed, unsigned int macLoopback,unsigned short portnum)
{
    unsigned int emacAleBase = EMAC_ALE_BASE;
    unsigned int emacMacBase;
    unsigned int emacDMABase = EMAC_DMA_BASE;

    if (portnum==1)
      emacMacBase = EMAC_MAC1_BASE;
    else
      emacMacBase = EMAC_MAC2_BASE;

  /* Reset the EMAC Switch Subsystem */
    CPSWSSReset(emacBase);

  /* Reset the EMAC */
    CPSWSlReset(emacMacBase);

    /* Enable ALE, Clear table and By Pass */ /* testing */
  CPSWALEInit(emacAleBase);
    CPSWALEBypassEnable(emacAleBase);

  CPSWALEPortStateSet(emacAleBase,0,CPSW_ALE_PORT_STATE_FWD);
  CPSWALELearnModeDisable(emacAleBase,0);
  CPSWALEPortStateSet(emacAleBase,1,CPSW_ALE_PORT_STATE_FWD);
  CPSWALELearnModeDisable(emacAleBase,1);
  CPSWALEPortStateSet(emacAleBase,2,CPSW_ALE_PORT_STATE_FWD);
  CPSWALELearnModeDisable(emacAleBase,2);

  /* Configure as VLAN Unaware mode, FIFO loopback disabled */
  CPSWVLANAwareDisable(emacBase);

  /* Enable the statistics for Port 0, GMII1 port and GMII2 port */
  CPSWStatisticsEnable(emacBase);

  /* Reset the DMAs */
  CPSWCPDMAReset(emacDMABase);


  /* Disable the Tx interrupts */
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX0_MASK_SHIFT);
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX1_MASK_SHIFT);
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX2_MASK_SHIFT);
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX3_MASK_SHIFT);
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX4_MASK_SHIFT);
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX5_MASK_SHIFT);
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX6_MASK_SHIFT);
  CPSWCPDMATxIntDisable(emacDMABase,CPSW_CPDMA_TX_INTMASK_CLEAR_TX7_MASK_SHIFT);

  /* Disable the RX interrupts */
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX0_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX1_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX2_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX3_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX4_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX5_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX6_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX7_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX0_THRESH_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX1_THRESH_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX2_THRESH_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX3_THRESH_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX4_THRESH_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX5_THRESH_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX6_THRESH_PEND_MASK_SHIFT);
  CPSWCPDMARxIntDisable(emacDMABase,CPSW_CPDMA_RX_INTMASK_CLEAR_RX7_THRESH_PEND_MASK_SHIFT);


  /* Disable the statistics and error interrupts */
  HWREG(emacDMABase + CPSW_CPDMA_DMA_INTMASK_CLEAR ) = ( (0x1 << CPSW_CPDMA_DMA_INTMASK_CLEAR_HOST_ERR_INT_MASK_SHIFT) |
      CPSW_CPDMA_DMA_INTMASK_CLEAR_STAT_INT_MASK);

  CPSWCPDMARxBufOffsetSet(emacDMABase,0);

  /* Enable the Tx and Rx DMAs */
  CPSWCPDMATxEnable(emacDMABase);
  CPSWCPDMARxEnable(emacDMABase);

   /* Enable GMII and interface A and B for external gasket */
   CPSWSlRGMIIEnable(emacMacBase);

  /* Configure for the MAC loopback */
  if(emacSpeed == EMAC_MODE_1000M)
  {
    /* Configure the GMAC */
    CPSWSlTransferModeSet(emacMacBase,CPSW_SLIVER_GIG_FULL_DUPLEX);
    HWREG(emacMacBase + CPSW_SL_MACCONTROL) |=
        ((0x1 << CPSW_SL_MACCONTROL_RX_CEF_EN_SHIFT) |
        (0x1 << CPSW_SL_MACCONTROL_RX_CSF_EN_SHIFT));

  }   /* For 100M speed */
  else if(emacSpeed == EMAC_MODE_100M)
  {
    /* Configure the GMAC */
    CPSWSlTransferModeSet(emacMacBase,CPSW_SLIVER_NON_GIG_FULL_DUPLEX);
    HWREG(emacMacBase + CPSW_SL_MACCONTROL) |=
      ((0x1 << CPSW_SL_MACCONTROL_RX_CEF_EN_SHIFT) |
      (0x1 << CPSW_SL_MACCONTROL_RX_CSF_EN_SHIFT));
  }
  else  /* For 10M speed */
  {
    /* Configure the GMAC */
    CPSWSlTransferModeSet(emacMacBase,CPSW_SLIVER_NON_GIG_FULL_DUPLEX);
    CPSWSlControlExtEnable(emacMacBase);
    HWREG(emacMacBase + CPSW_SL_MACCONTROL) |=
        ((0x1 << CPSW_SL_MACCONTROL_RX_CEF_EN_SHIFT) |
        (0x1 << CPSW_SL_MACCONTROL_RX_CSF_EN_SHIFT));
  }

  if(macLoopback)
  {
    HWREG(emacMacBase + CPSW_SL_MACCONTROL) |= (0x1 << CPSW_SL_MACCONTROL_LOOPBACK_SHIFT);
  }



}



/**
 * \brief   This API setups the TX and RX descriptors and send and receive
 *      the Ethernet packets.
 *
 *      This will compare the Ethernet packets using the function
 *      This EMAC_Verify_Packet()
 *
 * \param emacBase  EMAC Base address
 *
 * \return  If Tx and Rx packets are not same, then return errors.
 *
 **/

INT16 EMACTest_EthPackets(unsigned int emacBase,unsigned short portnum)
{
    INT32 i, loop = 0 ;
    INT16 errors = 0, retVal = 0 ;
    UINT32 status;
    EMAC_Desc* pDesc;

    for(loop=0; loop < 128; loop++)
    {
      packet_buffer1[loop] = 0;
      packet_buffer2[loop] = 0;
    }

    /* ---------------------------------------------------------------- *
     *                                                                  *
     *  Setup RX packets                                                *
     *                                                                  *
     * ---------------------------------------------------------------- */
    pDesc            = pDescBase;
    pDesc->pNext     = pDescBase + 1;
    pDesc->pBuffer   = packet_buffer1;
    pDesc->BufOffLen = RX_BUF;
    pDesc->PktFlgLen = EMAC_DSC_FLAG_OWNER;

    pDesc            = pDescBase + 1;
    pDesc->pNext     = 0;
    pDesc->pBuffer   = packet_buffer2;
    pDesc->BufOffLen = RX_BUF;
    pDesc->PktFlgLen = EMAC_DSC_FLAG_OWNER;

    /*
     *  Start RX receiving
     */
    pDescRx = pDescBase;

    CPSWCPDMARxHdrDescPtrWrite(EMAC_DMA_BASE,(UINT32)pDescRx,0);

    /* ---------------------------------------------------------------- *
     *                                                                  *
     *  Setup TX packets                                                *
     *      Send 2 copies of the same packet using different sizes      *
     *                                                                  *
     * ---------------------------------------------------------------- */
    for ( i = 0 ; i < TX_BUF ; i++ )
        packet_data[i] = i;

    pDesc            = pDescBase + 10;
    pDesc->pNext     = pDescBase + 11;
    pDesc->pBuffer   = packet_data;
    pDesc->BufOffLen = TX_BUF - 4; // 4 bytes for CRC
    pDesc->PktFlgLen = EMAC_DSC_FLAG_OWNER
                     | EMAC_DSC_FLAG_SOP
                     | EMAC_DSC_FLAG_EOP
                     | (TX_BUF - 4);

    pDesc            = pDescBase + 11;
    pDesc->pNext     = 0;
    pDesc->pBuffer   = packet_data;
    pDesc->BufOffLen = TX_BUF - 4; // 4 bytes for CRC
    pDesc->PktFlgLen = EMAC_DSC_FLAG_OWNER
                     | EMAC_DSC_FLAG_SOP
                     | EMAC_DSC_FLAG_EOP
                     | (TX_BUF - 4);

    /*
     *  Start TX sending
     */
    TxCount = 0;
    RxCount = 0;
    pDescTx = pDescBase + 10;

    CPSWCPDMATxHdrDescPtrWrite(EMAC_DMA_BASE,( UINT32 )pDescTx,0);

    DELAYMicroSeconds( 100 );

    /* ACK TX */
    status = HWREG(EMAC_DMA_BASE + CPSW_CPDMA_TX_CP(0));
    HWREG(EMAC_DMA_BASE + CPSW_CPDMA_TX_CP(0)) = status;

  DELAYMicroSeconds( 200 );

  /* ACK RX */
  status = HWREG(EMAC_DMA_BASE + CPSW_CPDMA_RX_CP(0));
  HWREG(EMAC_DMA_BASE + CPSW_CPDMA_RX_CP(0)) = status;


    /* ---------------------------------------------------------------- *
     *                                                                  *
     *  Normally there would be a interrupt to service the EMAC RX/TX   *
     *  transmission.  Instead a short pause and manually call the ISR  *
     *  Interrupt Service Routine to check on the status.               *
     *                                                                  *
     *  You can later add in the ISR and the code to support.           *
     *                                                                  *
     * ---------------------------------------------------------------- */

    /* Busy period */
    DELAYMicroSeconds(100000);

    /* ---------------------------------------------------------------- *
     *  Check packet and results                                        *
     * ---------------------------------------------------------------- */

  retVal = EMAC_Verify_Packet( pDescBase, TX_BUF - 4, 1 );


    if ( retVal )        // Verify Size + Contents
        errors++;

    retVal = EMAC_Verify_Packet( pDescBase + 1, TX_BUF - 4, 1 );


    if ( retVal ) // Verify Size + Contents
        errors++;


    return errors;


}

/**
 * \brief   This API compares the Ethernet packet with data.
 *
 *      This function checks the Rx packet's descriptors flags
 *      for verifying the packets.
 *
 * \param pDesc Rx Descriptor address
 * \param size  Rx Packet size
 * \param flagCRC CRC Flag
 *
 * \return  If the Rx descriptor flag values has some errors, then these errors
 *      and if Tx and Rx packets are not same, then return errors.
 **/

static INT16 EMAC_Verify_Packet( EMAC_Desc* pDesc, UINT32 size, UINT32 flagCRC )
{
    INT16 i;
    UINT32 SizeCRC      = ( flagCRC ) ? size + 4 : size;
    UINT32 packet_flags = pDesc->PktFlgLen;

    /* We must own this packet */
    if ( packet_flags & EMAC_DSC_FLAG_OWNER )
    {
      UARTPuts("\n\t\tVerify Err:- ret: 1",-1);
        return 1;
    }


    /* Must be SOP and EOP */
    if ( ( packet_flags & ( EMAC_DSC_FLAG_SOP | EMAC_DSC_FLAG_EOP ) )
                       != ( EMAC_DSC_FLAG_SOP | EMAC_DSC_FLAG_EOP ) )
    {
      UARTPuts("\n\t\tVerify Err:- ret: 2",-1);
        return 2;
    }


    /* If flagCRC is set, it must have a CRC */
    if ( flagCRC )
        if ( ( packet_flags & EMAC_DSC_FLAG_PASSCRC ) != EMAC_DSC_FLAG_PASSCRC )
        {
          UARTPuts("\n\t\tVerify Err:- ret: 3",-1);
            return 3;
        }

    /* Packet must be correct size */
    if ( ( packet_flags & 0xFFFF ) != SizeCRC )
    {
      UARTPuts("\n\t\tVerify Err:- ret: 5",-1);
        return 5;
    }

    /* Offset must be zero, packet size unchanged */
    if ( pDesc->BufOffLen != SizeCRC )
    {
      UARTPuts("\n\t\tVerify Err:- ret: 6",-1);
        return 6;
    }

    /* Validate the data */
    for ( i = 0 ; i < size ; i++ )
        if ( pDesc->pBuffer[i] != i )
        {
            return 7;
        }

    return 0;
}


