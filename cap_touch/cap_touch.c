/*
 * cap_touch.c
 *
 * This file contains API's to access cap_touch registers
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "cap_touch.h"
#include "evmskAM335x.h"
#include "hsi2c.h"
#include "interrupt.h"
#include "soc_AM335x.h"
#include "uartStdio.h"
#include "uart_irda_cir.h"
#include "gpio_v2.h"
#include "raster.h"
#include "ecap.h"
#include "hw_cm_per.h"
#include "hw_control_AM335x.h"
#include "hw_types.h"
#include "evmskam335x_diag.h"
#include "I2cApi.h"
#include "delay.h"

//lcd variables/defines
#define LINES             272
#define PIXELS            480
#define BLACK             0x00000000
#define WHITE             0x00FFFFFF
#define BLUE              0x000000FF
#define GREEN             0x0000FF00
#define RED               0x00FF0000
#define FrameBufferSize   LINES*PIXELS


/******************************************************************************
**              GLOBAL VARIABLE DEFINITIONS
******************************************************************************/
extern volatile unsigned char dataFromSlave[I2C_INSTANCE][60];
extern volatile unsigned char dataToSlave[I2C_INSTANCE][60];
extern volatile unsigned int  tCount[I2C_INSTANCE];
extern volatile unsigned int  rCount[I2C_INSTANCE];
extern volatile unsigned int  flag[I2C_INSTANCE];
extern volatile unsigned int  numOfBytes[I2C_INSTANCE];

//*****************************************************************************
//              LCD Functions
//****************************************************************************/
void DrawBox(int x1, int y1)
{
  unsigned int pixel;
  unsigned int line;
  unsigned int color;

  for (line=1; line<=LINES; line++)
    for (pixel=1; pixel<=PIXELS; pixel++)
    {
      color=WHITE;
      if( line>=(y1) & line<=(y1+40) )
        if( pixel>=(x1) & pixel<=(x1+40) )
          color=RED;
      frameBuf[ ((line-1)*480) + (pixel-1)] = color;
    }
}

void Draw2Box(int x1, int y1, int x2, int y2)
{
  unsigned int pixel;
  unsigned int line;
  unsigned int color;

  for (line=1; line<=LINES; line++)
    for (pixel=1; pixel<=PIXELS; pixel++)
    {
      color=WHITE;
      if( line>=(y1) & line<=(y1+40) )
        if( pixel>=(x1) & pixel<=(x1+40) )
          color=RED;
      if( line>=(y2) & line<=(y2+40) )
        if( pixel>=(x2) & pixel<=(x2+40) )
          color=GREEN;
      frameBuf[ ((line-1)*480) + (pixel-1)] = color;
    }
}

/******************************************************************************
**              Cap_touch Functions
******************************************************************************/
//read byte
UINT32 CapTouchReadByte(UINT32 regOffset)
{
  dataToSlave[I2C_0][0] = regOffset;
  // clear receive buffer
  dataFromSlave[I2C_0][0] = 0;
  dataFromSlave[I2C_0][1] = 0;
  rCount[I2C_0] = 0;
  tCount[I2C_0] = 0;
  SetupReception(1,I2C_0);

  return dataFromSlave[I2C_0][0];
}

//write byte
void CapTouchWriteByte(UINT32 regOffset, UINT32 data)
{
  /*  Read reg value  */
  dataToSlave[I2C_0][0] = regOffset;
  dataToSlave[I2C_0][1] = data;
  tCount[I2C_0] = 0;
  SetupI2CTransmit(2,I2C_0);
}

//CapTouchDumpRegs
//    Dump all cap touch registers
void CapTouchDumpRegs(void)
{
  UARTprintf("Dumping all Cap Touch registers:\n");
  UARTprintf("  DEVICE_MODE            :  0x%02x\n", CapTouchReadByte(DEVICE_MODE            ));
  UARTprintf("  GEST_ID                :  0x%02x\n", CapTouchReadByte(GEST_ID                ));
  UARTprintf("  TD_STATUS              :  0x%02x\n", CapTouchReadByte(TD_STATUS              ));
  UARTprintf("  TOUCH1_XH              :  0x%02x\n", CapTouchReadByte(TOUCH1_XH              ));
  UARTprintf("  TOUCH1_XL              :  0x%02x\n", CapTouchReadByte(TOUCH1_XL              ));
  UARTprintf("  TOUCH1_YH              :  0x%02x\n", CapTouchReadByte(TOUCH1_YH              ));
  UARTprintf("  TOUCH1_YL              :  0x%02x\n", CapTouchReadByte(TOUCH1_YL              ));
  UARTprintf("  TOUCH2_XH              :  0x%02x\n", CapTouchReadByte(TOUCH2_XH              ));
  UARTprintf("  TOUCH2_XL              :  0x%02x\n", CapTouchReadByte(TOUCH2_XL              ));
  UARTprintf("  TOUCH2_YH              :  0x%02x\n", CapTouchReadByte(TOUCH2_YH              ));
  UARTprintf("  TOUCH2_YL              :  0x%02x\n", CapTouchReadByte(TOUCH2_YL              ));
  UARTprintf("  TOUCH3_XH              :  0x%02x\n", CapTouchReadByte(TOUCH3_XH              ));
  UARTprintf("  TOUCH3_XL              :  0x%02x\n", CapTouchReadByte(TOUCH3_XL              ));
  UARTprintf("  TOUCH3_YH              :  0x%02x\n", CapTouchReadByte(TOUCH3_YH              ));
  UARTprintf("  TOUCH3_YL              :  0x%02x\n", CapTouchReadByte(TOUCH3_YL              ));
  UARTprintf("  TOUCH4_XH              :  0x%02x\n", CapTouchReadByte(TOUCH4_XH              ));
  UARTprintf("  TOUCH4_XL              :  0x%02x\n", CapTouchReadByte(TOUCH4_XL              ));
  UARTprintf("  TOUCH4_YH              :  0x%02x\n", CapTouchReadByte(TOUCH4_YH              ));
  UARTprintf("  TOUCH4_YL              :  0x%02x\n", CapTouchReadByte(TOUCH4_YL              ));
  UARTprintf("  TOUCH5_XH              :  0x%02x\n", CapTouchReadByte(TOUCH5_XH              ));
  UARTprintf("  TOUCH5_XL              :  0x%02x\n", CapTouchReadByte(TOUCH5_XL              ));
  UARTprintf("  TOUCH5_YH              :  0x%02x\n", CapTouchReadByte(TOUCH5_YH              ));
  UARTprintf("  TOUCH5_YL              :  0x%02x\n", CapTouchReadByte(TOUCH5_YL              ));
  UARTprintf("  ID_G_THGROUP           :  0x%02x\n", CapTouchReadByte(ID_G_THGROUP           ));
  UARTprintf("  ID_G_THPEAK            :  0x%02x\n", CapTouchReadByte(ID_G_THPEAK            ));
  UARTprintf("  ID_G_THCAL             :  0x%02x\n", CapTouchReadByte(ID_G_THCAL             ));
  UARTprintf("  ID_G_THWATER           :  0x%02x\n", CapTouchReadByte(ID_G_THWATER           ));
  UARTprintf("  ID_G_TEMP              :  0x%02x\n", CapTouchReadByte(ID_G_TEMP              ));
  UARTprintf("  ID_G_THDIFF            :  0x%02x\n", CapTouchReadByte(ID_G_THDIFF            ));
  UARTprintf("  ID_G_CTRL              :  0x%02x\n", CapTouchReadByte(ID_G_CTRL              ));
  UARTprintf("  ID_G_TIME_ENTER_MONITOR:  0x%02x\n", CapTouchReadByte(ID_G_TIME_ENTER_MONITOR));
  UARTprintf("  ID_G_PERIODACTIVE      :  0x%02x\n", CapTouchReadByte(ID_G_PERIODACTIVE      ));
  UARTprintf("  ID_G_PERIODMONITOR     :  0x%02x\n", CapTouchReadByte(ID_G_PERIODMONITOR     ));
  UARTprintf("  ID_G_AUTO_CLB_MODE     :  0x%02x\n", CapTouchReadByte(ID_G_AUTO_CLB_MODE     ));
  UARTprintf("  ID_G_LIB_VERSION_H     :  0x%02x\n", CapTouchReadByte(ID_G_LIB_VERSION_H     ));
  UARTprintf("  ID_G_LIB_VERSION_L     :  0x%02x\n", CapTouchReadByte(ID_G_LIB_VERSION_L     ));
  UARTprintf("  ID_G_CIPHER            :  0x%02x\n", CapTouchReadByte(ID_G_CIPHER            ));
  UARTprintf("  ID_G_MODE              :  0x%02x\n", CapTouchReadByte(ID_G_MODE              ));
  UARTprintf("  ID_G_PMODE             :  0x%02x\n", CapTouchReadByte(ID_G_PMODE             ));
  UARTprintf("  ID_G_FIRMID            :  0x%02x\n", CapTouchReadByte(ID_G_FIRMID            ));
  UARTprintf("  ID_G_STATE             :  0x%02x\n", CapTouchReadByte(ID_G_STATE             ));
  UARTprintf("  ID_G_FT5201ID          :  0x%02x\n", CapTouchReadByte(ID_G_FT5201ID          ));
  UARTprintf("  ID_G_ERR               :  0x%02x\n", CapTouchReadByte(ID_G_ERR               ));
  UARTprintf("  ID_G_CLB               :  0x%02x\n", CapTouchReadByte(ID_G_CLB               ));
  UARTprintf("  ID_G_B_AREA_TH         :  0x%02x\n", CapTouchReadByte(ID_G_B_AREA_TH         ));
  UARTprintf("  LOG_MSG_CNT            :  0x%02x\n", CapTouchReadByte(LOG_MSG_CNT            ));
  UARTprintf("  LOG_CUR_CHA            :  0x%02x\n", CapTouchReadByte(LOG_CUR_CHA            ));
}

// CaptTouchRWtest
//    Read, Write and Read back of the Interrupt status register
int CapTouchRWtest(void)
{
  unsigned char interruptStat=0;
  unsigned char newInterruptStat=0;

  interruptStat = CapTouchReadByte(ID_G_MODE);
  //UARTprintf("Current interrupt status: 0x%02x\n",  interruptStat);
  interruptStat = !interruptStat;
  //UARTprintf("Interrupt status channged to: 0x%02x\n",  interruptStat);
  CapTouchWriteByte(ID_G_MODE, interruptStat);
  newInterruptStat = CapTouchReadByte(ID_G_MODE);
  //UARTprintf("New interrupt status: 0x%02x\n",  newInterruptStat);

  if(newInterruptStat==interruptStat)
  {
    UARTprintf("Read/Write test successful\n");
    return 0;
  }
  else
  {
    UARTprintf("Read/Write test failed\n");
    return 1;
  }
}

// CapTouchConfig
//    some basic config required before usage
void CapTouchConfig(void)
{
  CapTouchWriteByte(DEVICE_MODE, NORMAL_OPMODE);
  CapTouchWriteByte(ID_G_THGROUP, 0x46);
  CapTouchWriteByte(ID_G_THPEAK, 0x3c);
  CapTouchWriteByte(ID_G_THCAL, 0x1d);
  CapTouchWriteByte(ID_G_THWATER, 0xd3);
  CapTouchWriteByte(ID_G_TEMP, 0xeb);
  CapTouchWriteByte(ID_G_THDIFF, 0xa0);
  CapTouchWriteByte(ID_G_TIME_ENTER_MONITOR, 0xc8);
  CapTouchWriteByte(ID_G_PERIODACTIVE, 0x06);
  CapTouchWriteByte(ID_G_PERIODMONITOR, 0x28);
  CapTouchWriteByte(ID_G_MODE, 0x01);
}

// ConfirmTouch
//  confirms touch of a single 40x40 box starting at x1,y1
int ConfirmTouch(int x1, int y1, int t1[2])
{
    if( t1[0]>=x1 & t1[0]<=(x1+40) )
      if( t1[1]>=y1 & t1[1]<=(y1+40) )
        return 1;
  return 0;
}

// Confirm2Touch
//  confirms touch of touching 2 40x40 boxes starting at x1,y1 and x2,y2
int Confirm2Touch(int x1, int y1, int x2, int y2, int t1[2], int t2[2])
{
    if( t1[0]>=x1 & t1[0]<=(x1+40) )
      if( t1[1]>=y1 & t1[1]<=(y1+40) )
        if( t2[0]>=x2 & t2[0]<=(x2+40) )
          if( t2[1]>=y2 & t2[1]<=(y2+40) )
            return 1;
  return 0;
}

// CapTouchTest
int CapTouchTest(void)
{
  int val=0;
  int fQuit=FALSE;
  int state=0;
  int touchPoints=0;
  int newTouch[2] = {0,0};
  int newTouches=0;
  int t1[2]={0,0};
  int t2[2]={0,0};
  int t3[2]={0,0};
  int t4[2]={0,0};
  int t5[2]={0,0};

  SetUpLCD();
  ClearLCD();
  EcapBkLightVary(MAXBACKLIGHT);

  val=CapTouchReadByte(ID_G_CIPHER);
  if(val!=0x55)
  {
    UARTprintf("ERROR: CapTouch Chip Vendor ID invalid\n");
    UARTprintf("  Chip Vendor ID found is 0x%02x\n", val);
    return 1;
  }
  UARTprintf("  Firmware:   0x%02x\n", CapTouchReadByte(ID_G_FIRMID) );
  UARTprintf("  LibVersion: 0x%02x%02x\n", CapTouchReadByte(ID_G_LIB_VERSION_H), CapTouchReadByte(ID_G_LIB_VERSION_L) );

  //CapTouchDumpRegs();
  if( CapTouchRWtest() )
    return 1;
  CapTouchConfig();

  UARTprintf("Touch box/boxes or press any key to quit\n");
  DrawBox(0,0);
  while(!fQuit)
  {
    touchPoints=CapTouchReadByte(TD_STATUS);
    if(touchPoints>5)
      touchPoints=0;
    newTouches=0;
    if(touchPoints>=1)
    {
      newTouch[0] = (CapTouchReadByte(TOUCH1_XH)&0xf)<<8 | CapTouchReadByte(TOUCH1_XL);
      newTouch[1] = (CapTouchReadByte(TOUCH1_YH)&0xf)<<8 | CapTouchReadByte(TOUCH1_YL);
      if( newTouch[0]!=t1[0] | newTouch[1]!=t1[1])
      {
        UARTprintf("Touch 1:    %d,%d\n", newTouch[0], newTouch[1]);
        t1[0]=newTouch[0];
        t1[1]=newTouch[1];
        newTouches++;
      }
    }
    if(touchPoints>=2)
    {
      newTouch[0] = (CapTouchReadByte(TOUCH2_XH)&0xf)<<8 | CapTouchReadByte(TOUCH2_XL);
      newTouch[1] = (CapTouchReadByte(TOUCH2_YH)&0xf)<<8 | CapTouchReadByte(TOUCH2_YL);
      if( newTouch[0]!=t2[0] | newTouch[1]!=t2[1])
      {
        UARTprintf("Touch 2:    %d,%d\n", newTouch[0], newTouch[1]);
        t2[0]=newTouch[0];
        t2[1]=newTouch[1];
        newTouches++;
      }
    }
    if(touchPoints>=3)
    {
      newTouch[0] = (CapTouchReadByte(TOUCH3_XH)&0xf)<<8 | CapTouchReadByte(TOUCH3_XL);
      newTouch[1] = (CapTouchReadByte(TOUCH3_YH)&0xf)<<8 | CapTouchReadByte(TOUCH3_YL);
      if( newTouch[0]!=t3[0] | newTouch[1]!=t3[1])
      {
        UARTprintf("Touch 3:    %d,%d\n", newTouch[0], newTouch[1]);
        t3[0]=newTouch[0];
        t3[1]=newTouch[1];
        newTouches++;
      }
    }
    if(touchPoints>=4)
    {
      newTouch[0] = (CapTouchReadByte(TOUCH4_XH)&0xf)<<8 | CapTouchReadByte(TOUCH4_XL);
      newTouch[1] = (CapTouchReadByte(TOUCH4_YH)&0xf)<<8 | CapTouchReadByte(TOUCH4_YL);
      if( newTouch[0]!=t4[0] | newTouch[1]!=t4[1])
      {
        UARTprintf("Touch 4:    %d,%d\n", newTouch[0], newTouch[1]);
        t4[0]=newTouch[0];
        t4[1]=newTouch[1];
        newTouches++;
      }
    }
    if(touchPoints>=5)
    {
      newTouch[0] = (CapTouchReadByte(TOUCH5_XH)&0xf)<<8 | CapTouchReadByte(TOUCH5_XL);
      newTouch[1] = (CapTouchReadByte(TOUCH5_YH)&0xf)<<8 | CapTouchReadByte(TOUCH5_YL);
      if( newTouch[0]!=t5[0] | newTouch[1]!=t5[1])
      {
        UARTprintf("Touch 5:    %d,%d\n", newTouch[0], newTouch[1]);
        t5[0]=newTouch[0];
        t5[1]=newTouch[1];
        newTouches++;
      }
    }
    switch(state)
    {
      case 0:
        if(newTouches==1)
          if( ConfirmTouch(0,0, t1) )
          {
            DrawBox(220,116);
            UARTprintf("   Box 1 successful\n");
            state++;
          }
        break;
      case 1:
        if(newTouches==1)
          if( ConfirmTouch(220,116, t1) )
          {
            DrawBox(440,232);
            UARTprintf("   Box 2 successful\n");
            state++;
          }
        break;
      case 2:
        if(newTouches==1)
          if( ConfirmTouch(440,232, t1) )
          {
            DrawBox(60,60);
            UARTprintf("   Box 3 successful\n");
            state++;
          }
        break;
      case 3:
        if(newTouches==1)
          if( ConfirmTouch(60,60, t1) )
          {
            DrawBox(300,140);
            UARTprintf("   Box 4 successful\n");
            state++;
          }
        break;
      case 4:
        if(newTouches==1)
          if( ConfirmTouch(300,140, t1) )
          {
            Draw2Box(80,80,200,200);
            UARTprintf("   Box 5 successful\n");
            state++;
          }
        break;
      case 5:
        if(newTouches==2)
          if( Confirm2Touch(80,80, 200,200, t1, t2) )
          {
            fQuit=TRUE;
            UARTprintf("   MultiBox successful\n");
            state++;
          }
        break;
      }
    if(UARTCharsAvail(UART_CONSOLE_BASE))
    {
      fQuit=TRUE;
      UARTprintf("\n");
    }
  }

  ClearLCD();
  EcapBkLightVary(LOWBACKLIGHT);
  DisableBackLight();

  if(state!=6)
    return 1;
  else
    return 0;
}

void CapTouchSetup(void)
{
  /*CT WAKE set HIGH for normal operation*/
  GPIOPinWrite(SOC_GPIO_2_REGS,GPIO_INSTANCE_PIN_NUMBER_1,GPIO_PIN_HIGH);
  delay(1);
  GPIOPinWrite(SOC_GPIO_2_REGS,GPIO_INSTANCE_PIN_NUMBER_1,GPIO_PIN_LOW);
  delay(1);
  GPIOPinWrite(SOC_GPIO_2_REGS,GPIO_INSTANCE_PIN_NUMBER_1,GPIO_PIN_HIGH);
  delay(1);

  I2CInit(I2C_0);
  I2CMasterSlaveAddrSet(SOC_I2C_0_REGS, CAPTOUCH_I2C_SLAVE_ADDR);

}


int CapTouchDiag(void)
{
  int val=0;

  UARTprintf("\n***************************\n");
  UARTprintf(  "*      CapTouch Test      *\n");
  UARTprintf(  "***************************\n");

  CapTouchSetup();

  val=CapTouchReadByte(ID_G_CIPHER);
  if(val!=0x00)
  {
    UARTprintf("CapTouch screen detected.\n");
    if(!CapTouchTest())
    {
      UARTprintf("PASS: CapTouch test\n");
      return 0;
    }
    else
      UARTprintf("FAIL: CapTouch test\n");
  }
  else
    UARTprintf("FAIL: CapTouch test\n");


  return -1;

}

