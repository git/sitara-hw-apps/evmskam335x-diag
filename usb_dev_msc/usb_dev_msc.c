/*
 * usb_dev_msc.c
 *
 * Main routines for the device mass storage class example.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "hw_types.h"
#include "soc_AM335x.h"
#include "evmskAM335x.h"
#include "hw_usb.h"
#include "debug.h"
#include "interrupt.h"
#include "usb.h"
#include "usblib.h"
#include "usb-ids.h"
#include "usbdevice.h"
#include "usbdmsc.h"
#include "usb_msc_structs.h"
#include "cache.h"
#include "mmu.h"
#include "cppi41dma.h"
#include "uartStdio.h"
#include "delay.h"
#include "uart_irda_cir.h"
#include "cp15.h"
#include "evmskAM335x_diag.h"

#if 0

#if defined(__IAR_SYSTEMS_ICC__)
#pragma data_alignment=(16*1024)
static volatile unsigned int pageTable[4*1024];
#else
static volatile unsigned int pageTable[4*1024]__attribute__((aligned(16*1024)));
#endif
#endif
unsigned char *dataBuffer;

extern endpointInfo usb_dev_msc_epInfo[];

#define START_ADDR_DDR                     (0x80000000)
#define START_ADDR_DEV                     (0x44000000)
#define START_ADDR_OCMC                    (0x40300000)
#define NUM_SECTIONS_DDR                   (512)
#define NUM_SECTIONS_DEV                   (960)
#define NUM_SECTIONS_OCMC                  (1)


//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>USB MSC Device (usb_dev_msc)</h1>
//!
//! This example application turns the evaluation board into a USB mass storage
//! class device.  The application will use the microSD card for the storage
//! media for the mass storage device.  The screen will display the current
//! action occurring on the device ranging from disconnected, no media, reading,
//! writing and idle.
//
//*****************************************************************************

//*****************************************************************************
//
// The USB controller instance
//
//*****************************************************************************
#define USB_INSTANCE 0

//*****************************************************************************
//
// The number of ticks to wait before falling back to the idle state.  Since
// the tick rate is 100Hz this is approximately 3 seconds.
//
//*****************************************************************************
#define USBMSC_ACTIVITY_TIMEOUT 30

//*****************************************************************************
//
// DMA Configuration.
//
//*****************************************************************************
#define NUMBER_OF_ENDPOINTS   2 //Total number of send points(RX +TX) used in this USB configuration
#define USB_MSC_BUFER_SIZE    512

//*****************************************************************************
//
// This enumeration holds the various states that the device can be in during
// normal operation.
//
//*****************************************************************************
volatile enum
{
    //
    // Unconfigured.
    //
    MSC_DEV_DISCONNECTED,

    //
    // Connected but not yet fully enumerated.
    //
    MSC_DEV_CONNECTED,

    //
    // Connected and fully enumerated but not currently handling a command.
    //
    MSC_DEV_IDLE,

    //
    // Currently reading the SD card.
    //
    MSC_DEV_READ,

    //
    // Currently writing the SD card.
    //
    MSC_DEV_WRITE
}
g_eMSCState;

//*****************************************************************************
//
// The Flags that handle updates to the status area to avoid drawing when no
// updates are required..
//
//*****************************************************************************
#define FLAG_UPDATE_STATUS      1
static unsigned int g_ulFlags;
static unsigned int g_ulIdleTimeout;
unsigned int g_bufferIndex = 0;


//*****************************************************************************
//
// Handles bulk driver notifications related to the receive channel (data from
// the USB host).
//
// \param pvCBData is the client-supplied callback pointer for this channel.
// \param ulEvent identifies the event we are being notified about.
// \param ulMsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the bulk driver to notify us of any events
// related to operation of the receive data channel (the OUT channel carrying
// data from the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
unsigned int
RxHandler(void *pvCBData, unsigned int ulEvent,
               unsigned int ulMsgValue, void *pvMsgData)
{
    return(0);
}

//*****************************************************************************
//
// Handles bulk driver notifications related to the transmit channel (data to
// the USB host).
//
// \param pvCBData is the client-supplied callback pointer for this channel.
// \param ulEvent identifies the event we are being notified about.
// \param ulMsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the bulk driver to notify us of any events
// related to operation of the transmit data channel (the IN channel carrying
// data to the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
unsigned int
TxHandler(void *pvCBData, unsigned int ulEvent, unsigned int ulMsgValue,
          void *pvMsgData)
{
    return(0);
}



//*****************************************************************************
//
// Sets up the AINTC Interrupt
//
//*****************************************************************************
static void USB0AINTCConfigure(void)
{
    /* Initializing the ARM Interrupt Controller. */
    //IntAINTCInit();

    /* Registering the Interrupt Service Routine(ISR). */
    IntRegister(SYS_INT_USB0, USB0DeviceIntHandler);

    /* Setting the priority for the system interrupt in AINTC. */
    IntPrioritySet(SYS_INT_USB0, 0, AINTC_HOSTINT_ROUTE_IRQ);

    /* Enabling the system interrupt in AINTC. */
    IntSystemEnable(SYS_INT_USB0);
}

static void CPDMAAINTCConfigure(void)
{

    /* Registering the Interrupt Service Routine(ISR). */
    IntRegister(SYS_INT_USBSSINT, USB0DeviceIntHandler);

    /* Setting the priority for the system interrupt in AINTC. */
    IntPrioritySet(SYS_INT_USBSSINT, 0, AINTC_HOSTINT_ROUTE_IRQ);

    /* Enabling the system interrupt in AINTC. */
    IntSystemEnable(SYS_INT_USBSSINT);
}

static void USBInterruptEnable(void)
{
    /* Configuring AINTC to receive UART0 interrupts. */
    USB0AINTCConfigure();

  CPDMAAINTCConfigure();
}

//*****************************************************************************
//
// This function is the call back notification function provided to the USB
// library's mass storage class.
//
//*****************************************************************************
unsigned int
USBDMSCEventCallback(void *pvCBData, unsigned int ulEvent,
                     unsigned int ulMsgParam, void *pvMsgData)
{
    //
    // Reset the time out every time an event occurs.
    //
    g_ulIdleTimeout = USBMSC_ACTIVITY_TIMEOUT;

    switch(ulEvent)
    {
        //
        // Writing to the device.
        //
        case USBD_MSC_EVENT_WRITING:
        {
            //
            // Only update if this is a change.
            //
            if(g_eMSCState != MSC_DEV_WRITE)
            {
                //
                // Go to the write state.
                //
                g_eMSCState = MSC_DEV_WRITE;

                //
                // Cause the main loop to update the screen.
                //
                g_ulFlags |= FLAG_UPDATE_STATUS;
            }

            break;
        }

        //
        // Reading from the device.
        //
        case USBD_MSC_EVENT_READING:
        {
            //
            // Only update if this is a change.
            //
            if(g_eMSCState != MSC_DEV_READ)
            {
                //
                // Go to the read state.
                //
                g_eMSCState = MSC_DEV_READ;

                //
                // Cause the main loop to update the screen.
                //
                g_ulFlags |= FLAG_UPDATE_STATUS;
            }

            break;
        }
        case USBD_MSC_EVENT_IDLE:
        default:
        {
            break;
        }
    }

    return(0);
}


//*****************************************************************************
//
// This is the main loop that runs the application.
//
//*****************************************************************************
int usb_dev_msc_init(void)
{
  //
  //USB module clock enable
  //
  USB0ModuleClkConfig();

  //
  //USB Interrupt enable
  //
  USBInterruptEnable();

  //
  //Delay timer setup
  //
  DelayTimerSetup();

  //
  // Initialize the idle timeout and reset all flags.
  //
  g_ulIdleTimeout = 0;
  g_ulFlags = 0;

  //
  // Initialize the state to idle.
  //
  g_eMSCState = MSC_DEV_IDLE;

  USBDMSCInit(0, (tUSBDMSCDevice *)&g_sMSCDevice);

#ifdef DMA_MODE
  Cppi41DmaInit(USB_INSTANCE, usb_dev_msc_epInfo, NUMBER_OF_ENDPOINTS);

  for(;g_bufferIndex < NUM_OF_RX_BDs; g_bufferIndex++)
  {
    dataBuffer = (unsigned char *)cppiDmaAllocBuffer();
    doDmaRxTransfer(USB_INSTANCE, USB_MSC_BUFER_SIZE, dataBuffer,
              g_sMSCDevice.psPrivateData->ucOUTEndpoint);
  }
#endif
  return 0;
}

int usb_dev_msc_test(void)
{
  signed char testquit=0;
  unsigned char usbd_rwF=0; //for checking read/write access

  UARTPuts("\r\n",-1);
    //
    // Drop into the main loop.
    //

   while(1)
    {
    if(g_ulIdleTimeout != 0)
    {
      g_ulIdleTimeout--;
    }

        switch(g_eMSCState)
        {
            case MSC_DEV_READ:
            {
                //
                // Update the screen if necessary.
                //
                if(g_ulFlags & FLAG_UPDATE_STATUS)
                {
                    g_ulFlags &= ~FLAG_UPDATE_STATUS;
                    UARTPuts("\rReading",-1);
                    usbd_rwF|=0x01;
                }

                //
                // If there is no activity then return to the idle state.
                //
                if(g_ulIdleTimeout == 0)
                {
                    g_eMSCState = MSC_DEV_IDLE;

                }
                //timeout=0;

                break;
            }
            case MSC_DEV_WRITE:
            {
                //
                // Update the screen if necessary.
                //
                if(g_ulFlags & FLAG_UPDATE_STATUS)
                {
                    g_ulFlags &= ~FLAG_UPDATE_STATUS;
                    UARTPuts("\rWriting",-1);
                    usbd_rwF|=0x02;
                }

                //
                // If there is no activity then return to the idle state.
                //
                if(g_ulIdleTimeout == 0)
                {
                    g_eMSCState = MSC_DEV_IDLE;
                }

                break;
            }
            case MSC_DEV_IDLE:
            default:
            {

        break;
            }
        }


      testquit = UARTCharGetNonBlocking(UART_CONSOLE_BASE);

      if(testquit==0x1B)
        {
          UARTPuts("\nUSB Dev MSC Test completed\n\r", -1);

          // Disabling the system interrupt in AINTC.
          IntSystemDisable(SYS_INT_USB0);

          // Disabling the system interrupt in AINTC.
          IntSystemDisable(SYS_INT_USBSSINT);

          // UnRegistering the Interrupt Service Routine(ISR).
          IntUnRegister(SYS_INT_USB0);
          // UnRegistering the Interrupt Service Routine(ISR).
          IntUnRegister(SYS_INT_USBSSINT);

          if(usbd_rwF==0x03)
            return 0;
          else
            return -1;
        }


    }
}



