/*
 * evmskAM335x_diag.h
 *
 * This file contains the main() and other functions.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef EVMSKAM335X_DIAG_H_
#define EVMSKAM335X_DIAG_H_

/*****************************************************************************
**                    Macro definitions
*****************************************************************************/

//#define CAPTOUCH  /* To be enabled for the capacitive touchscreen board */

#ifdef CAPTOUCH
#define VERSION   "3.3a"
#else
#define VERSION   "3.3"
#endif

#define BOARD_HEADER      "AA5533EE"
#define BOARD_NAME        "A335X_SK"
#define BOARD_VERSION     "1.2B"

#define HW_FLOW


#define CLKOUT1_SEL_MODE          0x03

#define  GPIO_INSTANCE_PIN_NUMBER_0      (0x00)
#define  GPIO_INSTANCE_PIN_NUMBER_1      (0x01)
#define  GPIO_INSTANCE_PIN_NUMBER_2      (2u)
#define  GPIO_INSTANCE_PIN_NUMBER_3      (3u)
#define  GPIO_INSTANCE_PIN_NUMBER_4      (4u)
#define  GPIO_INSTANCE_PIN_NUMBER_5      (5u)
#define  GPIO_INSTANCE_PIN_NUMBER_6      (6u)
#define  GPIO_INSTANCE_PIN_NUMBER_7      (7u)
#define  GPIO_INSTANCE_PIN_NUMBER_28     (0x1C)
#define  GPIO_INSTANCE_PIN_NUMBER_29     (0x1D)
#define  GPIO_INSTANCE_PIN_NUMBER_30     (0x1E)

/* Wrapper Definitions related to GPIO Keypad Pins */
#define GPIO_BASE_ADD_KEYPAD_SW1_2_4  (SOC_GPIO_2_REGS)
#define GPIO_SW1_PIN_NUM               (3u)
#define GPIO_SW2_PIN_NUM               (2u)
#define GPIO_SW4_PIN_NUM               (5u)

#define GPIO_BASE_ADD_KEYPAD_SW3    (SOC_GPIO_0_REGS)
#define GPIO_SW3_PIN_NUM               (30u)


#define GPIO_SYS_INT_NUM_SW1_2_4        (SYS_INT_GPIOINT2A)
#define GPIO_SYS_INT_NUM_SW3          (SYS_INT_GPIOINT0A)


#define UART_CONSOLE_BASE                    (SOC_UART_0_REGS)

#define HSMMCSD_0_CARD_DETECT_PINNUM     (6u)

#define ESC 0x1B

#define USER_KEY_1  1
#define USER_KEY_2  2
#define USER_KEY_3  4
#define USER_KEY_4  8

#define LED_ON    1
#define LED_OFF   0

#define LED_1   1
#define LED_2 2
#define LED_3 3
#define LED_4 4

/* LCD brightnes values*/
#define FRAMEBUFFERSIZE   130560   /*SK3358 LCD - 480*272 pixels*/
#define LOWBACKLIGHT       3000
#define MIDBACKLIGHT       1500
#define MAXBACKLIGHT       750

#define LCD_WIDTH     480
#define LCD_HEIGHT    272
/*
 *====================
 * Typedefs
 *====================
 */
typedef unsigned char   UINT8;
typedef unsigned short  UINT16;
typedef unsigned int  UINT32;
typedef char  INT8;
typedef short   INT16;
typedef int   INT32;
typedef int   STATUS;


#define FAIL_DIAG   -1
#define SUCCESS_DIAG 0


/* Global variables */
#if 0
extern volatile unsigned int tCount;
extern volatile unsigned int rCount;
extern volatile unsigned int flag; // = 1;
extern volatile unsigned int numOfBytes;

extern volatile unsigned char dataToSlave[100]; //increased from 2 to 100
extern volatile unsigned char dataFromSlave[50];
#endif
extern volatile unsigned char gKey; //keypad value

extern unsigned int frameBuf[FRAMEBUFFERSIZE];


/*****************************************************************************
**                    FUNCTION PROTOTYPES
*****************************************************************************/

/* GPIO APIs for LED, Keypad, VTT*/
extern unsigned int GPIO0Pin6PinMuxSetup(void);
extern unsigned int GPIO0Pin7PinMuxSetup(void);
extern unsigned int GPIO1Pin30PinMuxSetup(void);
extern void GPIO2ModuleClkConfig(void);
extern unsigned int GPIO2PinMuxSetup(unsigned int pinNo);
extern unsigned int GPIO1PinMuxSetup(unsigned int pinNo);
extern unsigned int GPIO0Pin30PinMuxSetup(void);
extern void GPIOINTCConfigure(void);

extern void Clear_Leds(void);
extern void UserLedOnOff(unsigned int ledno,unsigned int onoff);
extern void UserLedtest(void);
extern int Checkforkeypress(unsigned char testno);

extern void configWakeGpio(void);
extern void enableGpioWake(void);
extern void disableGpioWake(void);


/*LCD, PWM-Ecap APIs*/
extern void PWMSSTBClkEnable(unsigned int);
extern void PWMSSModuleClkConfig(unsigned int instanceNum);

extern int Lcdtest(void);
extern void EcapInit(void);
extern void EcapBkLightEnable(void);
extern void EcapBkLightDisable(void);
extern void EcapBkLightDim(void);
extern void EnableBackLight(void);
extern void DisableBackLight(void);
extern void EcapBkLightVary(unsigned int value);
extern void LCDCEraseBuffer(void);
extern void LCDCDrawBorder(void);
extern void SetUpLCD(void);
extern void LCDFBConfig(void);
extern void SquareBoxDraw(UINT16 x, UINT16 y, UINT16 size, UINT8 r, UINT8 g, UINT8 b);

extern void Raster0Init(void);
extern void Raster0EOFIntEnable(void);
extern void Raster0EOFIntDisable(void);
extern int Lcdtest_gradient(void);
extern void ClearLCD(void);

extern void Delay(volatile unsigned int count);


/*Audio APIs*/
extern void ToneLoopInit(void);
extern void ToneLoopStart(void);
extern void ToneLoopdeInit(void);
extern void AudioCodecInit(void);

extern INT16 LPDDRTest(void);

extern void Clkout1_Setup(void);

extern void E2prominit(void);
extern int E2promtest(void);

extern int Accelerometer_id_test(void);
extern int Accelerometer_full_test(void);

extern int Pmic_test(void);

extern int usb_dev_msc_test(void);
extern int usb_host_msc_test(void);
extern int usb_host_msc_init(void);
extern int usb_dev_msc_init(void);

extern int hs_mmcsd_test(unsigned char idMemF);

extern INT16 DELAYMicroSeconds(UINT32 duration);
extern INT16 DELAYMilliSeconds(UINT32 duration);
extern INT16 EMACChipConfigure(unsigned int);

extern INT16 EMACPinSetup(unsigned int);
extern void EMACClockSetup(unsigned int);

/* Touchscreen APIs */
#ifdef CAPTOUCH
extern int Cap_touch_test(void);
#endif
extern int tsc_test(void);
extern void StepEnable(void);
extern void StepDisable(void);
extern void enableTSWakeup(void);
extern void disableTSWakeup(void);
extern void configTSWakeup(void);




#endif /* EVMSKAM335X_DIAG_H_ */
