/*
 * fat_usbmsc_mmcsd.c
 *
 * This file was modified from a sample available from the FatFS website.
 * It was modified to work with the StarterWare USB stack.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "diskio.h"
#include "hw_types.h"
#include "usblib.h"
#include "usbmsc.h"
#include "usbhost.h"
#include "usbhmsc.h"
#include "ff.h"
#include "mmcsd_proto.h"
#include "hs_mmcsdlib.h"
#include "uartStdio.h"

extern tUSBHMSCInstance g_USBHMSCDevice[];
extern unsigned char usb_flag;

static volatile
DSTATUS USBStat = STA_NOINIT;    /* Disk status */

typedef struct _fatDevice
{
    /* Pointer to underlying device/controller */
    void *dev;

    /* File system pointer */
    FATFS *fs;

  /* state */
  unsigned int initDone;

}fatDevice;


#define DRIVE_NUM_MMCSD     0
#define DRIVE_NUM_MAX      2


fatDevice fat_devices[DRIVE_NUM_MAX];


/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS
disk_initialize(
    BYTE bValue)                /* Physical drive number (0) */
{
  unsigned int ulMSCInstance;

  if(usb_flag)
  {

  ulMSCInstance = (unsigned int)&g_USBHMSCDevice[bValue];

  /* Set the not initialized flag again. If all goes well and the disk is */
    /* present, this will be cleared at the end of the function.            */
    USBStat |= STA_NOINIT;

    /* Find out if drive is ready yet. */
    if (USBHMSCDriveReady(ulMSCInstance)) return(FR_NOT_READY);

    /* Clear the not init flag. */
    USBStat &= ~STA_NOINIT;

  } else
  {
    unsigned int status;

        if (DRIVE_NUM_MAX <= bValue)
        {
            return STA_NODISK;
        }

        if ((DRIVE_NUM_MMCSD == bValue) && (fat_devices[bValue].initDone != 1))
        {
            mmcsdCardInfo *card = (mmcsdCardInfo *) fat_devices[bValue].dev;

            /* SD Card init */
            status = MMCSDCardInit(card->ctrl);

            if (status == 0)
            {
                UARTPuts("\r\nCard Init Failed \r\n", -1);

                return STA_NOINIT;
            }
            else
            {
    #if DEBUG
                if (card->cardType == MMCSD_CARD_SD)
                {
                    UARTPuts("\r\nSD Card ", -1);
                    UARTPuts("version : ",-1);
                    UARTPutNum(card->sd_ver);

                    if (card->highCap)
                    {
                        UARTPuts(", High Capacity", -1);
                    }

                    if (card->tranSpeed == SD_TRANSPEED_50MBPS)
                    {
                        UARTPuts(", High Speed", -1);
                    }
                }
                else if (card->cardType == MMCSD_CARD_MMC)
                {
                    UARTPuts("\r\nMMC Card ", -1);
                }
    #endif
                /* Set bus width */
                if (card->cardType == MMCSD_CARD_SD)
                {
                    MMCSDBusWidthSet(card->ctrl);
                }

                /* Transfer speed */
                MMCSDTranSpeedSet(card->ctrl);
            }

        fat_devices[bValue].initDone = 1;
        }

  }
    return 0;
}



/*-----------------------------------------------------------------------*/
/* Returns the current status of a drive                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
    BYTE drv)                   /* Physical drive number (0) */
{
   if(usb_flag)
   {
  if (drv) return STA_NOINIT;        /* Supports only single drive */
    return USBStat;
   }
   else
     return 0;
}



/*-----------------------------------------------------------------------*/
/* This function reads sector(s) from the disk drive                     */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
    BYTE drv,               /* Physical drive number (0) */
    BYTE* buff,             /* Pointer to the data buffer to store read data */
    DWORD sector,           /* Physical drive nmuber (0) */
    BYTE count)             /* Sector count (1..255) */
{

  unsigned int ulMSCInstance;

  if(usb_flag)
  {

  ulMSCInstance = (unsigned int)&g_USBHMSCDevice[drv];

  if(USBStat & STA_NOINIT)
    {
        return(RES_NOTRDY);
    }

    /* READ BLOCK */
    if (USBHMSCBlockRead(ulMSCInstance, sector, buff, count) == 0)
        return RES_OK;


  }
  else
  {
    if (drv == DRIVE_NUM_MMCSD)
      {
        mmcsdCardInfo *card = fat_devices[drv].dev;

          /* READ BLOCK */
        if (MMCSDReadCmdSend(card->ctrl, buff, sector, count) == 1)
        {
              return RES_OK;
        }
        }

  }

  return RES_ERROR;
}



/*-----------------------------------------------------------------------*/
/* This function writes sector(s) to the disk drive                     */
/*-----------------------------------------------------------------------*/

#if _READONLY == 0
DRESULT disk_write (
    BYTE ucDrive,           /* Physical drive number (0) */
    const BYTE* buff,       /* Pointer to the data to be written */
    DWORD sector,           /* Start sector number (LBA) */
    BYTE count)             /* Sector count (1..255) */
{
  unsigned int ulMSCInstance;

  if(usb_flag)
  {
  ulMSCInstance = (unsigned int)&g_USBHMSCDevice[ucDrive];

    if (ucDrive || !count) return RES_PARERR;
    if (USBStat & STA_NOINIT) return RES_NOTRDY;
    if (USBStat & STA_PROTECT) return RES_WRPRT;

    /* WRITE BLOCK */
    if(USBHMSCBlockWrite(ulMSCInstance, sector, (unsigned char *)buff,
                         count) == 0)
        return RES_OK;

  }
  else
  {
    if (ucDrive == DRIVE_NUM_MMCSD)
      {
        mmcsdCardInfo *card = fat_devices[ucDrive].dev;
          /* WRITE BLOCK */
          if(MMCSDWriteCmdSend(card->ctrl,(BYTE*) buff, sector, count) == 1)
        {
              return RES_OK;
        }
      }
  }

  return RES_ERROR;
}
#endif /* _READONLY */

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
    BYTE drv,               /* Physical drive number (0) */
    BYTE ctrl,              /* Control code */
    void *buff)             /* Buffer to send/receive control data */
{
  if(usb_flag)
  {
    if(USBStat & STA_NOINIT)
    {
        return(RES_NOTRDY);
    }

    switch(ctrl)
    {
        case CTRL_SYNC:
            return(RES_OK);

        default:
            return(RES_PARERR);
    }
  }
  else
  {
    return RES_OK;
  }
}

/*---------------------------------------------------------*/
/* User Provided Timer Function for FatFs module           */
/*---------------------------------------------------------*/
/* This is a real time clock service to be called from     */
/* FatFs module. Any valid time must be returned even if   */
/* the system does not support a real time clock.          */

DWORD get_fattime (void)
{
    return    ((2007UL-1980) << 25) // Year = 2007
            | (6UL << 21)           // Month = June
            | (5UL << 16)           // Day = 5
            | (11U << 11)           // Hour = 11
            | (38U << 5)            // Min = 38
            | (0U >> 1)             // Sec = 0
            ;
}

