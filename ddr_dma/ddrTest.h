/*
 * ddrTest.h
 *
 * DDR test.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef DDRTEST_H_
#define DDRTEST_H_

#include "evmskAM335x.h"
#include "evmskam335x_diag.h"

/*
 *====================
 * Defines
 *====================
 */

#ifndef NULL
#    define NULL 0
#endif

/*
 *======================
 *Macros
 *======================
 */

//#define FAIL -1
//#define SUCCESS 0




#define  LPDDR_PATTERN1_ADDR  0x85000000

#define  LPDDR_PATTERN_START_ADDR  0x85000000
#define  LPDDR_PATTERN_LENGTH    0x07FFFFFF

#define LPDDR_DATA_WIDTH     16

/* LPDDR */
#define LPDDR_PATTERN0_ERR  -50   /*LPDDR Rolling 1 test failed */
#define LPDDR_PATTERN1_ERR  -51   /*LPDDR pattern 0xAAAAAAAA test failed */
#define LPDDR_PATTERN2_ERR  -52   /*LPDDR pattern 0x55555555 test failed */

INT16 LPDDRPatternTest (UINT16 patternSel);
INT16 LPDDRTest (void);


#endif /* DDRTEST_H_ */
