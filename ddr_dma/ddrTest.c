/*
 * ddrTest.c
 *
 * DDR memory test.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include"ddrTest.h"
#include "uartStdio.h"
#include "evmskAM335x.h"
#include "evmskam335x_diag.h"

#define DEBUG_LPDDR 1


/**
 * LPDDRPatternTest
 *
 * This function is used to write and verify the patterns in the LPDDR
 *
 *
 * @param
 *    patternSel      [IN]    Pattern Selection number
 *       0 � Rolling 0 pattern test
           1 � 0xAAAAAAAA pattern test
           2 - 0x55555555 pattern test
 *
 *
 * @return
 * @        returns SUCCESS_DIAG.
 *          LPDDR_PATTERN0_ERR
 *          LPDDR_PATTERN1_ERR
 *          LPDDR_PATTERN2_ERR
 *
 */
INT16 LPDDRPatternTest (UINT16 patternSel)
{
  UINT32 data = 0;
  UINT32 *ptr;
  UINT32 count = 0;
  UINT32 tempCount = 0;


  /* Pattern 0 selection - rolling 1 test  */
  if(0 == patternSel)
  {
    /* initialize the data */
    data = 0x00000001;

    /* Write and read back the data for verifying the LPDDR data bus */
    for(count = 0; count < LPDDR_DATA_WIDTH; count++)
    {
      /* Write the data */
      *(UINT32 *)LPDDR_PATTERN1_ADDR = (data << count);

      /* Give some delay */
      for(tempCount = 0; tempCount < 100; tempCount++);

      /* Read back the data */
      if((data << count) != *(UINT32 *)LPDDR_PATTERN1_ADDR)
      {
        UARTPuts("\n\nDDR3 dataline -Rolling 1 Test failed\n",-1);
        return LPDDR_PATTERN0_ERR;
      }
    }

    UARTPuts("\n\nDDR3 dataline - Rolling 1 Test Pass\n",-1);
  }/* Pattern 1 selection - pattern 0xAAAAAAAA  */
  else if(1 == patternSel || 2 == patternSel )
  {
    /* initialize the data */
    if( 1 == patternSel )
    {
      data = 0xAAAAAAAA;
    }
    else
    {
      data = 0x55555555;
    }

    /* Initialize the starting address */
    ptr = (UINT32*)LPDDR_PATTERN_START_ADDR;

    /* Write and read back the data for verifying the LPDDR */
    for(count = 0; count < (LPDDR_PATTERN_LENGTH / 4); count++)
    {
      /* Write the data */
      *ptr++ = data;
    }

    /* reassign the starting address */
    ptr = (UINT32*)LPDDR_PATTERN_START_ADDR;

    /* Read back the data */
    for(count = 0; count < (LPDDR_PATTERN_LENGTH / 4); count++)
    {
      if(data != *ptr++)
      {
        if( 1 == patternSel )
        {
          UARTPuts("\n\n DDR3 Pattern 0xAAAAAAAA Test failed \n",-1);
          return LPDDR_PATTERN1_ERR;
        }
        else
        {
#ifdef DEBUG_LPDDR
          UARTPuts("\n\n DDR3 Pattern 0x55555555 Test failed \n",-1);
#endif
          return LPDDR_PATTERN2_ERR;

        }

      }
    }

    if( 1 == patternSel )
      {
        UARTPuts("\n\n DDR3 Pattern 0xAAAAAAAA Test Pass \n",-1);
      }
      else
      {

        UARTPuts("\n\n DDR3 Pattern 0x55555555 Test Pass \n",-1);
      }


  }



  return SUCCESS_DIAG;

}



/**
 * LPDDRTest
 *
 * This function is used to call patterns function, It called
 * from diagnostic suite.

 * @return
 * @        returns SUCCESS_DIAG.
 *          FAIL_DIAG
 *
 */
INT16 LPDDRTest (void)
{
  INT16 retVal = 0;

  /* Call the rolling 1 test */
  retVal = LPDDRPatternTest(0);
  if(SUCCESS_DIAG != retVal)
  {
    return FAIL_DIAG;
  }
#if 0

#ifdef DEBUG_LPDDR
  UARTPuts("\n\n  DDR Pattern 0xAAAAAAAA Test processing \n",-1);
#endif


  /* Call the pattern 0xAAAAAAAA test */
  retVal = LPDDRPatternTest(1);
  if(SUCCESS_DIAG != retVal)
  {
    return FAIL_DIAG;
  }
#ifdef DEBUG_LPDDR
  UARTPuts("\n\n  DDR Pattern 0xAAAAAAAA Test completed \n",-1);

  UARTPuts("\n\nDDR Pattern 0x55555555 Test processing \n",-1);
#endif
  /* Call the pattern 0x55555555 test */
  retVal = LPDDRPatternTest(2);
  if(SUCCESS_DIAG != retVal)
  {
    return FAIL_DIAG;
  }
#ifdef DEBUG_LPDDR
  UARTPuts("\n\n  DDR Pattern 0x55555555 Test completed \n",-1);
#endif
#endif

  return SUCCESS_DIAG;
}


