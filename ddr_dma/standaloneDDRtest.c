/*
 * standaloneDDRtest.c
 *
 * Stand-alone DDR test.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "standaloneDDRtest.h"
//#include <stdio.h>
#include "uartStdio.h"
#include "hw_cm_per.h"
#include "soc_AM335x.h"
#include "edma.h"

/* EDMA3 Event queue number. */
#define EVT_QUEUE_NUM             (0)

void memWrite(unsigned int Addr, unsigned int size, unsigned int initData)
{
unsigned int len=0;
  for(len=0; len<size; len++)
  {
    HWREG(Addr+4*len) = (initData+len);
  }
}

INT16 DDR_DMATest(void)
{
  unsigned int pattern=0;
  unsigned int queuqePri=0;
  UINT16 retVal=0;
  UINT32 i;
  UINT32 ddrDMAaddress = 0;
  UINT16 failures = 0;

  //EdmaPrcm();
  EDMA3Init(SOC_EDMA30CC_0_REGS, 0);

  UARTPuts ("\nClearing DDR and SRAM\n",-1);
  memWrite(DDR_PATTERN_START_ADDR, DMA_SIZE, 0);  //clear DDR
    memWrite(INTERNAL_SRAM_BASE_ADDR, DMA_SIZE, 0); //clear SRAM
    /*DDR -> SRAM DMA data Transfer */
    pattern=0xAAAAAAAA;
  queuqePri=0;

  UARTPuts ("\nFill DDR with pattern\n",-1);
  memWrite(DDR_PATTERN_START_ADDR, DMA_SIZE, pattern);  //fill DDR with pattern
  edmaConfigure(DDR_PATTERN_START_ADDR, INTERNAL_SRAM_BASE_ADDR, DMA_SIZE, queuqePri); //DMA from DDR to SRAM
  retVal = compare(INTERNAL_SRAM_BASE_ADDR, DMA_SIZE, pattern);

  if(retVal!=SUCCESS)
  {
    UARTPuts ("\n DDR DMA Pattern 0xAAAAAAAA Test Failed \n ",-1);
    failures++;
  }

  /*SRAM->DDR EDMA data Transfer */

  memWrite((INTERNAL_SRAM_BASE_ADDR), DMA_SIZE, pattern);
  for (i=0; i < (TOTAL_DDR_DENSITY/DMA_SIZE_BYTES); i++)
  {
    ddrDMAaddress=(DDR_PATTERN_START_ADDR+(DMA_SIZE_BYTES*i));

    if(i%0x100==0)
    {
        //printf("\n0x%x Test in progress currently @0x%x",pattern,ddrDMAaddress);
        UARTPuts("\n",-1);
        UARTPutHexNum(pattern);
        UARTPuts(" Test in progress currently ",-1);
        UARTPutHexNum(ddrDMAaddress);
    }


    edmaConfigure((INTERNAL_SRAM_BASE_ADDR), ddrDMAaddress, DMA_SIZE, queuqePri);
    retVal = DMA_compare((DDR_PATTERN_START_ADDR+(DMA_SIZE_BYTES*i)), (DMA_SIZE), pattern);
    if(retVal!=SUCCESS)
    {
      UARTPuts("\n SRAM DMA Pattern 0xAAAAAAAA Test failed \n",-1);
      failures++;
    }



  }

  UARTPuts ("\n DMA pattern 0xAAAAAAAA test complete",-1);

  /* Disable EDMA for the transfer */
  EDMA3DisableTransfer(SOC_EDMA30CC_0_REGS, 1,EDMA3_TRIG_MODE_MANUAL);

  EDMA3FreeChannel(SOC_EDMA30CC_0_REGS, EDMA3_CHANNEL_TYPE_DMA,1, EDMA3_TRIG_MODE_MANUAL, 1,0);

  EDMA3Deinit(SOC_EDMA30CC_0_REGS, 0);

    if (failures>=1)
    {
      UARTPuts("\n",-1);
      UARTPutNum(failures);
      UARTPuts(" errors found\n",-1);
      return FAIL;
    }

    //printf ("\nDDR test passed from 0x%x to 0x%x",DDR_PATTERN_START_ADDR,(DDR_PATTERN_START_ADDR+TOTAL_DDR_DENSITY-1));
    UARTPuts("\nDDR test passed from ",-1);
    UARTPutHexNum(DDR_PATTERN_START_ADDR);
    UARTPuts(" to ",-1);
    UARTPutHexNum((DDR_PATTERN_START_ADDR+TOTAL_DDR_DENSITY-1));
    UARTPuts("\n",-1);

    return SUCCESS;
}


INT16 edmaConfigure(unsigned int srcAddr, unsigned int DstAddr, unsigned short dmaSize, unsigned int QUEPRI)
{
  //printf ("EDMA Transfer Start for QUEPRI %x\n",QUEPRI);
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_QUEPRI)   = (QUEPRI & 0x7);
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_DRAE(0))  = 0x00000001;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_DMAQNUM(0)) = 0x00000000;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_DCHMAP(0))  = 0x00000000;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_OPT(0)) = 0x00100200;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_SRC(0)) = srcAddr;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_DST(0)) = DstAddr;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_A_B_CNT(0)) = ((0x00010000) | (dmaSize * 4)); /* byte len */
  //HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_A_B_CNT(0) ) = ((0x00010000) | (dmaSize)); // byte len -
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_CCNT(0))  = 0x00000001;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_SRC_DST_BIDX(0))  = 0x00000000;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_SRC_DST_CIDX(0))  = 0x00000000;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_LINK_BCNTRLD(0))  = 0x0000ffff;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_IESR) = 0x00000001;
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_ESR)  = 0x00000001;

  while( (HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_IPR) & 0x1) !=1 ) {}
  HWREG(SOC_EDMA30CC_0_REGS + EDMA3CC_ICR) = 0x1;
  //printf("\EDMA Transfer Complete for QUEPRI %x\n", QUEPRI);

  return 0;
}


INT16 compare(UINT32 Addr, UINT32 size, UINT32 initData)
{
  unsigned int len=0, fail=0;
  for(len=0; len<size; len++) {
     if( HWREG(Addr+4*len) != (initData+len) )
     {
     //printf ("\nFailed@%x\tExpected=%x \tRead=%x\n", (Addr+4*len),(initData+len),RD_MEM_32(Addr+4*len) );
     fail++;
     }
     }
  if(fail!=0) {
    //printf("\nTest Case Failed %d locations",fail);
    return FAIL;
    }
 // printf ("\nTest Case Passed for Destination Addr=%x\n", (Addr));
  return SUCCESS;
}


INT16 EdmaPrcm(void)
{
  HWREG(SOC_PRCM_REGS + CM_PER_TPCC_CLKCTRL)  = 2; // it was L3
  HWREG(SOC_PRCM_REGS + CM_PER_TPTC0_CLKCTRL) = 2;
  HWREG(SOC_PRCM_REGS + CM_PER_TPTC1_CLKCTRL) = 2;
  HWREG(SOC_PRCM_REGS + CM_PER_TPTC2_CLKCTRL) = 2;
    return 0;
}

INT16 DMA_compare(UINT32 Addr, UINT32 size, UINT32 initData)
{
  unsigned int len=0, fail=0;

  for(len=0; len<size; len++) {
     if( HWREG(Addr+4*len) != (initData+len) )
     {
     //printf ("\nFailed@%x\tExpected=%x \tRead=%x\n", (Addr+4*len),(initData+len),RD_MEM_32(Addr+4*len) );
         UARTPuts("\nFailed",-1);
         UARTPutHexNum((Addr+4*len));
       UARTPuts("\tExpected=",-1);
       UARTPutHexNum((initData+len));
       UARTPuts(" \tRead=",-1);
       UARTPutHexNum((HWREG(Addr+4*len)));
       UARTPuts("\n=",-1);
       fail++;
     }
     }
  if(fail!=0) {
    return FAIL;
    }
  return SUCCESS;
}



