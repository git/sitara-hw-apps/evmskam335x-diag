/*
 * standaloneDDRtest.h
 *
 * Stand-alone DDR test.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifdef __cplusplus
extern "C" {
#endif

#include "hw_types.h"
/*
 *====================
 * Typedefs
 *====================
 */

typedef unsigned char   UINT8;
typedef unsigned short  UINT16;
typedef unsigned int  UINT32;
typedef char  INT8;
typedef short   INT16;
typedef int   INT32;
typedef int   STATUS;

/*
 *======================
 *Macros
 *======================
 */

#define FAIL -1
#define SUCCESS 0



#define  DDR_PATTERN1_ADDR    0x86000000  //0x80000000
#define  DDR_PATTERN_START_ADDR 0x86000000  //0x80000000
#define INTERNAL_SRAM_BASE_ADDR 0x40302000
#define TPCC_BASE_ADDR      0x49000000
#define PRCM_BASE_ADDR        0x44E00000

#define  TOTAL_DDR_DENSITY     0x8000000 //128MB
//#define  TOTAL_DDR_DENSITY     0x20000000 //15X15 (4Gb)
#define  DDR_TEST_LENGTH    0x10000
#define DMA_SIZE        0x2000  //8K Words //old setting 16K 0x3FFF
#define DMA_SIZE_BYTES      (DMA_SIZE * 4)//BYTES



#define LPDDR_DATA_WIDTH     16

#define WR_MEM_32(a, d) (*(unsigned int*)(a) = (d))
#define RD_MEM_32(a) (*(unsigned int*)(a))

INT16 DDR_DMATest (void);

INT16 edmaFill(UINT32 addr, UINT16 dmaSize);
INT16 edmaWrite(UINT32 DstAddr, UINT32 srcAddr, UINT16 dmaSize, UINT16 QUEPRI);
INT16 EdmaPrcm();
INT16 compare(UINT32 Addr, UINT32 size, UINT32 initData);
INT16 DMA_compare(UINT32 Addr, UINT32 size, UINT32 initData);
INT16 edmaConfigure(unsigned int srcAddr, unsigned DstAddr, unsigned short dmaSize, unsigned int QUEPRI);

#ifdef __cplusplus
}    /* End of extern C */
#endif    /* #ifdef __cplusplus */


