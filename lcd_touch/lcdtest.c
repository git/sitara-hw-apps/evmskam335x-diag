/*
 * lcdtest.c
 *
 * LCD Test application
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "hsi2c.h"
#include "soc_AM335x.h"
#include "gpio_v2.h"
#include "raster.h"
#include "hw_types.h"
#include "uart_irda_cir.h"
#include "interrupt.h"
#include "uartStdio.h"
#include "evmskAM335x.h"
#include "ecap.h"
#include "LCD_3colors.h"
#include "evmskam335x_diag.h"
#include "delay.h"



/******************************************************************************
**                      INTERNAL MACRO DEFINITIONS
*******************************************************************************/



/*****************************************************************************
**                INTERNAL FUNCTION PROTOTYPES
*****************************************************************************/


/******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
volatile unsigned char COLOR;
unsigned int frameBuf[FRAMEBUFFERSIZE];


/*
** Enables Backlight for Ecap
*/
void EcapBkLightEnable(void)
{
    /* Start the counter */
    ECAPCounterControl(SOC_ECAP_2_REGS, ECAP_COUNTER_FREE_RUNNING);

    /* Configure ECAP to generate PWM waveform with 0% duty cycle */
    ECAPAPWMCaptureConfig(SOC_ECAP_2_REGS, 3000, 3300);
}

/*
** Disables Backlight for Ecap
*/
void EcapBkLightDisable(void)
{
    /* Start the counter */
    ECAPCounterControl(SOC_ECAP_2_REGS, ECAP_COUNTER_FREE_RUNNING);

    /* Configure ECAP to generate PWM waveform with 90% duty cycle */
    ECAPAPWMCaptureConfig(SOC_ECAP_2_REGS, 0, 3300);
}

/*
** Dims Backlight for Ecap
*/
void EcapBkLightDim(void)
{
    /* Start the counter */
    ECAPCounterControl(SOC_ECAP_2_REGS, ECAP_COUNTER_FREE_RUNNING);

    /* Configure ECAP to generate PWM waveform */
    ECAPAPWMCaptureConfig(SOC_ECAP_2_REGS, 1500, 3300);
}

/*
** Varies backlight for Ecap
*/
void EcapBkLightVary_Demo(void)
{
    signed int cnt = 2500;
    //volatile unsigned int delay;

    /* Set Counter */
    ECAPCounterConfig(SOC_ECAP_2_REGS, 0);

    UARTPuts("\nLCD Brightness Control(Low to High):\n", -1);

    do
    {
            /* Configure ECAP to generate PWM waveform with duty cycle */
            ECAPAPWMCaptureConfig(SOC_ECAP_2_REGS, 3000 - cnt, 3300);


            delay(1);

            UARTPuts("\r    \r", -1);
            UARTPutNum(cnt);
            cnt--;

    }while(cnt >= 0);
    UARTPuts("\n", -1);

    UARTPuts("\nLCD Brightness Control(High to Low):\n", -1);

    do
    {
        /* Configure ECAP to generate PWM waveform with duty cycle */
        ECAPAPWMCaptureConfig(SOC_ECAP_2_REGS, 3000 - cnt, 3300);

        delay(1);

        UARTPuts("\r", -1);
        UARTPutNum(cnt);
        cnt++;
    }while(cnt <= 2500);


}

/*
** Varies backlight for Ecap
*/
void EcapBkLightVary(unsigned int value)
{
    //volatile unsigned int delay;

    /* Set Counter */
    ECAPCounterConfig(SOC_ECAP_2_REGS, 0);

    /* Configure ECAP to generate PWM waveform with duty cycle */
    ECAPAPWMCaptureConfig(SOC_ECAP_2_REGS, 3000 - value, 3300);

    //delay = 0x5FF;
    //while(delay--);
    delay(100);
}


/*
** Initializes ECAP
*/
void EcapInit(void)
{
    /* Enable Interface and Module Clocks for PWMSS */
    PWMSSModuleClkConfig(2);

    /* Enable Clock */
    ECAPClockEnable(SOC_PWMSS2_REGS);

    /* Configure ECAP to Generate PWM wave form */
    ECAPOperatingModeSelect(SOC_ECAP_2_REGS, ECAP_APWM_MODE);

    /* Set Counter */
    ECAPCounterConfig(SOC_ECAP_2_REGS, 0);

    /* Configure ECAP_PWM_OUT pin as high */
    ECAPAPWMPolarityConfig(SOC_ECAP_2_REGS, ECAP_APWM_ACTIVE_LOW);
}


int Lcdtest_gradient(void)
{
  unsigned int pc, pixcnt=0;
  unsigned int i;
  SetUpLCD();

      delay(100);

      EcapBkLightVary(MAXBACKLIGHT);

      UARTPuts("\n",-1);

      UARTPuts("Display Gradient                       \n", -1);
      COLOR = 0;
      for (i=0; i<90; i++)
      {
        for (pc=0; pc<80; pc++)
        {
              pixcnt++;
              COLOR=0xFF;
              frameBuf[(i*480)+pc+0] = COLOR<<0;
              frameBuf[(i*480)+pc+80] = COLOR<<8;
              frameBuf[(i*480)+pc+160] = COLOR<<16;
              frameBuf[(i*480)+pc+240] = COLOR<<8;
              frameBuf[(i*480)+pc+320] = COLOR<<0;
              frameBuf[(i*480)+pc+400] = COLOR<<16;
          }

          if(COLOR<0xFF)
          {
            COLOR++;
          }
      }

      for (i=90; i<120; i++)
      {
        for (pc=0; pc<480; pc++)
        {
              pixcnt++;
              COLOR=(255*pc)/480;
              frameBuf[(i*480)+pc+0] = COLOR;
              frameBuf[((i+30)*480)+pc+0] = (255-COLOR)<<8;
              frameBuf[((i+60)*480)+pc+0] = COLOR<<16;
          }

        if (COLOR<0xFF)
        {
             COLOR++;
          }
      }

      for (i=180; i<190; i++)
      {
        for (pc=0; pc<480; pc++)
        {
              pixcnt++;
              COLOR=(255*pc)/480;
              frameBuf[((i+0) *480)+pc+0] = 0xFF<<0;
              frameBuf[((i+10)*480)+pc+0] = 0XFF<<8;
              frameBuf[((i+20)*480)+pc+0] = 0XFF<<16;
              frameBuf[((i+30)*480)+pc+0] = 0X00FFFFFF;
              frameBuf[((i+40)*480)+pc+0] = 0X00000000;
          }

        if (COLOR<0xFF)
        {
             COLOR++;
          }
      }


      for (i=230; i<272; i++)
      {
        for (pc=0; pc<480; pc++)
        {
              pixcnt++;
              COLOR=(255*pc)/480;
              frameBuf[(i*480)+pc+0] = (COLOR) + (COLOR<<8) + (COLOR<<16);
          }

        if (COLOR<0xFF)
          {
             COLOR++;
          }
      }

      return 0;
}


void ClearLCD(void)
{
  unsigned int pc;

    /*Clear LCD*/
    for (pc=0; pc<FRAMEBUFFERSIZE; pc++)
    {
      frameBuf[pc] = 0;
    }
}

void PixelDraw(UINT16 x,UINT16 y,UINT8 r,UINT8 g,UINT8 b)
{
   UINT32 pixel;
   UINT32 color;

   pixel = ((y*480) + x);
   color = 0 + (r<<16) + (g<<8) + b;

   frameBuf[pixel] = color;
}

void LCDCEraseBuffer(void)
{
     UINT32 x,y;

     y = 0;
     x = 0;

     for (y = 0; y < LCD_HEIGHT ; y++)
     {
       for (x = 0; x < LCD_WIDTH ; x++)
       {
            PixelDraw(x, y, 0x0, 0x0, 0x0);
       }
     }
}


void LCDCDrawBorder(void)
{
     UINT32 x,y;

     for (y = 0; y < LCD_HEIGHT ; y++)
     {
         PixelDraw(  0, y, 0xFF, 0xFF, 0xFF);
         PixelDraw((LCD_WIDTH-1), y, 0xFF, 0xFF, 0xFF);
     }
     for (x = 0; x < LCD_WIDTH ; x++)
     {
         PixelDraw(x, (LCD_HEIGHT-1), 0xFF, 0xFF, 0xFF);
           PixelDraw(x,   0, 0xFF, 0xFF, 0xFF);
     }

     for (x = 0; x < LCD_WIDTH ; x++)
     {
         PixelDraw(x, ((LCD_HEIGHT/2)-1), 0xFF, 0xFF,0xFF);
           PixelDraw(x, (LCD_HEIGHT-1), 0xFF, 0xFF, 0xFF);
           PixelDraw(x, (LCD_HEIGHT-1), 0xFF, 0xFF, 0xFF);
     }

     for (y = 0; y < LCD_HEIGHT ; y++)
     {
           PixelDraw(((LCD_WIDTH/2)-1), y, 0xFF, 0xFF, 0xFF);
     }
}




void CrossDraw(UINT16 x, UINT16 y, UINT16 size, UINT8 r, UINT8 g, UINT8 b)
{
   UINT16 i;

   for (i = x-(size/2); i<x+(size/2)+1; i++)
   {
       PixelDraw(i, y, r, g, b);

   }
   for (i = y-(size/2); i<y+(size/2)+1; i++)
   {
       PixelDraw(x, i, r, g, b);
   }
}

void SquareBoxDraw(UINT16 x, UINT16 y, UINT16 size, UINT8 r, UINT8 g, UINT8 b)
{
  int i,j;

  for(i=x;i<(x+size);i++)
  {
    for(j=y;j<(y+size);j++)
    {
      PixelDraw(i, j, r, g, b);
    }
  }

}

/* LCD Framebuffer configuration*/
void LCDFBConfig(void)
{
    RasterDMAFBConfig(SOC_LCDC_0_REGS,
                      (unsigned int)frameBuf,
                      (unsigned int)frameBuf + sizeof(frameBuf) - 2,
                       0);

    RasterDMAFBConfig(SOC_LCDC_0_REGS,
                      (unsigned int)frameBuf,
                      (unsigned int)frameBuf + sizeof(frameBuf) - 2,
                       1);


    /* Enable raster */
    RasterEnable(SOC_LCDC_0_REGS);

/*
    Delay(0x3FFFF);
    Delay(0x3FFFF);
    Delay(0x3FFFF);
*/
    delay(100);
    EcapBkLightVary(MAXBACKLIGHT);
}

int Lcdtest(void)
{
  unsigned int pc, pixcnt=0;
    unsigned int i;

    SetUpLCD();

    delay(100);

    EcapBkLightVary(MAXBACKLIGHT);

    UARTPuts("\n",-1);

    UARTPuts("Display 3 colours with Colour Names       \n", -1);

    for (pc=0; pc<(272*480); pc++)
    {
          frameBuf[pc] = LCD_3colors[pc];
    }

    //Delay(0xFFFFF);
    delay(1000);
    EcapBkLightVary_Demo();



    UARTPuts("Display black                            \n", -1);
    EcapBkLightVary(MAXBACKLIGHT);

    for (pc=0; pc<FRAMEBUFFERSIZE; pc++)
    {
      pixcnt++;
      frameBuf[pc] = 0x00000000;
    }

    delay(1000);

    UARTPuts("Display white                            \n", -1);
    for (i=0; i<272; i++)
    {
      for (pc=0; pc<480; pc++)
      {
            pixcnt++;
            frameBuf[(i*480)+pc] = 0x00FFFFFF;
        }
    }

    delay(1000);

    UARTPuts("Display 3 colours                        \n", -1);

    for (pc=0; pc<(90*480); pc++)
    {
      pixcnt++;
      frameBuf[pc] = 0x000000FF;
    }

    for (pc=(90*480); pc<(180*480); pc++)
    {
      pixcnt++;
      frameBuf[pc] = 0x0000FF00;
    }

    for (pc=(180*480); pc<(272*480); pc++)
    {
      pixcnt++;
      frameBuf[pc] = 0x00FF0000;
    }

    delay(1000);

    UARTPuts("Display Gradient in green                \n", -1);
    COLOR = 0;
    for (i=0; i<272; i++)
    {
      for (pc=0; pc<480; pc++)
      {
            pixcnt++;
            frameBuf[(i*480)+pc] = COLOR<<8;
        }

      if (COLOR<0xFF)
      {
           COLOR++;
        }
    }

    delay(1000);

    UARTPuts("Display Gradient in red                  \n", -1);
    COLOR = 0;

    for (i=0; i<272; i++)
    {
      for (pc=0; pc<480; pc++)
      {
            pixcnt++;
            frameBuf[(i*480)+pc] = COLOR<<16;
        }

        if (COLOR<0xFF)
        {
           COLOR++;
        }

    }

    delay(1000);

    UARTPuts("Display Gradient in white                \n", -1);

    COLOR = 0;
    for (i=0; i<272; i++) {
           for (pc=0; pc<480; pc++) {
             pixcnt++;
             frameBuf[(i*480)+pc] = COLOR<<16 | COLOR<<8 | COLOR;
           }

           if (COLOR<0xFF) {
             COLOR++;
           }
    }

    delay(1000);


    UARTPuts("Display Gradient                       \n", -1);
    COLOR = 0;
    for (i=0; i<90; i++) {
      for (pc=0; pc<80; pc++) {
        pixcnt++;
        COLOR=0xFF;
        frameBuf[(i*480)+pc+0] = COLOR<<0;
        frameBuf[(i*480)+pc+80] = COLOR<<8;
        frameBuf[(i*480)+pc+160] = COLOR<<16;
        frameBuf[(i*480)+pc+240] = COLOR<<8;
        frameBuf[(i*480)+pc+320] = COLOR<<0;
        frameBuf[(i*480)+pc+400] = COLOR<<16;
      }

      if (COLOR<0xFF) {
        COLOR++;
      }

    }

    for (i=90; i<120; i++) {
      for (pc=0; pc<480; pc++) {
        pixcnt++;
        COLOR=(255*pc)/480;
        frameBuf[(i*480)+pc+0] = COLOR;
        frameBuf[((i+30)*480)+pc+0] = (255-COLOR)<<8;
        frameBuf[((i+60)*480)+pc+0] = COLOR<<16;
      }

      if (COLOR<0xFF) {
        COLOR++;
      }
    }


    for (i=180; i<190; i++) {
      for (pc=0; pc<480; pc++) {
        pixcnt++;
        COLOR=(255*pc)/480;
        frameBuf[((i+0) *480)+pc+0] = 0xFF<<0;
        frameBuf[((i+10)*480)+pc+0] = 0XFF<<8;
        frameBuf[((i+20)*480)+pc+0] = 0XFF<<16;
        frameBuf[((i+30)*480)+pc+0] = 0X00FFFFFF;
        frameBuf[((i+40)*480)+pc+0] = 0X00000000;

      }

      if (COLOR<0xFF) {
        COLOR++;
      }
    }

    for (i=230; i<272; i++) {
      for (pc=0; pc<480; pc++) {
        pixcnt++;
        COLOR=(255*pc)/480;
        frameBuf[(i*480)+pc+0] = (COLOR) + (COLOR<<8) + (COLOR<<16);
      }

      if (COLOR<0xFF) {
        COLOR++;
      }
    }

    delay(1000);

#if 0
    LCDCEraseBuffer();
    LCDCDrawBorder();
    Delay(0xFFFFF);

    LCDCEraseBuffer();
    CrossDraw(30,17,10,0xFF,0xFF,0xFF);
    CrossDraw(30,255,10,0xFF,0xFF,0xFF);
    CrossDraw(450,17,10,0xFF,0xFF,0xFF);
    CrossDraw(450,255,10,0xFF,0xFF,0xFF);
    Delay(0xFFFFF);

    LCDCEraseBuffer();
    SquareBoxDraw(450,255,10,0xFF,0xFF,0xFF);
    Delay(0xFFFFF);

    LCDCEraseBuffer();
    SquareBoxDraw(30,255,10,0xFF,0xFF,0xFF);
    Delay(0xFFFFF);

    LCDCEraseBuffer();
    SquareBoxDraw(450,17,10,0xFF,0xFF,0xFF);
    Delay(0xFFFFF);
#endif

    LCDCEraseBuffer();
    EcapBkLightVary(LOWBACKLIGHT);
    DisableBackLight();

    return 0;

}

/*
** Configures raster to display image
*/
void Raster0Init(void)
{
    RasterClocksEnable(SOC_LCDC_0_REGS);

    /* Disable raster */
    RasterDisable(SOC_LCDC_0_REGS);

    /* Configure the pclk */
    RasterClkConfig(SOC_LCDC_0_REGS, 23040000, 192000000);

    /* Configuring DMA of LCD controller */
    RasterDMAConfig(SOC_LCDC_0_REGS, RASTER_DOUBLE_FRAME_BUFFER,
                    RASTER_BURST_SIZE_16, RASTER_FIFO_THRESHOLD_8,
                    RASTER_BIG_ENDIAN_DISABLE);

    /* Configuring modes(ex:tft or stn,color or monochrome etc) for raster controller */
    RasterModeConfig(SOC_LCDC_0_REGS, RASTER_DISPLAY_MODE_TFT_UNPACKED,
                     RASTER_DATA, RASTER_COLOR, RASTER_RIGHT_ALIGNED);


     /* Configuring the polarity of timing parameters of raster controller */
    RasterTiming2Configure(SOC_LCDC_0_REGS, RASTER_FRAME_CLOCK_LOW |
                                            RASTER_LINE_CLOCK_LOW  |
                                            RASTER_PIXEL_CLOCK_HIGH|
                                            RASTER_SYNC_EDGE_RISING|
                                            RASTER_SYNC_CTRL_ACTIVE|
                                            RASTER_AC_BIAS_HIGH     , 0, 255);

    /* Configuring horizontal timing parameter */
    RasterHparamConfig(SOC_LCDC_0_REGS, 480, 4, 8, 43);

    /* Configuring vertical timing parameters */
    RasterVparamConfig(SOC_LCDC_0_REGS, 272, 10, 4, 12);


    RasterFIFODMADelayConfig(SOC_LCDC_0_REGS, 128);
}

/*
** Configures raster to display image
*/
void SetUpLCD(void)
{
  EnableBackLight();
  EcapBkLightEnable();

  LCDFBConfig();

}



void EnableBackLight(void)
{
  GPIOPinWrite(SOC_GPIO_1_REGS,28,GPIO_PIN_HIGH);
}

void DisableBackLight(void)
{
  GPIOPinWrite(SOC_GPIO_1_REGS,28,GPIO_PIN_LOW);
}

/*
**  A wrapper function which enables the End-Of-Frame interrupt of Raster.
*/
void Raster0EOFIntEnable(void)
{
    /* Enable End of frame0/frame1 interrupt */
    RasterIntEnable(SOC_LCDC_0_REGS, RASTER_END_OF_FRAME0_INT |
                                     RASTER_END_OF_FRAME1_INT);

}

/*
** A wrapper function which disables the End-Of-Frame interrupt of Raster.
*/
void Raster0EOFIntDisable(void)
{
    /* Enable End of frame0/frame1 interrupt */
    RasterIntDisable(SOC_LCDC_0_REGS, RASTER_END_OF_FRAME0_INT |
                                     RASTER_END_OF_FRAME1_INT);

}

/******************************* End of file *********************************/
