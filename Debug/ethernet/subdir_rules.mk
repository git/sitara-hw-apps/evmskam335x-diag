################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
ethernet/ar8031.obj: ../ethernet/ar8031.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/tms470/bin/cl470" -mv7A8 --code_state=32 --abi=eabi -me -g --include_path="C:/ti/ccsv5/tools/compiler/tms470/include" --include_path="../audio" --include_path="../id_memory" --include_path="../ethernet" --include_path="../powermanagement" --include_path="../pmic" --include_path="../cap_touch" --include_path="../i2cApi" --include_path="../" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/third_party/fatfs/src" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/grlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/mmcsdlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/usblib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/hw" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a/am335x" --gcc --define=am335x --define=DMA_MODE --define=evmskAM335x --diag_warning=225 --display_error_number --unaligned_access=on --enum_type=packed --preproc_with_compile --preproc_dependency="ethernet/ar8031.pp" --obj_directory="ethernet" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ethernet/emac.obj: ../ethernet/emac.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/tms470/bin/cl470" -mv7A8 --code_state=32 --abi=eabi -me -g --include_path="C:/ti/ccsv5/tools/compiler/tms470/include" --include_path="../audio" --include_path="../id_memory" --include_path="../ethernet" --include_path="../powermanagement" --include_path="../pmic" --include_path="../cap_touch" --include_path="../i2cApi" --include_path="../" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/third_party/fatfs/src" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/grlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/mmcsdlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/usblib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/hw" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a/am335x" --gcc --define=am335x --define=DMA_MODE --define=evmskAM335x --diag_warning=225 --display_error_number --unaligned_access=on --enum_type=packed --preproc_with_compile --preproc_dependency="ethernet/emac.pp" --obj_directory="ethernet" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ethernet/emacTest.obj: ../ethernet/emacTest.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/tms470/bin/cl470" -mv7A8 --code_state=32 --abi=eabi -me -g --include_path="C:/ti/ccsv5/tools/compiler/tms470/include" --include_path="../audio" --include_path="../id_memory" --include_path="../ethernet" --include_path="../powermanagement" --include_path="../pmic" --include_path="../cap_touch" --include_path="../i2cApi" --include_path="../" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/third_party/fatfs/src" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/grlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/mmcsdlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/usblib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/hw" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a/am335x" --gcc --define=am335x --define=DMA_MODE --define=evmskAM335x --diag_warning=225 --display_error_number --unaligned_access=on --enum_type=packed --preproc_with_compile --preproc_dependency="ethernet/emacTest.pp" --obj_directory="ethernet" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


