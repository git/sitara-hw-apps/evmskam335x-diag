################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
usb_dev_msc/ramdisk.obj: ../usb_dev_msc/ramdisk.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/tms470/bin/cl470" -mv7A8 --code_state=32 --abi=eabi -me -g --include_path="C:/ti/ccsv5/tools/compiler/tms470/include" --include_path="../audio" --include_path="../id_memory" --include_path="../ethernet" --include_path="../powermanagement" --include_path="../pmic" --include_path="../cap_touch" --include_path="../i2cApi" --include_path="../" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/third_party/fatfs/src" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/grlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/mmcsdlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/usblib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/hw" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a/am335x" --gcc --define=am335x --define=DMA_MODE --define=evmskAM335x --diag_warning=225 --display_error_number --unaligned_access=on --enum_type=packed --preproc_with_compile --preproc_dependency="usb_dev_msc/ramdisk.pp" --obj_directory="usb_dev_msc" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

usb_dev_msc/usb_dev_msc.obj: ../usb_dev_msc/usb_dev_msc.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/tms470/bin/cl470" -mv7A8 --code_state=32 --abi=eabi -me -g --include_path="C:/ti/ccsv5/tools/compiler/tms470/include" --include_path="../audio" --include_path="../id_memory" --include_path="../ethernet" --include_path="../powermanagement" --include_path="../pmic" --include_path="../cap_touch" --include_path="../i2cApi" --include_path="../" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/third_party/fatfs/src" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/grlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/mmcsdlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/usblib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/hw" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a/am335x" --gcc --define=am335x --define=DMA_MODE --define=evmskAM335x --diag_warning=225 --display_error_number --unaligned_access=on --enum_type=packed --preproc_with_compile --preproc_dependency="usb_dev_msc/usb_dev_msc.pp" --obj_directory="usb_dev_msc" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

usb_dev_msc/usb_msc_structs.obj: ../usb_dev_msc/usb_msc_structs.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/tms470/bin/cl470" -mv7A8 --code_state=32 --abi=eabi -me -g --include_path="C:/ti/ccsv5/tools/compiler/tms470/include" --include_path="../audio" --include_path="../id_memory" --include_path="../ethernet" --include_path="../powermanagement" --include_path="../pmic" --include_path="../cap_touch" --include_path="../i2cApi" --include_path="../" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/third_party/fatfs/src" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/grlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/mmcsdlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/usblib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/hw" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a/am335x" --gcc --define=am335x --define=DMA_MODE --define=evmskAM335x --diag_warning=225 --display_error_number --unaligned_access=on --enum_type=packed --preproc_with_compile --preproc_dependency="usb_dev_msc/usb_msc_structs.pp" --obj_directory="usb_dev_msc" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

usb_dev_msc/usbdmscglue.obj: ../usb_dev_msc/usbdmscglue.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/tms470/bin/cl470" -mv7A8 --code_state=32 --abi=eabi -me -g --include_path="C:/ti/ccsv5/tools/compiler/tms470/include" --include_path="../audio" --include_path="../id_memory" --include_path="../ethernet" --include_path="../powermanagement" --include_path="../pmic" --include_path="../cap_touch" --include_path="../i2cApi" --include_path="../" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/third_party/fatfs/src" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/grlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/mmcsdlib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/usblib/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/hw" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a" --include_path="C:/ti/AM335X_StarterWare_02_00_00_07/include/armv7a/am335x" --gcc --define=am335x --define=DMA_MODE --define=evmskAM335x --diag_warning=225 --display_error_number --unaligned_access=on --enum_type=packed --preproc_with_compile --preproc_dependency="usb_dev_msc/usbdmscglue.pp" --obj_directory="usb_dev_msc" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


