################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../powermanagement/slpWkup_cgttms470.asm 

C_SRCS += \
../powermanagement/am335x_clock_data.c \
../powermanagement/cm3wkup_proxy.c \
../powermanagement/ds0PwrMgmnt.c 

OBJS += \
./powermanagement/am335x_clock_data.obj \
./powermanagement/cm3wkup_proxy.obj \
./powermanagement/ds0PwrMgmnt.obj \
./powermanagement/slpWkup_cgttms470.obj 

ASM_DEPS += \
./powermanagement/slpWkup_cgttms470.pp 

C_DEPS += \
./powermanagement/am335x_clock_data.pp \
./powermanagement/cm3wkup_proxy.pp \
./powermanagement/ds0PwrMgmnt.pp 

C_DEPS__QUOTED += \
"powermanagement\am335x_clock_data.pp" \
"powermanagement\cm3wkup_proxy.pp" \
"powermanagement\ds0PwrMgmnt.pp" 

OBJS__QUOTED += \
"powermanagement\am335x_clock_data.obj" \
"powermanagement\cm3wkup_proxy.obj" \
"powermanagement\ds0PwrMgmnt.obj" \
"powermanagement\slpWkup_cgttms470.obj" 

ASM_DEPS__QUOTED += \
"powermanagement\slpWkup_cgttms470.pp" 

C_SRCS__QUOTED += \
"../powermanagement/am335x_clock_data.c" \
"../powermanagement/cm3wkup_proxy.c" \
"../powermanagement/ds0PwrMgmnt.c" 

ASM_SRCS__QUOTED += \
"../powermanagement/slpWkup_cgttms470.asm" 


